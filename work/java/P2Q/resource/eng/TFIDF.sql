/****** Script for SelectTopNRows command from SSMS  ******/
DECLARE @maxWeight float;

select  @maxWeight = (select max(weight) as res from [interestminingYahooL6100m].[dbo].[Evidence])

DECLARE @minWeight float;

select  @minWeight = (select min(weight) as res from [interestminingYahooL6100m].[dbo].[Evidence])

select @minWeight, @maxWeight, @minWeight /(@maxWeight - @minWeight)

INSERT INTO SemanticSimilarity.dbo.Freq (word, freq, informationSourceId)
       SELECT keyword, (AVG(weight) - @minWeight)/(@maxWeight - @minWeight) , 'TFIDF' --[typeOfEvidence]
  FROM [interestminingYahooL6100m].[dbo].[Evidence]
  where informationSourceId='TFIDF' 
  group by keyword
  order by AVG(weight) desc

  use SemanticSimilarity
  truncate Table Freq

  SELECT * --max(freq) --COUNT(*)
     
  FROM [SemanticSimilarity].[dbo].[Freq]

  where freq>9
  
  SELECT min(freq) --COUNT(*)
     
  FROM [SemanticSimilarity].[dbo].[Freq]
