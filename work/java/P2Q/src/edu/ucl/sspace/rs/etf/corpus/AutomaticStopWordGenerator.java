/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ucl.sspace.rs.etf.corpus;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import edu.ucl.sspace.rs.etf.database.ESConnection;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Miodrag Dinic
 * 
 * 		   Vuk Batanovic
 */
public class AutomaticStopWordGenerator {

    private static Connection con = null;
    private static HashMap lexicon = null;
    private static HashMap stopwords = null;
    private static long totalNumberOfTerms = 0;
    private static long totalNumberOfWords = 0;

    public static final int _LOCAL_MAXIMUM = 0;
    public static final int _STDDEV = 1;
    public static final int _ARBITRARY = 2;

    public static void main(String[] args) throws IOException, SQLException {

        // Converting 'Vreme-words.txt' corpus to dual1 coding system
        //SrCorpusProcessor.processSrCorpus("vreme-words.txt", "vreme-corpus-dual1.txt");
        // Cleaning the 'vreme' corpus
        //CorpusManagementES.cleanDual1PlainTxtCorpora("vreme-corpus-dual1.txt", "vreme-corpus-clean.txt");
        // Stemming the 'vreme' corpus
        //CorpusManagementES.stemCorpus(new File("vreme-corpus-clean.txt"), new File("vreme-corpus-stemmed.txt"));

        // Creating the Lexicon file for the 'vreme' corpus.
        //AutomaticStopWordGenerator. buildLexiconAndCalculateFreq("CorpusStemmerIzlaz.txt","lexicon-Wiki-stemmed.txt");
        //AutomaticStopWordGenerator.evaluateStopWordLists("STDDEV_II___Wiki-stemmed","StopWordsListEnrichedMedium-stemmed.txt", 0,20 , "SWL-EVALUATION_STDDEV_II__Wiki-stemmed.txt", AutomaticStopWordGenerator._STDDEV);
        // Generate Stop-Word list
        //AutomaticStopWordGenerator.generateStopWordList("StopWordList-VremeCorpus-Nonstemmed.txt", 4);
        // Merging stop-word lists
        //AutomaticStopWordGenerator.mergeStopWordLists("StopWordsListEnrichedLarge.txt", "StopWordsListEnrichedLarge-merged.txt");
        // Removing stop-words from corpus
        //AutomaticStopWordGenerator.removeStopWords("vreme-corpus-clean.txt", "vreme-corpus-clean-filtered.txt");

        // Creating the Lexicon file for the 'vreme' corpus.
        //AutomaticStopWordGenerator.buildLexiconAndCalculateFreq("vreme-corpus-stemmed.txt","lexicon-vreme-stemmed.txt");
        //AutomaticStopWordGenerator.rankLexiconAndFillDB();
        // Generate Stop-Word list
        //AutomaticStopWordGenerator.generateStopWordList("StopWordList-VremeCorpus-Stemmed.txt", 11);
        // Merging stop-word lists
        //AutomaticStopWordGenerator.mergeStopWordLists("StopWordsListEnrichedLarge-stemmed.txt", "StopWordsListEnrichedLarge-stemmed-merged.txt");

        // Creating the Lexicon file for the 'vreme' corpus.
        //AutomaticStopWordGenerator.buildLexiconAndCalculateFreq("MergedVremeAndWikiCorpus-stemmed.txt", "lexicon-MergedVremeAndWikiCorpus-stemmed.txt");
        // Generate Stop-Word list
        //AutomaticStopWordGenerator.generateStopWordList("StopWordList-MergedVremeAndWikiCorpus-stemmed.txt", 15);
        // Merging stop-word lists
        //AutomaticStopWordGenerator.mergeStopWordLists("StopWordsListEnrichedLarge-stemmed.txt", "StopWordsListEnrichedLarge-stemmed-merged.txt");
        //AutomaticStopWordGenerator.removeStopWords("MergedVremeAndWikiCorpus-stemmed.txt", "MergedVremeAndWikiCorpus-stemmed-FILTERED.txt");

        //AutomaticStopWordGenerator.calculateStandardDeviation("stddev.txt");
        //AutomaticStopWordGenerator.redirectSystemOut("systemOutput.txt");
        //AutomaticStopWordGenerator.evaluateStopWordLists("ARBITRARY__Vreme-stemmed", "StopWordsListEnrichedMedium-stemmed.txt", 250,600 , "SWL-EVALUATION_ARBITRARY__Vreme-stemmed.txt", AutomaticStopWordGenerator._ARBITRARY);

        //AutomaticStopWordGenerator.generateStopWordList("SWL-I311__ARBITRARY__Vreme-stemmed.txt", 311, AutomaticStopWordGenerator._ARBITRARY);

        // Creating the Lexicon file for the 'CorpusCleanerIzlaz' corpus.
        //AutomaticStopWordGenerator.buildLexiconAndCalculateFreq("CorpusCleanerIzlaz.txt","lexicon-CorpusCleanerIzlaz.txt");
        // Generate Stop-Word list
        //AutomaticStopWordGenerator.generateStopWordList("StopWordList-WikiCorpus-Stemmed.txt", 4);

        // Converting to dual1 coding system
        //SrCorpusProcessor.processSrCorpus("StopWordsListPlainSmall300.txt", "StopWordsListPlainSmall300-dual1.txt");
        // Cleaning
        //CorpusManagementES.cleanDual1PlainTxtCorpora("StopWordsListPlainSmall300-dual1.txt", "StopWordsListPlainSmall300-clean.txt");
        // Stemming
        //CorpusManagementES.stemCorpus(new File("StopWordsListPlainSmall300-clean.txt"), new File("StopWordsListPlainSmall300-stemmed.txt"));

        //AutomaticStopWordGenerator.removeStopWords("CorpusStemmerIzlaz.txt", "WikiCorpusStemmed-FILTERED-StopWordsListEnrichedMedium-stemmed.txt");
        //AutomaticStopWordGenerator.createExcellImportFile("zipfLaw.txt");
        //AutomaticStopWordGenerator.importStopWordList("SWL-MERGED__SWL-I2__STDDEV__Wiki-stemmed.txt");
        //AutomaticStopWordGenerator.mergeStopWordLists("StopWordsListEnrichedMedium-stemmed.txt", "SWL-MERGED__SWL-I2__STDDEV__Wiki-stemmed.txt");
    }

    private AutomaticStopWordGenerator() {
    }

    public static void redirectSystemOut(String redirectFilePath) {
        try {
            System.setOut(new PrintStream(new FileOutputStream(redirectFilePath)));
        } catch (FileNotFoundException ex) {
            System.out.println("FileNotFoundException in 'redirectSystemOut' : "+ex.getMessage());
        }
    }

    public static long getTotalNumberOfWords() {
        return totalNumberOfWords;
    }

    public static Connection getCon() throws SQLException {
        con = ESConnection.getConnection();
        return con;
    }

    public static void calculateStandardDeviation(String outputFile) {
        try {
            File output = new File(outputFile);
            if (output.exists()) {
                output.delete();
                output.createNewFile();
            }
            PrintWriter pw = new PrintWriter(output);

            for (int i = 1; i < 1000; i++) {
                String query = "SELECT STDDEV(diff) FROM stopwords WHERE rank <= ?";
                PreparedStatement ps = getCon().prepareStatement(query);
                ps.setInt(1, i);
                ResultSet res = ps.executeQuery();
                if (res.next()) {
                    query = "SELECT * FROM stopwords WHERE rank=?";
                    ps = getCon().prepareStatement(query);
                    ps.setInt(1, i);
                    ResultSet ress = ps.executeQuery();
                    ress.next();
                    pw.write(i + "\t" + res.getDouble(1) + "\t" + ress.getDouble("diff") + "\r\n");
                }
            }
            pw.close();

        } catch (SQLException e) {
            System.out.println("SQL exception in calculateStandardDeviation : " + e.getMessage());
        } catch (IOException e) {
            System.out.println("IOException in calculateStandardDeviation : " + e.getMessage());
        }

    }

    public static int getTotalNumberOfTerms() {
        try {
            String query = "SELECT COUNT(*) FROM lexicon";
            PreparedStatement ps = getCon().prepareStatement(query);
            ResultSet res = ps.executeQuery();
            if (res.next()) {
                return res.getInt(1);
            } else {
                return 0;
            }
        } catch (SQLException e) {
            System.out.println("SQL exception in getTotalNumberOfTerms : " + e.getMessage());
            return 0;
        }
    }

    private static void truncateDB() {
        try {
            String query = "TRUNCATE lexicon";
            PreparedStatement ps = getCon().prepareStatement(query);
            ps.execute();

            query = "TRUNCATE rankedlexicon";
            ps = getCon().prepareStatement(query);
            ps.execute();

            query = "TRUNCATE stopwords";
            ps = getCon().prepareStatement(query);
            ps.execute();

            stopwords = null;
        } catch (SQLException e) {
            System.out.println("SQL exception : " + e.getMessage());
        }
    }

    private static double getThreshold(int exponent) {
        if (getTotalNumberOfTerms() != 0) {
            int limit;
            if ((limit = getInterestLimit(exponent)) < 0) {
                return -1;
            }
            try {
                String query = "SELECT STDDEV(diff) FROM stopwords WHERE rank<=?";
                PreparedStatement ps = getCon().prepareStatement(query);
                ps.setInt(1, limit);
                ResultSet res = ps.executeQuery();

                double threshold = 0;
                if (res.next()) {
                    threshold = res.getDouble(1);
                }

                if (threshold != 0) {
                    return threshold;
                } else {
                    return -1;
                }
            } catch (SQLException sqEx) {
                System.out.println("SQL Error - method : getThreshold" + sqEx.getMessage());
                return -1;
            }
        } else {
            return -1;
        }
    }

    private static int getInterestLimit(int exponent) {
        if (exponent >= 0 && ((getTotalNumberOfTerms() / (java.lang.Math.pow(2, exponent))) >= 1)) {
            int limit = (int) (getTotalNumberOfTerms() / (java.lang.Math.pow(2, exponent)));
            return limit;
        } else {
            return -1;
        }
    }

    /*
    private int getInterestLimit(int exponent) {
    return exponent;
    }
     */

    public static void importStopWordList(String listPath){
        try{
            String query = "TRUNCATE stopwords";
            PreparedStatement ps = ESConnection.getConnection().prepareStatement(query);
            ps.execute();

            File fileIn = new File(listPath);
            BufferedReader br = new BufferedReader(new FileReader(fileIn));

            String line;
            int updated = 0;
            System.out.println("-------Importing--------\nPlease wait...");
            while ((line = br.readLine()) != null) {
                String words[] = line.split("\\s");
                for (int i = 0; i < words.length; i++) {
                    if (!words[i].equals("")) {
                        if (AutomaticStopWordGenerator.addStopWord(words[i]) == true) {
                            updated++;
                        }
                    }
                }
            }
            System.out.println("Imported total of " + updated + " words");
        }catch(IOException ioe){
            System.out.println("IOException in 'importStopWordList' : "+ioe.getMessage());
        }catch(SQLException sqle){
            System.out.println("SQLException in 'importStopWordList' : "+sqle.getMessage());
        }
    }

    public static void mergeStopWordLists(String listPath, String mergedPath) {
        try {
            File fileOut = new File(mergedPath);
            if (fileOut.exists()) {
                fileOut.delete();
                fileOut.createNewFile();
            }
            PrintWriter pw = new PrintWriter(fileOut);

            File fileIn = new File(listPath);
            BufferedReader br = new BufferedReader(new FileReader(fileIn));

            String line;
            int updated = 0;
            System.out.println("-------Merging started--------\nPlease wait...");
            while ((line = br.readLine()) != null) {
                String words[] = line.split("\\s");
                for (int i = 0; i < words.length; i++) {
                    if (!words[i].equals("")) {
                        if (AutomaticStopWordGenerator.addStopWord(words[i]) == true) {
                            updated++;
                        }
                    }
                }
            }
            System.out.println("Merging completed! Updated total of " + updated + " words");

            String query = "SELECT * FROM stopwords ORDER BY term ASC";
            PreparedStatement ps = ESConnection.getConnection().prepareStatement(query);
            ResultSet res = ps.executeQuery();
            System.out.println("Creating stop-word list file : " + mergedPath);
            stopwords = new HashMap();
            while (res.next()) {
                pw.write(res.getString("term") + "\r\n");
                stopwords.put(res.getString("term"), 1);
            }
            System.out.println("Complete!");

            br.close();
            pw.close();


        } catch (Exception e) {
            System.out.println("Exception at mergeStopWordLists : " + e.getMessage());
        }
    }

    public static boolean addStopWord(String word) {
        try {
            String query = "SELECT COUNT(*) FROM stopwords WHERE term=?";
            PreparedStatement ps = ESConnection.getConnection().prepareStatement(query);
            ps.setString(1, word);
            ResultSet res = ps.executeQuery();
            if (res.next()) {
                if (res.getInt(1) == 0) {
                    query = "INSERT INTO stopwords(term,diff) VALUES(?,-1)";
                    ps = ESConnection.getConnection().prepareStatement(query);
                    ps.setString(1, word);
                    ps.executeUpdate();
                    if (stopwords != null) {
                        stopwords.put("word", 1);
                    }

                    return true;
                } else {
                    return false;
                }
            }
            return false;
        } catch (Exception e) {
            System.out.println("Exception in addStopWord : " + e.getMessage());
            return false;
        }
    }

    public static void evaluateStopWordLists(String swlNameTag, String referenceListPath, int startAt, int numOfIterations, String resultsPath, int algType) {
        try {
            File refFile = new File(referenceListPath);
            BufferedReader br = new BufferedReader(new FileReader(refFile));

            File resultsFile = new File(resultsPath);
            if (resultsFile.exists()) {
                resultsFile.delete();
                resultsFile.createNewFile();
            }
            PrintWriter pw = new PrintWriter(resultsFile);

            ArrayList<String> referenceList = new ArrayList<String>();
            String line = null;
            while ((line = br.readLine()) != null) {
                String words[] = line.split("\\s");
                for (int i = 0; i < words.length; i++) {
                    if (!words[i].equals("") && !referenceList.contains(words[i])) {
                        referenceList.add(words[i]);
                    }
                }
            }
            br.close();
            final int refTotalCount = referenceList.size();
            int iteration = startAt;
            double maxPrecision = 0;
            int maxPrecisionIteration = 0;
            double results[] = new double[numOfIterations];
            pw.write("iteration" + "\t" + "precision" + "\t" + "hitCount" + "\t" + "listCount" + "\t" + "refListCount" + "\r\n");
            while ((++iteration) <= numOfIterations) {
                generateStopWordList("SWL-I" + iteration + "__" + swlNameTag + ".txt", iteration, algType);
                int hitCount = 0;
                int totalCount;
                if (stopwords != null) {
                    totalCount = stopwords.size();
                } else {
                    break;
                }
                for (int i = 0; i < refTotalCount; i++) {
                    if (stopwords.containsKey(referenceList.get(i))) {
                        hitCount++;
                    }
                }
                double precision = 0;
                if (refTotalCount >= totalCount) {
                    results[iteration - 1] = precision = ((double) totalCount / refTotalCount) * ((double) hitCount / refTotalCount);
                } else {
                    results[iteration - 1] = precision = ((double) refTotalCount / totalCount) * ((double) hitCount / refTotalCount);
                }
                if (precision > maxPrecision) {
                    maxPrecision = precision;
                    maxPrecisionIteration = iteration;
                }
                pw.write(iteration + "\t" + precision + "\t" + hitCount + "\t" + totalCount + "\t" + refTotalCount + "\r\n");
            }
            pw.close();

            System.out.println("\n\nBEST PRECISION : " + maxPrecision);
            System.out.println("BEST PRECISION ACHIVED AT ITERATION :" + maxPrecisionIteration);
            System.out.println("BEST STOP-WORD LIST IS : SWL-I" + maxPrecisionIteration + "__" + swlNameTag + ".txt");
            System.out.println("Results apended to file : " + resultsPath);

        } catch (IOException e) {
            System.out.println("evaluateStopWordLists : " + e.getMessage());
        }
    }

    public static void generateStopWordList(String outputPath, int argument, int algSelect) {
        try {
            switch (algSelect) {
                case 0:
                    successiveLocalMaximumThresholdSelection(outputPath, argument);
                    break;
                case 1:
                    stddevThresholdSelection(outputPath, argument);
                    break;
                case 2:
                    arbitraryThresholdSelection(outputPath, argument);
                    break;
                default:
                    throw new StopWordGeneratorException("Invalid Algorithm selection option in 'generateStopWordList'!");
            }
        } catch (StopWordGeneratorException swge) {
            System.out.println(swge.getMessage());
            //System.exit(1);
        }
    }

    private static void successiveLocalMaximumThresholdSelection(String outputPath, int numOfIterations) {
        if (numOfIterations > 0) {
            rankLexiconAndFillDB();
            int offset = 0;
            try {
                File fileOut = new File(outputPath);
                if (fileOut.exists()) {
                    fileOut.delete();
                    fileOut.createNewFile();
                }
                PrintWriter pw = new PrintWriter(fileOut);

                System.out.println("---------Stop-word list build started----------\n");
                System.out.println("NUMBER OF ITERATIONS : " + numOfIterations);

                ResultSet res;
                int currentIteration = 0;
                boolean reachedTheEnd = false;
                while (currentIteration < numOfIterations) {
                    String query = "SELECT * FROM stopwords WHERE diff=(SELECT MAX(diff) FROM stopwords WHERE rank>?) AND diff<>0";
                    PreparedStatement ps = getCon().prepareStatement(query);
                    ps.setInt(1, offset);
                    res = ps.executeQuery();
                    if (res.next()) {
                        offset = res.getInt("rank");
                    } else {
                        reachedTheEnd = true;
                        break;
                    }
                    currentIteration++;
                }

                if (reachedTheEnd) {
                    System.out.println("\nReached the maximum number of iterations!\nMAXIMUM NUMBER OF ITERATIONS : " + currentIteration + "\n");
                }

                String query = "DELETE FROM stopwords WHERE rank>?";
                PreparedStatement ps = getCon().prepareStatement(query);
                ps.setInt(1, offset);
                ps.executeUpdate();

                query = "SELECT * FROM stopwords";
                res = ps.executeQuery(query);
                System.out.println("Generating output file : " + outputPath + " containing stop words.");
                stopwords = new HashMap();
                while (res.next()) {
                    pw.write(res.getString("term") + "\r\n");
                    stopwords.put(res.getString("term"), 1);
                }
                pw.close();

                System.out.println("Building stop-word list complete!\n");
                System.out.println("TOTAL NUMBER OF STOP WORDS : " + offset);

            } catch (SQLException e) {
                System.out.println("SQL ERROR in 'generateStopWordList' " + e.getMessage());
            } catch (IOException e) {
                System.out.println("IOException in 'generateStopWordList' " + e.getMessage());
            }

        }
    }

    private static void arbitraryThresholdSelection(String outputPath, int threshold) throws StopWordGeneratorException {
        try {
            if (threshold > 0) {
                rankLexiconAndFillDB();

                File fileOut = new File(outputPath);
                if (fileOut.exists()) {
                    fileOut.delete();
                    fileOut.createNewFile();
                }
                PrintWriter pw = new PrintWriter(fileOut);

                System.out.println("---------Stop-word list build started----------\n");
                System.out.println("THRESHOLD VALUE : " + threshold);

                String query = "DELETE FROM stopwords WHERE rank>?";
                PreparedStatement ps = getCon().prepareStatement(query);
                ps.setInt(1, threshold);
                ps.executeUpdate();

                query = "SELECT * FROM stopwords";
                ResultSet res = ps.executeQuery(query);
                System.out.println("Generating output file : " + outputPath + " containing stop words.");
                stopwords = new HashMap();
                while (res.next()) {
                    pw.write(res.getString("term") + "\r\n");
                    stopwords.put(res.getString("term"), 1);
                }
                pw.close();

                System.out.println("Building stop-word list complete!\n");
                System.out.println("TOTAL NUMBER OF STOP WORDS : " + stopwords.size());


            } else {
                stopwords = null;
                throw new StopWordGeneratorException("ERROR in 'arbitraryThresholdSelection' - threshold must be greater than 0 !");
            }
        } catch (IOException ioe) {
            System.out.println("IOException in 'arbitraryThresholdSelection'" + ioe.getMessage());
        } catch (SQLException sqle) {
            System.out.println("SQLException in 'arbitraryThresholdSelection'" + sqle.getMessage());
        }
    }

    private static void stddevThresholdSelection(String outputPath, int exponent) throws StopWordGeneratorException {
        if (exponent > 0) {
            double threshold;
            rankLexiconAndFillDB();
            if ((threshold = getThreshold(exponent)) > 0) {
                int offset = 0;
                try {
                    String query = "CREATE VIEW intervalofinterest AS SELECT * FROM stopwords LIMIT ?";
                    int limit = getInterestLimit(exponent);
                    PreparedStatement ps = getCon().prepareStatement(query);
                    ps.setInt(1, limit);
                    ps.execute();

                    File fileOut = new File(outputPath);
                    if (fileOut.exists()) {
                        fileOut.delete();
                        fileOut.createNewFile();
                    }
                    PrintWriter pw = new PrintWriter(fileOut);

                    System.out.println("---------Stop-word list build started----------\n");
                    System.out.println("THRESHOLD VALUE : " + threshold);
                    System.out.println("INTERVAL OF INTEREST : 0-" + limit + "\n");

                    ResultSet res;
                    double currentMax;
                    while (true) {
                        query = "SELECT * FROM intervalofinterest WHERE diff=(SELECT MAX(diff) FROM intervalofinterest WHERE rank>?) AND diff>=?";
                        ps = getCon().prepareStatement(query);
                        ps.setInt(1, offset);
                        ps.setDouble(2, threshold);
                        res = ps.executeQuery();
                        if (res.next()) {
                            currentMax = res.getDouble("diff");
                            offset = res.getInt("rank");
                        } else {
                            break;
                        }
                    }
                    query = "DROP VIEW intervalofinterest";
                    ps.execute(query);

                    query = "DELETE FROM stopwords WHERE rank>?";
                    ps = getCon().prepareStatement(query);
                    ps.setInt(1, offset);
                    ps.executeUpdate();

                    query = "SELECT * FROM stopwords";
                    res = ps.executeQuery(query);
                    System.out.println("Generating output file : " + outputPath + " containing stop words.");
                    stopwords = new HashMap();
                    while (res.next()) {
                        pw.write(res.getString("term") + "\r\n");
                        stopwords.put(res.getString("term"), 1);
                    }
                    pw.close();

                    System.out.println("Building stop-word list complete!\n");
                    System.out.println("TOTAL NUMBER OF STOP WORDS : " + offset);

                } catch (Exception e) {
                    System.out.println("Generating stop-word list failed :" + e.getMessage());
                }
            } else {
                stopwords = null;
                throw new StopWordGeneratorException("Invalid threshold, probably the intervalOfInterest returned -1 because exponent was too large!");
            }

        }
    }

    private static void initStopWordList() {
        try {
            if (stopwords == null) {

                String query = "SELECT * FROM stopwords ORDER BY term ASC";
                PreparedStatement ps = ESConnection.getConnection().prepareStatement(query);
                ResultSet res = ps.executeQuery();

                stopwords = new HashMap();
                while (res.next()) {
                    stopwords.put(res.getString("term"), 1);
                }
            }
        } catch (SQLException sqle) {
            System.out.println("SQL Exception - initStopWordList : " + sqle.getMessage());
        }

    }

    public static HashMap getStopWordList() {
        initStopWordList();
        return stopwords;
    }

    public static String removeStopWords(String line) {

        String strline = line.trim();
        String[] words = strline.split("\\s");

        initStopWordList();
        if (stopwords != null) {

            String filtered = "";
            for (int i = 0; i < words.length; i++) {
                Integer test = (Integer) stopwords.get(words[i]);
                if (test == null && !words[i].equals("")) {
                    filtered += words[i] + " ";
                }
            }

            return filtered.trim();
        }

        return line;
    }

    public static void removeStopWords(String corpusPath, String outputPath) {
        try {
            File fileOut = new File(outputPath);
            if (fileOut.exists()) {
                fileOut.delete();
                fileOut.createNewFile();
            }
            PrintWriter pw = new PrintWriter(fileOut);

            File fileIn = new File(corpusPath);
            BufferedReader br = new BufferedReader(new FileReader(fileIn));

            initStopWordList();
            if (stopwords != null) {
                FileProcessingInfoES fpi = new FileProcessingInfoES(fileIn);
                String line;
                long removed = 0;
                System.out.println("--------Corpus filtering started---------");
                while ((line = br.readLine()) != null) {
                    String words[] = line.split("\\s");
                    int localRemoved = 0;
                    for (int i = 0; i < words.length; i++) {
                        Integer test = (Integer) stopwords.get(words[i]);
                        if (test == null && !words[i].equals("")) {
                            pw.write(words[i] + " ");
                        } else {
                            removed++;
                            localRemoved++;
                        }
                        if ((i + 1 == words.length) && (localRemoved != words.length)) {
                            pw.write("\r\n");
                        }
                    }
                    fpi.printInfo(line);
                }
                System.out.println("Removing stop-words complete!");
                System.out.println("Removed [" + removed + "] words");

                br.close();
                pw.close();
            } else {
                System.out.println("Stop-word list not generated!");
            }

        } catch (FileNotFoundException fnfe) {
            System.out.println("File not found exception - removeStopWords method : " + fnfe.getMessage());
        } catch (Exception e) {
            System.out.println("Exception in removeStopWords : " + e.getMessage());
        }
    }

    public static void buildLexiconAndCalculateFreq(String corpusPath, String outputPath, String termFrequenciesPath) throws IOException {

        lexicon = new HashMap();
        totalNumberOfWords = 0;
        totalNumberOfTerms = 0;
        File fileIn = new File(corpusPath);
        BufferedReader br = new BufferedReader(new FileReader(fileIn));
        File fileOut = new File(outputPath);
        if (fileOut.exists()) {
            fileOut.delete();
            fileOut.createNewFile();
        }
        PrintWriter pw = new PrintWriter(fileOut);
        try {
            //truncateDB();
            String line = null;
            long step = 5000;
            Long tf = null;
            System.out.print("\nLexicon file creation in progres : ");
            while ((line = br.readLine()) != null) {
                String words[] = line.split("\\s");
                for (int i = 0; i < words.length; i++) {
                    if (!words[i].equals("")) {
                        tf = new Long(1);
                        totalNumberOfWords++;
                        if ((tf = (Long) lexicon.put(words[i], tf)) == null) {
                            pw.write(words[i] + "\r\n");
                        } else {
                            tf += 1;
                            lexicon.put(words[i], tf);
                        }
                        if (lexicon.size() > step) {
                            System.out.print(".");
                            step += 5000;
                        }
                    }
                }
            }
            totalNumberOfTerms = lexicon.size();

            System.out.print("100%");
            System.out.println("\nLexicon file creation complete!\n");
            System.out.println("Total number of terms : " + totalNumberOfTerms);
            System.out.println("Total number of words : " + totalNumberOfWords);

            br.close();
            pw.flush();
            pw.close();
            // dispose all the resources after using them.

            calculate_TF_and_TFnorm(outputPath, termFrequenciesPath);    //Calculating term frequences

        } catch (Exception e) {
            System.out.println("Lexicon file creation failed!");
        }

    }

    private static void insertIntoLexiconDB(String term, long TF, double TFnorm, double TFnorm0_1) throws IOException {

        try {
            PreparedStatement ps = null;
            // get the connection
            String sqlString = null;
            sqlString = "INSERT INTO lexicon (term, tf, tfnorm, tfnorm0_1) VALUES (?,?,?,?);";

            System.out.println("Term: " + term);
            System.out.println("TF= " + TF);
            System.out.println("TFnorm= " + TFnorm);
            System.out.println("TFnorm0_1= " + TFnorm0_1);
            System.out.println();
            
            ps = getCon().prepareStatement(sqlString);
            ps.setString(1, term);
            ps.setString(2, "" + TF);
            ps.setString(3, "" + TFnorm);
            ps.setString(4, "" + TFnorm0_1);
            ps.executeUpdate();

            
        } catch (SQLException sqEx) {
            System.out.println("SQL Error - method : insertIntoLexiconDB");
            System.out.println(sqEx);

        }
    }

    private static void calculate_TF_and_TFnorm(String lexiconPath, String termFrequenciesPath) throws FileNotFoundException, IOException {
        File fileIn = new File(lexiconPath);
        BufferedReader br = new BufferedReader(new FileReader(fileIn));
        
        File fileOut = new File(termFrequenciesPath);
        if (fileOut.exists()) {
            fileOut.delete();
            fileOut.createNewFile();
        }
        PrintWriter pw = new PrintWriter(fileOut);

        Long TF;
        Double TFnorm;
        Double TFnorm0_1;

        FileProcessingInfoES fp = new FileProcessingInfoES(fileIn);
        try {
            String line;
            System.out.println("\nCalculating TF and TFnorm, filling database in progress...");
            while ((line = br.readLine()) != null) {
                TF = (Long) lexicon.get(line);
                Double norm = (TF.doubleValue()) / totalNumberOfWords;
                TFnorm = -(java.lang.Math.log10(norm));
                TFnorm0_1 = -(java.lang.Math.log10(1.0/totalNumberOfWords));
                TFnorm0_1 = TFnorm / TFnorm0_1;
                //insertIntoLexiconDB(line, TF, TFnorm, TFnorm0_1);
                pw.write(line + " " + TF + " " + TFnorm + " " + TFnorm0_1 + "\r\n");
                fp.printInfo(line);
            }
            System.out.println("\nSuccessfully completed task!");
            pw.flush();
            pw.close();

            //rankLexiconAndFillDB();
        } catch (Exception e) {
            System.out.println("\nmethod 'calculate_TF_and_TFnorm' failed!");
        } finally {
            br.close();
            // dispose all the resources after using them.
        }
    }

    public static void rankLexiconAndFillDB() {

        try {
            System.out.println("Ranking terms in Lexicon and updating the database...");

            String sql = "TRUNCATE rankedlexicon";
            PreparedStatement st = getCon().prepareStatement(sql);
            st.execute();

            sql = "TRUNCATE stopwords";
            st = getCon().prepareStatement(sql);
            st.execute();

            sql = "insert into rankedlexicon(tid,term,tf,tfnorm) select tid,term,tf,tfnorm from lexicon where tf>100 order by tf desc";
            st = getCon().prepareStatement(sql);
            st.executeUpdate();
            System.out.println("Complete!\n");

            System.out.println("Calculating divergence...");
            sql = "insert into stopwords(rank,term,diff) select t1.rank, t1.term, abs(t1.tfnorm-t2.tfnorm) "
                    + "from rankedlexicon as t1, rankedlexicon as t2 where t1.rank=t2.rank-1";
            st = getCon().prepareStatement(sql);
            st.executeUpdate();
            System.out.println("Complete!\n");

        } catch (SQLException ex) {
            Logger.getLogger(AutomaticStopWordGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void createExcellImportFile(String outputPath) throws IOException {
        File fileOut = new File(outputPath);
        PrintWriter pw = new PrintWriter(fileOut);

        try {
            PreparedStatement ps = null;
            // get the connection
            String sqlString = null;
            sqlString = "SELECT term,tf,tfnorm FROM lexicon order by tf desc limit 5000;";

            ps = getCon().prepareStatement(sqlString);
            ps.executeQuery();
            ResultSet rs = ps.getResultSet();
            int rank = 0;
            int linesWritten = 0;
            int step = 1000;
            System.out.println("\nCreating excell import file:" + outputPath);
            while (rs.next()) {
                int termIndex = rs.findColumn("term");
                int tfIndex = rs.findColumn("tf");
                int tfNormIndex = rs.findColumn("tfnorm");

                String term = rs.getString(termIndex);
                long tf = rs.getLong(tfIndex);
                double tfnorm = rs.getDouble(tfNormIndex);

                rank++;
                pw.write(term + "\t" + tf + "\t" + tfnorm + "\r\n");
                linesWritten++;
                if (linesWritten == step) {
                    System.out.println("[" + linesWritten + "] lines processed..");
                    step += 1000;
                }
            }
            System.out.println("\nTask Completed successfully!");


        } catch (SQLException sqEx) {
            sqEx.printStackTrace();
            System.exit(1);
        } finally {
            pw.flush();
            pw.close();
        }
    }
}
