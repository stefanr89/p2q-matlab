/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ucl.sspace.rs.etf.corpus;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Vuk Batanovic
 */
public class StopWordListBuilder {
    private class StopWord implements Comparable<StopWord>
    {
        private String word;
        private long numberOfOccurrences;
        
        StopWord (String w)
        {
            word = w;
            numberOfOccurrences = 1;
        }
        
        public void increaseOccurrences () { numberOfOccurrences++;}

        public int compareTo(StopWord sw) {
            if (this.numberOfOccurrences > sw.numberOfOccurrences) return -1;
            else if (this.numberOfOccurrences == sw.numberOfOccurrences) return 0;
            else return 1;
        }
        
        public String getWord () { return word;}
        
        public String toString () { return word + "\t\t" + numberOfOccurrences;}
    }
    
    private ArrayList<StopWord> listaReci = new ArrayList<StopWord> (50000);
    
    public StopWordListBuilder (File inputFile) throws FileNotFoundException, IOException
    {
        String line = null;
        BufferedReader br = new BufferedReader (new FileReader (inputFile));
        int lineCnt = 0;
        while ((line = br.readLine()) != null)
        {
            String [] tokens = line.split("\\s");
            for (String s: tokens)
            {
                if (s.equals("")) continue;
                int index = findIndex(s);
                if (index == -1) listaReci.add(new StopWord(s));
                else listaReci.get(index).increaseOccurrences();
            }
            lineCnt++;
            System.out.println("Obradjena linija: " + lineCnt);
            if (lineCnt % 10000 == 0) Collections.sort(listaReci);
        }
        
        Collections.sort(listaReci);
    }
    
    private int findIndex (String word)
    {
        for (int i=0; i<listaReci.size(); i++)
            if (listaReci.get(i).getWord().equalsIgnoreCase (word)) return i;
        return -1;
    }
    
    public void printFirstXWords (int x)
    {
        for (int i=0; i<x; i++)
        {
            StopWord sw = listaReci.get(i);
            System.out.println(sw);
        }
    }
    
    public void saveFirstXWords (int x, File output) throws IOException
    {
        FileWriterES fw = new FileWriterES (output);
        for (int i=0; i<x; i++)
        {
            StopWord sw = listaReci.get(i);
            fw.writeLine(sw.toString());
        }
    }
    
    public int getSize () { return listaReci.size();} // 177982 razlicitih reci
                                                       //146244
                                                       //144349
                                                       //139821
    
    public static void cleanStopWordList (File inputFile, File outputFile) throws FileNotFoundException, IOException
    {
        FileReaderES fr = new FileReaderES(inputFile);
        FileWriterES fw = new FileWriterES(outputFile);
        ArrayList<String> lista = new ArrayList<String> (300);
        String line = null;
        while ((line = fr.readLine()) != null)
        {
            String [] tokens = line.split("\\s");
            lista.add(tokens[0]);
        }
        Collections.sort(lista);
        for (String s: lista)
            fw.writeLine(s);
        fr.close();
        fw.close();
    }
    
    public static void main (String[] args) throws FileNotFoundException, IOException
    {
        /*
        File inputFile = new File ("CorpusCleanerIzlaz.txt");
        File outputFile = new File ("StopWordsList.txt");
        StopWordListBuilder swlb = new StopWordListBuilder (inputFile);
        swlb.saveFirstXWords (10000, outputFile);
        System.out.println(swlb.getSize());
         * 
         */
        File inputFile = new File ("StopWordsListPlainSmall.txt");
        File outputFile = new File ("StopWordsListPlainSmall2.txt");
        cleanStopWordList (inputFile, outputFile);
    }
}
