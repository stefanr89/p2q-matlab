/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ucl.sspace.rs.etf.paraphrase;

import edu.ucl.sspace.rs.etf.corpus.FileWriterES;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author Vuk Batanovic
 */
public class ParaphraseGrader extends JFrame {
    
    private JTextArea recenica1 = new JTextArea (), recenica2 = new JTextArea ();
    private JPanel gornji = new JPanel (), srednji = new JPanel (), donji = new JPanel();
    private JButton iste = new JButton ("Iste"), razlicite = new JButton("Različite"), dalje = new JButton ("Preskoči");
    private File input, output;
    private int lineCnt = 0, naRedu = 0;
    private ArrayList<String> listaParova = new ArrayList<String> ();
    private ArrayList<String> listaParovaGraded = new ArrayList<String> ();
    private JLabel info;
    private boolean vecOcenjenFajl;
    
    public ParaphraseGrader () throws FileNotFoundException, IOException
    {
        JFileChooser fc = new JFileChooser ();
        FileNameExtensionFilter ff = new javax.swing.filechooser.FileNameExtensionFilter("TXT fajl", "txt");
        fc.setFileFilter(ff);
        int returnVal = fc.showOpenDialog(null);
        if (returnVal != JFileChooser.APPROVE_OPTION) System.exit(0);
        String filePath = fc.getSelectedFile().getAbsolutePath();
        
        this.setSize(600, 250);
        this.setTitle("Procena semantičke sličnosti parova rečenica");
        
        input = new File (filePath);
        BufferedReader br = new BufferedReader (new InputStreamReader (new FileInputStream(input), "UTF-8")); 
        String line = "";
        while ((line = br.readLine()) !=null)
        {
            lineCnt++;
            listaParova.add(line);
        }
        br.close();
        
        if (listaParova.isEmpty())
        {
            JOptionPane.showMessageDialog(this, "Odabran je prazan fajl!", "", JOptionPane.INFORMATION_MESSAGE);
            System.exit(0);
        }
        
        String temp = listaParova.get(0);
        if (temp.split("\t").length == 3)
        {
            output = new File (filePath);
            vecOcenjenFajl = true;
        }
        else
        {
            output = new File (filePath.substring(0, filePath.length()-4).concat("Graded.txt"));
            vecOcenjenFajl = false;
        }
        
        

        info = new JLabel ("Linija 1 od " + lineCnt);
        info.setHorizontalAlignment(JLabel.CENTER);
        naRedu++;
        
        JPanel textPanel = new JPanel (new GridLayout (2,1));
        textPanel.add(gornji);
        textPanel.add(srednji);
        
        gornji.setLayout(new BorderLayout ());
        gornji.add("Center", recenica1);
        gornji.add("North", new JLabel ("Rečenica 1:"));
        srednji.setLayout(new BorderLayout());
        srednji.add("Center", recenica2);
        srednji.add("North", new JLabel ("Rečenica 2:"));
        donji.setLayout(new FlowLayout());
        donji.add(iste);
        donji.add(razlicite);
        donji.add(dalje);
        
        this.getContentPane().setLayout(new BorderLayout ());
        this.getContentPane().add("Center", textPanel);
        this.getContentPane().add("South", donji);
        this.getContentPane().add("North", info);
                
        recenica1.setEditable(false);
        recenica2.setEditable(false);
        recenica1.setLineWrap(true);
        recenica2.setLineWrap(true);
        recenica1.setWrapStyleWord(true);
        recenica2.setWrapStyleWord(true);

        String recenice = nadjiSledeciPar ();
        if (recenice == null)
        {
            JOptionPane.showMessageDialog(this, "Već su obrađene sve rečenice!", "", JOptionPane.INFORMATION_MESSAGE);
            System.exit(0);
        }
        else
        {
            String [] delovi = recenice.split("\t");
            if (!vecOcenjenFajl)
            {
                recenica1.setText(delovi[0]);
                recenica2.setText(delovi[1]);
            }
            else
            {
                recenica1.setText(delovi[1]);
                recenica2.setText(delovi[2]);
            }
            info.setText("Linija " + naRedu + " od " + lineCnt);
        }

        
        iste.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent e)
            {
                upisi (1);
            }
        });
        
        razlicite.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent e)
            {
                upisi (0);
            }
        });
       
        dalje.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent e)
            {
                upisi (10);
            }
        });
        
        this.setVisible(true);
        this.setLocationRelativeTo(null);
        this.addWindowListener(new WindowAdapter () {
            public void windowClosing (WindowEvent e)
            {
                if (vecOcenjenFajl)
                {
                    while (!listaParova.isEmpty())
                    {
                        String s = listaParova.remove(0);
                        listaParovaGraded.add(s);
                    }
                }
                zavrsi ();
            }
        });
    }
    
    private void upisi (int ocena)
    {
        String par = listaParova.get(0);
        String line = "";
        if (!vecOcenjenFajl)
            line = ocena + "\t" + par;
        else
        {
            String [] delovi = par.split("\t");
            line = ocena + "\t" + delovi[1] + "\t" + delovi[2];
        }
        listaParovaGraded.add(line);
        listaParova.remove(0);

        if (!vecOcenjenFajl && listaParova.isEmpty())
            zavrsi();
        else
        {
            String recenice = nadjiSledeciPar ();
            if (recenice == null)
            {
                JOptionPane.showMessageDialog(this, "Obrađene su sve rečenice!", "", JOptionPane.INFORMATION_MESSAGE);
                zavrsi();
            }
            else
            {
                String [] delovi = recenice.split("\t");
                if (!vecOcenjenFajl)
                {
                    recenica1.setText(delovi[0]);
                    recenica2.setText(delovi[1]);
                }
                else
                {
                    recenica1.setText(delovi[1]);
                    recenica2.setText(delovi[2]);
                }
            }
            naRedu++;
            info.setText("Linija " + naRedu + " od " + lineCnt);
        }
    }
    
    private void zavrsi ()
    {
        try
        {
            if (vecOcenjenFajl)
            {
                output.delete();
                output.createNewFile();
            }
            FileWriterES fwout = new FileWriterES (output);
            for (String s: listaParovaGraded)
                fwout.writeLine(s);
            fwout.close();
            
            if (!vecOcenjenFajl)
            {
                input.delete();
                input.createNewFile();
                FileWriterES fwin = new FileWriterES (input);
                for (String s: listaParova)
                    fwin.writeLine(s);
                fwin.close();
                if (listaParova.isEmpty())
                    JOptionPane.showMessageDialog(this, "Obrađene su sve rečenice!", "", JOptionPane.INFORMATION_MESSAGE);
            }
            
        }
        catch (IOException e) {}

        this.dispose();
    }

    private String nadjiSledeciPar ()
    {   
        String recenice = null;
        if (!vecOcenjenFajl && !listaParova.isEmpty())
            recenice = listaParova.get(0);
        else
        {
            while (listaParova.isEmpty() == false && !listaParova.get(0).startsWith("10"))
            {
                listaParovaGraded.add(listaParova.get(0));
                listaParova.remove(0);
                naRedu++;
            }
            if (!listaParova.isEmpty())
                recenice = listaParova.get(0);
        }
        
        return recenice;
    }
    
    public static void main (String [] args)
    {
        try {
            /*
            try {
                new ParaphraseGrader ();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ParaphraseGrader.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ParaphraseGrader.class.getName()).log(Level.SEVERE, null, ex);
            }
             * 
             */
            BufferedReader br = new BufferedReader (new InputStreamReader (new FileInputStream (new File ("ParaphraseTestSet.txt"))));

            FileWriterES fwes = new FileWriterES (new File ("ParaphraseTestSetClean.txt"));
        
        
        String line="";
        while ((line = br.readLine()) !=null)
        {
            String [] items = line.split("\t");
            line = items[1] + "\t" + items[2];
            fwes.writeLine(line);
        }
        
        br.close();
        fwes.close();
        }
        catch (IOException ex) {
            Logger.getLogger(ParaphraseGrader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
