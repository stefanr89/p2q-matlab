/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ucl.sspace.rs.etf.stemmer;

import edu.ucl.sspace.rs.etf.corpus.FileProcessingInfoES;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 *
 * @author Vuk Batanovic
 */
public class SerbianStemmer {
    
    private class StreamGobbler extends Thread
    {
        private InputStream is;
        private StringBuffer result;
        private FileProcessingInfoES processingInfo;
    
        StreamGobbler(InputStream is, File inputFile)
        {
            this.is = is;
            result = new StringBuffer();
            processingInfo = new FileProcessingInfoES (inputFile);
        }
    
        public void run()
        {
            try
            {
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader br = new BufferedReader(isr);
                String line="";
                while ( (line = br.readLine()) != null)
                {
                    result.append(line);  
                    processingInfo.printInfo(line);
                }
            } catch (IOException ioe)
              {
                ioe.printStackTrace();  
              }
        }

	public String getResult () { return result.toString();}
    }
    
    public String stem (File inputFile)
    {
        String result = "";
        Process process;
        try
        {
            process = Runtime.getRuntime().exec("cmd /c perl stemmer-opt.pl " + inputFile.getName());
            StreamGobbler sg = new StreamGobbler (process.getInputStream(), inputFile);
            sg.start();
            process.waitFor();
            if(process.exitValue() == 0)
            {
                System.out.println("\nCorpus stemming successful!\n");
                result = sg.getResult();
            }
            else
            {
                System.out.println("\nCorpus stemming failure!\n");
            }
        }
        catch(Exception e) {
            System.out.println("Exception: "+ e.toString());
        }
        return result;
    }
    
    public String stem (String str)
    {
        String result = "";
        Process process;
        try
        {
            process = Runtime.getRuntime().exec("cmd /c perl stemmer-opt-E.pl");
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter (process.getOutputStream()));
            bw.write(str + "\r\n");
            bw.flush();
            process.waitFor();
            if(process.exitValue() == 0)
            {
                System.out.println("\nLine stemming successful!\n");
                BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));
                result = br.readLine();
                System.out.println(result);
                bw.close();
                br.close();
            }
            else
            {
                System.out.println("\nLine stemming failure!\n");
            }
        }
        catch(Exception e) {
            System.out.println("Exception: "+ e.toString());
        }
        return result;
    }
     
}
