/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ucl.sspace.rs.etf.paraphrase;

import edu.ucl.sspace.rs.etf.corpus.ESPreprocessor;
import edu.ucl.sspace.rs.etf.corpus.FileWriterES;
import edu.ucl.sspace.rs.etf.corpus.SrCorpusProcessor;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author Vuk Batanovic
 */
public class NewsPageProcessor {
    
    public static void buildParaphraseCorpus (String paraphraseCorpusPath) throws IOException
    {
        File paraphraseCorpusFile = new File (paraphraseCorpusPath);
        if (paraphraseCorpusFile.exists())
        {
            paraphraseCorpusFile.delete();
            paraphraseCorpusFile.createNewFile();
        }
        FileWriterES fw = new FileWriterES(paraphraseCorpusFile);
        String baseURL = "http://www.vesti.rs/arhiva/2011/";
        for (int j=1; j<=7; j++)
        {
            String month = Integer.toString(j);
            if (j<10) month = "0" + month;
            int dayNo;
            if (j==2)
                dayNo = 28;
            else if (j==1 || j==3 || j==5 || j==7 || j==8 || j==10 || j==12)
                dayNo = 31;
            else
                dayNo = 30;
            
            for (int i=1; i<=dayNo; i++)
            {
                String day = Integer.toString(i);
                if (i<10) day = "0" + day;
                String fullURL = baseURL + month + "/" + day + "/";
                ArrayList<String> newsList = processImportantNewsPage(fullURL);
                for (String newsLink: newsList)
                {
                    Article article = processArticlePage(newsLink);
                    if (article != null && article.paraphraseNo() != 0)
                        fw.writeLine(article.getBestParaphrase().toString());
                }
                System.out.println ("Obrađen dan: 2011-" + month + "-" + day);
            }
        }
        fw.close();
    }
    
    /**
     * 
     * @param URL strane sa spiskom najvaznijih vesti za taj dan na www.vesti.rs
     * @return Lista URLova za svaku od najvaznijih vesti za taj dan
     * @throws IOException 
     */
    public static ArrayList<String> processImportantNewsPage (String page) throws IOException
    {
        ArrayList<String> list = new ArrayList<String> ();
        Document doc = null;
        boolean keepDoing = true;
        while (keepDoing)
        {
            try
            {
                doc = Jsoup.connect(page).get();
                keepDoing = false;
            }
            catch (IOException e) {}
        }
        Elements vestiKlasa = doc.getElementsByClass("tagclouds");
        Element vesti = vestiKlasa.first();
        Elements links = vesti.getElementsByTag("a");
        for (Element link : links) {
            String linkHref = link.attr("href");
            list.add(linkHref);
        }
        return list;
    }
    
    /**
     * 
     * @param page URL strane sa vescu na www.vesti.rs
     * @return Article - klasa koja sadrzi sve parafraze pronadjene u datom clanku
     * @throws IOException 
     */
    public static Article processArticlePage (String page)
    {
        ArrayList<String> list = new ArrayList<String> ();
        Document doc = null;
        boolean keepDoing = true;
        while (keepDoing)
        {
            try
            {
                doc = Jsoup.connect(page).get();
                keepDoing = false;
            }
            catch (IOException e) {}
        }
        Elements clanciKlasa = doc.getElementsByClass("full_article");
        int brClanaka = clanciKlasa.size();
        if (brClanaka == 1) return null;
        Elements vesti = doc.getElementsByTag("p");
        for (int i=0; i<brClanaka; i++)
            list.add(vesti.get(i).text().trim());
        
        ArrayList<String []> processedHeadlines = processArticleHeadline (list);
        return makeArticle(page, processedHeadlines);
    }
    
    /**
     * 
     * @param list Lista headline-ova za svaki izvor ove vesti
     * @return Niz parova stringova koji predstavljaju prve 2 recenice (ili samo 1 ako ih nema vise) iz svakog headline-a
     */
    private static ArrayList<String []> processArticleHeadline (ArrayList<String> list)
    {
        ArrayList<String[]> newList = new ArrayList<String[]> ();
        for (String str: list)
        {
            // Zamenjuje non-breakable space karakter obicnim razmakom radi onemogucivanja gresaka u daljem radu
            String cleared = str.replace('\u00A0', ' ').trim();
            String [] recenice = cleared.split("\\?!|!|\\.\\s|\\?|\\*");
            
            recenice = SentenceParser.removeEmptyStrings (recenice);
            recenice = SentenceParser.reattachBrokenSentences (recenice);
            
            String [] receniceOdInteresa = new String [2];
            int brojPronadjenih = 0;
            
            for (int i=0; i<recenice.length; i++)
            {
                String recenica = recenice[i];
                                
                recenica = recenica.trim();
                
                // Recenice koje se zavrsavaju ili pocinju tackama su skracene tako da se ne uzimaju u obzir
                if (recenica.endsWith("..")) continue;
                if (recenica.startsWith("..")) continue;
                if (recenica.endsWith(".")) recenica = recenica.substring(0, recenica.length()-1).trim();
                if (recenica.endsWith("(CH 1)") || recenica.endsWith("(CH 8)")) recenica = recenica.substring (0, recenica.length()-6);
                if (recenica.endsWith(".[ antrfile ]")) recenica = recenica.substring(0, recenica.length() - 13);
                                
                if (recenica.length() == 0) continue;
                
                
                // Uklanja /Vesti online/ i (vesti online) sa pocetka headline-a
                if (recenica.charAt(0) == '/' || recenica.charAt(0) == '(')
                {
                    int startIndex = -1;
                    for (int j=1; j<recenica.length(); j++)
                        if (recenica.charAt(j) == '/' || recenica.charAt(j) == ')')
                        {
                            startIndex = j;
                            break;
                        }
                    if (startIndex == -1) continue;
                    else
                        recenica = recenica.substring(startIndex+1).trim();
                }
                
                // Uklanja "Pretraga po temi" sufiks koji se javlja u pojedinim vestima
                if (recenica.contains("Pretraga po temi"))
                    recenica = recenica.substring(0, recenica.indexOf("Pretraga po temi"));
                
                // Uklanja "Vesti online/" prefiks koji nema pocetnu kosu crtu i koji se javlja u pojedinim vestima
                if (recenica.startsWith("Vesti online/"))
                    recenica = recenica.substring(13);
                
                String [] reci = recenica.split("\\s+");
                
                // Izbacivanje reci sa pocetka vesti koja govori o mestu desavanja
                if (reci.length >= 8)
                {
                    recenica = SentenceParser.removeSentenceDateSourcePlacePrefixes (recenica, reci);
                    reci = recenica.split(" ");
                }
                
                // Recenice krace od 8 reci su najcesce naslovi i ne uzimaju se u obzir
                if (reci.length < 8)
                    continue;
               
                receniceOdInteresa[brojPronadjenih++] = recenica;
                if (brojPronadjenih == 2) break;
            }
            if (receniceOdInteresa[0] != null)
            {
                if (receniceOdInteresa[1] == null)
                {
                    String [] temp = new String[1];
                    temp[0] = receniceOdInteresa[0];
                    receniceOdInteresa = temp;
                }
                newList.add(receniceOdInteresa);
            }
        }
        return newList;
    }
    
    
    
    private static Article makeArticle (String page, ArrayList<String []> list)
    {
        Article article = new Article (page);
        for (int i=0; i<list.size(); i++)
            for (int j=i+1; j<list.size(); j++)
            {
                String [] istr = list.get(i);
                String [] jstr = list.get(j);
                String str1 = istr[0];
                String str2 = jstr[0];
                processPotentialParaphrase(article, str1, str2);
                if (jstr.length > 1)
                {
                    str2 = jstr[1];
                    processPotentialParaphrase(article, str1, str2);
                }
                if (istr.length > 1)
                {
                    str1 = istr[1];
                    str2 = jstr[0];
                    processPotentialParaphrase(article, str1, str2);
                    if (jstr.length > 1)
                    {
                        str2 = jstr[1];
                        processPotentialParaphrase(article, str1, str2);
                    }
                }
            }
        
        return article;
    }
    
    private static void processPotentialParaphrase (Article article, String sentence1, String sentence2)
    {
        if (sentence1.equals(sentence2)) return;
        
        String srRecenica1 = SrCorpusProcessor.processSrString(sentence1.trim());
        String dpRecenica1 = ESPreprocessor.Singleton.INSTANCE.getSingleton().documentPreprocess(srRecenica1);
        String filtriranaRecenica1 = ESPreprocessor.Singleton.INSTANCE.getSingleton().filter(dpRecenica1, true, false);
        String srRecenica2 = SrCorpusProcessor.processSrString(sentence2.trim());
        String dpRecenica2 = ESPreprocessor.Singleton.INSTANCE.getSingleton().documentPreprocess(srRecenica2);
        String filtriranaRecenica2 = ESPreprocessor.Singleton.INSTANCE.getSingleton().filter(dpRecenica2, true, false);
        
        String [] words1 = filtriranaRecenica1.split("\\s");
        String [] words2 = filtriranaRecenica2.split("\\s");
        if (words1.length*0.5 > words2.length || words2.length*0.5 > words1.length)
            return;
        ArrayList<String> longWords1 = new ArrayList<String> ();
        ArrayList<String> longWords2 = new ArrayList<String> ();
        for (String word: words1)
            if (word.length()>=6) longWords1.add(word);
        for (String word: words2)
            if (word.length()>=6) longWords2.add(word);
        if (longWords1.size()*0.5 > longWords2.size() || longWords2.size()*0.5 > longWords1.size())
            return;
        if (longWords1.size() > 4 && longWords2.size() > 4)
        {
            int brReciKracaRecenica;
            int brReciDuzaRecenica;
            if (longWords1.size() > longWords2.size())
            {
                brReciDuzaRecenica = longWords1.size();
                brReciKracaRecenica = longWords2.size();
            }
            else
            {
                brReciDuzaRecenica = longWords2.size();
                brReciKracaRecenica = longWords1.size();
            }
            
            int brIstih = 0;
            for (String word1: longWords1)
                for (String word2: longWords2)
                    if (word1.equals(word2))
                    {
                        brIstih++;
                        break;
                    }


            // Ovde se radi o istoj recenici ponovljenoj dva puta sa nekom greskom u kucanju
            if (longWords1.size() == longWords2.size() && longWords1.size() == brIstih +1)
                return;
            
            int brIstihBezPonavljanja = 0;
            // Broji se koliko reci se javlja u obe recenice, ali sada bez ponavljanja
            for (int i=0; i<longWords1.size(); i++)
                for(int j=0; j<longWords2.size(); j++)
                {
                    String word1 = longWords1.get(i);
                    String word2 = longWords2.get(j);
                    if (word1.equals(word2))
                    {
                        brIstihBezPonavljanja++;
                        while (longWords1.remove(word1));
                        while (longWords2.remove(word2));
                        i--;
                        break;
                    }
                }

            
            if (brIstih >= 4 && brIstih >= 0.15*brReciKracaRecenica && brIstih <= 0.85*brReciKracaRecenica)
                article.addParaphrase(sentence1, sentence2, brIstihBezPonavljanja, brReciKracaRecenica, brReciDuzaRecenica);
        }
    }
}
