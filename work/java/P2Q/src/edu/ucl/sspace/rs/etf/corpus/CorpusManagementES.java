/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.ucl.sspace.rs.etf.corpus;


import edu.ucl.sspace.rs.etf.stemmer.SerbianStemmer;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author Davor
 */
public class CorpusManagementES {

    /* Ocisti korpus(! koji je u cistom txt formatu)       */
    public static void cleanCorpus(File inputCorpusFile,File outputCorpusFile){
       try{
           // Inicijalizacija
           FileProcessingInfoES fpi=new FileProcessingInfoES(inputCorpusFile);
           FileWriterES fw=new FileWriterES(outputCorpusFile);
           FileReaderES fr=new FileReaderES(inputCorpusFile);
           
           // DODATO - izbacivanje stop reci
            ESPreprocessor.Singleton.INSTANCE.getSingleton().printStopWords();

           String line="";
           while((line=fr.readLine())!=null){
                              
               // filtriranje

               /*
               
               line = ESPreprocessor.Singleton.INSTANCE.getSingleton().filter(line, false, false);
               
               
              // if (line.split(" ").length < 10) continue; // izbacuju se linije u kojima postoji manje od 10 reci
               
               // DODATO - izbacivanje automatski generisanog teksta
               if ((line.startsWith("slika") || line.startsWith("image")) && line.endsWith("jpg")) continue;
               if (line.contains("je grad u nyemacxkoj saveznoj drzxavi")) continue;
               if (line.contains("je opsxtina u nyemacxkoj saveznoj drzxavi")) continue;
               if (line.contains("mjesto u nyemacxkoj saveznoj drzxavi")) continue;
               if (line.contains("je naselyeno mjesto na podrucxju grada")) continue;
               if (line.contains("je naselyeno mjesto u opsxtini")) continue;
               if (line.contains("je naselyeno mesto u opsxtini")) continue;
               if (line.contains("su naselyeno mjesto u opsxtini")) continue;
               if (line.contains("su naselyeno mesto u opsxtini")) continue;
               if (line.contains("je naselye u opsxtini")) continue;
               if (line.contains("je naselyeno mjesto u sastavu")) continue;
               if (line.contains("je naselyeno mesto u sastavu")) continue;
               if (line.contains("su naselyeno mjesto u sastavu")) continue;
               if (line.contains("su naselyeno mesto u sastavu")) continue;
               if (line.contains("sa srednyom udalyenosxcyu od sunca koja iznosi astronomskih jedinica aj")) continue;
               if (line.contains("je asteroid glavnog asteroidnog pojasa")) continue;
               if (line.contains("mozxe da se odnosi na objekat dubokog neba")) continue;
               if (line.contains("mozxe da oznacxava ngc objekte dubokog neba")) continue;
               if (line.contains("je jedan od preko sovjetskih vjesxtacxkih satelita lansiranih u okviru programa kosmos")) continue;
               if (line.contains("namijenyena za istrazxivanye planete venere lansirana je")) continue;
               if (line.startsWith("nato oznaka ss")) continue;
               if (line.contains("prema popisu iz")) continue;
               if ((line.contains("francuskoj u regionu") || line.contains("francuske u regionu"))
                       && (line.contains("je naselyeno mesto") || line.contains("je naselye i opsxtina")))
                   continue;
               if (line.contains("do kraja godine ima josx dan")) continue;
               if (line.contains("po poslednyem sluzxbenom popisu")) continue;
               if (line.contains("jugoslovenski") && line.contains("film iz godine") && (line.split(" ").length <= 10)) continue;
               if (line.startsWith("opsxtina") && (line.contains("srbiji") || line.contains("srbije") || line.contains("je jedna od opsxtina"))) continue;
               if (line.contains("je selo u makedoniji nalazi se")) continue;
               if (line.contains("je selo u opsxtini")) continue;
               if (line.contains("je selo u polyskoj koje se nalazi u vojvodstvu")) continue;
               if (line.contains("nenaselyeno ostrvce u hrvatskom dijelu jadranskog mora")) continue;
               if (line.contains("mozxe da se odnosi na")) continue;
               if (line.contains("se mozxe odnositi na") || line.contains("mozxe se odnositi na")) continue;
               if (line.contains("mozxe da oznacxava")) continue;
               if (line.contains("oblasti u republici makedoniji")) continue;
               if (line.startsWith("selo") && line.contains("se nalazi u opsxtini") && line.split(" ").length <=15) continue;
               if (line.contains("naselyeno mesto u rumuniji")) continue;
               if (line.contains("je naselyeno mesto u republici hrvatskoj")) continue;
               if (line.contains("naselye") &&
                       (line.contains("hrvatska pripada") || line.contains("hrvatska nekada je pripada"))) continue;
               if (line.contains("jeziku je verzija vikipedije na")) continue;
               if (line.contains("je okrug u republici letoniji") || line.contains("je okrug u republici litvaniji") ||
                       line.contains("je okrug u republici rumuniji") || line.contains("italiji sedisxte okruga")) continue;
               if (line.contains("entitetu federacija bosne i hercegovine")) continue;
               if (line.contains("jedna od oblasti albanije")) continue;
               if (line.contains("do nove teritorijalne organizacije")) continue;
               if (line.contains("prva savezna liga jugoslavije")) continue;
               if (line.contains(" pripada regiji u republici sloveniji")) continue;
               */
               
               //line = ESPreprocessor.Singleton.INSTANCE.getSingleton().filter(line, false, true);
        	   line = ESPreprocessor.Singleton.INSTANCE.getSingleton().filter(line, false, true);
               if (line.split(" ").length < 2) continue; // izbacuju se linije u kojima postoji manje od 2 reci
               
               /*
               String [] strValues = ESPreprocessor.filter (line, false, false);
               //if (strValues.length < 2) continue;  // izbacuju se linije u kojima postoji samo jedna rec
               
               line="";
               for(int i=0;i<strValues.length;i++){
                   if((i+1)==strValues.length) line+=strValues[i];
                   else line+=strValues[i]+" ";
               }
                */ 
                
               if(line.length()>0) 
               {
                   fw.writeLine(" " + line);
                   // koliko je procesirano
                   fpi.printInfo(line);
               }
               
               
           }

           fw.close();
           System.out.println ("\nCorpus cleaning successful!\n");

       }catch(Exception ex){
           ex.printStackTrace();
       }
    }

    // uradi stemming nad korpusom
    public static void stemCorpus(File inputCorpusFile,File outputCorpusFile){
       try{
           // Inicijalizacija
           PrintWriter pw = new PrintWriter (new FileWriter (outputCorpusFile));
           SerbianStemmer serbStem = new SerbianStemmer();
           String result = serbStem.stem(inputCorpusFile);
           pw.print(result);
           pw.flush();
           pw.close();
       }catch(Exception ex){
           ex.printStackTrace();
       }
    }


    public static void makeSmallerCorpus(File inputCorpusFile,File outputCorpusFile,int numOfMB,int option) throws FileNotFoundException, IOException{
        FileReaderES fr=new FileReaderES(inputCorpusFile);
        // Create file name depend of option: stemmed or no
  
        FileWriterES fw=new FileWriterES(outputCorpusFile);
        FileProcessingInfoES fp=new FileProcessingInfoES(inputCorpusFile);


        String line="";
        while((line=fr.readLine())!=null){
            if(option==1){
                
                line = ESPreprocessor.Singleton.INSTANCE.getSingleton().filter(line, false, false); 
                /*
                String[] valueStrs = ESPreprocessor.filter(line,false,false);  // clean+withoutnumbers
               
                // and recombine all stemmed words
                line="";
                for(int i=0;i<valueStrs.length;i++){
                    if((i+1)!=valueStrs.length) line+=valueStrs[i]+" ";
                    else line+=valueStrs[i]; // the last word
                }
                 */ 
                
            } 
            // else: do nothing(no stemming) with line
            if(line.length()>0) fw.writeLine(line);
            fp.printInfo(line);
            if(fp.getNumOfMB()>=numOfMB) break;
        }
    }

    public static void createLSAFiles(File corpusFile,int numOfFiles,int option) throws FileNotFoundException, IOException{
        long size=corpusFile.length();
        long sizePerFile=size/numOfFiles;      // in [B]- num of Bytes per file
        long numOfKBPerFiles=sizePerFile/1024; // num of KB per files

        FileReaderES fr=new FileReaderES(corpusFile);
        String line=null;
        int num=0;

        // construct LSA file name
        // LSA_num.txt
        // and prepare for writting
        String lsaFileName="LSA_"+num+".txt";
        File outputFile=new File(lsaFileName);
        FileWriterES fw=new FileWriterES(outputFile);

        int KB=0;

        while((line=fr.readLine())!=null){
            if(numOfKBPerFiles<=0){
                // Print info that lsa file has been created
                System.out.println("LSA file name: "+lsaFileName+" has been created");
                // reset numOfKBPF
                numOfKBPerFiles=sizePerFile/1024;
                // close prev.file
                fw.close();
                // construct new name
                num++;
                lsaFileName="LSA_"+num+".txt";
                // new file(initialize it)
                outputFile=new File(lsaFileName);
                fw=new FileWriterES(outputFile);
            }

            // if clean + stemm word option is included
            if(option==1){
                
                line = ESPreprocessor.Singleton.INSTANCE.getSingleton().filter(line, true, false); 
                /*
                String[] valueStrs = ESPreprocessor.Singleton.INSTANCE.getSingleton()..filter(line,true,false); // clean
               
                // and recombine all stemmed words
                line="";
                for(int i=0;i<valueStrs.length;i++){
                    if((i+1)!=valueStrs.length) line+=valueStrs[i]+" ";
                    else line+=valueStrs[i]; // the last word
                }
                 */ 
                 
            }

            // write to to lsa file(evaluate option if it is need for stemming)
            // stop words must be excluded(do it latter)
            KB+=line.length();
            if(KB>=1024){
                KB=0;
                numOfKBPerFiles--;
            }
        

            if(line.length()>0) fw.writeLine(line);

        }



    }

    public static void main(String[] args) throws FileNotFoundException, IOException{
        /* // OLD
        // make files of 20,40,60,80,100[MB]
         int i=100;
         File inputCorpusFile=new File("corpusfinal.txt");
         //File outputCorpusFile=new File("corpusfinal.txt");
         CorpusManagementES.createLSAFiles(inputCorpusFile,200,0);
         */ 
        
         
         File inputCorpusFile = new File ("srwiki-20110620-abstract.xml");
         File outputCorpusFile = new File ("preradjen_korpus.txt");
         cleanCorpus (inputCorpusFile, outputCorpusFile);
    }



}
