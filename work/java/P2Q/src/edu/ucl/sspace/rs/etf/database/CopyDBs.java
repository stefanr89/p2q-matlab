package edu.ucl.sspace.rs.etf.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * 
 * @author Bojan Furlan
 *
 */


public class CopyDBs {
	static void copyFromMySQLToMSSQLForConceptExtraction() {


		try {
			Connection conConcept_Extraction = ESConnection.getConnection("Concept_Extraction");
			Connection conInterestminingL61k = ESConnection.getConnection("interestminingl6typeALL");

			PreparedStatement stmt = conInterestminingL61k.prepareStatement("SELECT id, CONCAT(title, ' ', body) as txt FROM Posts WHERE postTypeId=1 ORDER BY id ASC;");
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				PreparedStatement stmt1 = conConcept_Extraction.prepareStatement("INSERT INTO Questions (text, wordCount, source) VALUES (?, ?, 'L6')");
				System.out.println("%s - %s".format(rs.getString(1), rs.getString(2)));
				stmt1.setString(1, rs.getString(2));
				stmt1.setInt(2, rs.getString(2).split(" ").length);
				stmt1.executeUpdate();
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void main(String[] args) {
		//copyFromMySQLToMSSQLForConceptExtraction();
		//copyFromConceptExtractionToL6ALL();
		rescueCopyFromConceptExtractionToL6ALL();
	}



	public static void copyFromConceptExtractionToL6ALL() {
	
		try {
			Connection conConcept_Extraction = ESConnection.getConnection("Concept_Extraction");
			Connection conInterestminingL6typeALL = ESConnection.getConnection("interestminingl6typeALL");
			String mySQLDb = "interestminingl6typeALL";

		    //map indexes
			ArrayList<String> realQuestionIndexes = new ArrayList<String>();
			PreparedStatement stmt = conInterestminingL6typeALL.prepareStatement("SELECT id FROM Posts WHERE postTypeId = 2 ORDER BY id DESC;");  // za rescue treba poceti od 16030 i uzeti jos 2986
			ResultSet rs = stmt.executeQuery();
		    while (rs.next()) {
		      realQuestionIndexes.add(rs.getString(1));
		    }

		    ArrayList<Integer> copiedQuestionIndexes = new ArrayList<Integer>();
		    PreparedStatement stmt1 = conConcept_Extraction.prepareStatement("SELECT idQuestion FROM Questions WHERE source = 'L6-typeALL-Answers' ORDER BY idQuestion DESC;");
		    ResultSet rs1 = stmt1.executeQuery();
		    while (rs1.next()) {
		      copiedQuestionIndexes.add(Integer.parseInt(rs1.getString(1)));
		    }

		    if (realQuestionIndexes.size() != copiedQuestionIndexes.size()) {
		        System.out.println(String.format("Indexes do not match! Real: %d, Copied: %d",realQuestionIndexes.size(), copiedQuestionIndexes.size()));
		      }

		    
		    //transfer
		    PreparedStatement stmt2 = conConcept_Extraction.prepareStatement("SELECT * FROM dbo.Concepts WHERE engine = 'ConceptExtractor-L6-typeAll-Answers';");
		    ResultSet rs2 = stmt2.executeQuery();

		    while (rs2.next()) {
		      String postId = realQuestionIndexes.get(copiedQuestionIndexes.indexOf(rs2.getInt("idQuestion")));
		      PreparedStatement stmt3 = conInterestminingL6typeALL.prepareStatement("SELECT ownerUserId FROM Posts WHERE id = ?;");
		      stmt3.setString(1, postId);
		      ResultSet rs3 = stmt3.executeQuery();
		      rs3.next();
		      String userId = rs3.getString(1);

		      PreparedStatement stmt4 = conInterestminingL6typeALL.prepareStatement("INSERT INTO Evidence (userId, keyword, informationSourceId, trust, weight, typeOfEvidence)"
		      		+ "VALUES (?, ?, 'ConceptExtractor', '1.0,1.0,1.0', ? , 'Question')" );
				stmt4.setString(1, userId);
				stmt4.setString(2, rs2.getString("text"));
				stmt4.setDouble(3, rs2.getDouble("weight"));
				//stmt4.setString(4, postId);
				stmt4.executeUpdate();
				
			    PreparedStatement stmt5 = conInterestminingL6typeALL.prepareStatement("insert into EvidencePost (idEvidence, idPost) values (@@IDENTITY, ?);");
						stmt5.setString(1, postId);
						stmt5.executeUpdate();
		    }


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void rescueCopyFromConceptExtractionToL6ALL() {
		
		try {
			Connection conConcept_Extraction = ESConnection.getConnection("Concept_Extraction");
			Connection conInterestminingL6typeALL = ESConnection.getConnection("interestminingl6typeALL");			
			
		    //map indexes
			//HashMap<String,String> realQuestionIndexes = new HashMap<String,String>();
			PreparedStatement stmt = conInterestminingL6typeALL.prepareStatement(
					"SELECT distinct * "
					+ "FROM "
					+ "(SELECT  * "
					+ "FROM interestminingL6typeALL.dbo.Posts "
					+ "where postTypeId = 2 "
					+ "and id not in ( "
					+ "select distinct idPost from interestminingL6typeALL.dbo.EvidencePost inner join interestminingL6typeALL.dbo.Evidence "
					+ "on id = idEvidence "
					+ "where informationSourceId = 'ConceptExtractor' "
					+ ")) as NotProcessedPosts, "
					+ "(select RTRIM(LTrim(text)) as aText, min(idQuestion) as idQuestion "
					+ "from Concept_Extraction.dbo.Questions "
					+ "where idQuestion BETWEEN 27140 AND 30126 "
					+ "and source = 'L6-typeAll-Answers' "
					+ "and wordCount >2 "
					+ "group by RTRIM(LTrim(text)) ) as DistinctCnetProecessedAnswers "
					+ "where DATALENGTH(RTRIM(LTrim(aText)))>7 "
					+ "and RTRIM(LTrim(aText)) collate Latin1_General_CI_AI = RTRIM(LTrim(body)) collate Latin1_General_CI_AI ;");  // za rescue treba poceti od 16030 i uzeti jos 2986
			ResultSet rs = stmt.executeQuery();
		    while (rs.next()) {
		      //realQuestionIndexes.put(rs.getString("idQuestion"),rs.getString("id"));
		      
		      String idQuestion = rs.getString("idQuestion");
		      String postId = rs.getString("id");
		    
		      PreparedStatement stmt3 = conInterestminingL6typeALL.prepareStatement("SELECT ownerUserId FROM Posts WHERE id = ?;");
		      stmt3.setString(1, postId);
		      ResultSet rs3 = stmt3.executeQuery();
		      rs3.next();
		      String userId = rs3.getString(1);

				////dohvati sve concepte za taj tekst
				PreparedStatement stmt2 = conConcept_Extraction.prepareStatement(""
						+ "SELECT c.* "
						+ "FROM Concepts c "
						+ "WHERE "
						+ "engine = 'ConceptExtractor-L6-typeAll-Answers' "
						+ "and c.idQuestion = ? ;");
				stmt2.setString(1, idQuestion);

				ResultSet rs2 = stmt2.executeQuery();

				//ubaci

				while (rs2.next()) {

					PreparedStatement stmt4 = conInterestminingL6typeALL.prepareStatement("INSERT INTO Evidence (userId, keyword, informationSourceId, trust, weight, typeOfEvidence)"
							+ "VALUES (?, ?, 'ConceptExtractor', '1.0,1.0,1.0', ? , 'Answer')" );
					stmt4.setString(1, userId);
					stmt4.setString(2, rs2.getString("text"));
					stmt4.setDouble(3, rs2.getDouble("weight"));
					//stmt4.setString(4, postId);
					stmt4.executeUpdate();

					System.out.println(postId + " " + userId);
					PreparedStatement stmt5 = conInterestminingL6typeALL.prepareStatement("insert into EvidencePost (idEvidence, idPost) values (@@IDENTITY, ?);");
					stmt5.setString(1, postId);
					stmt5.executeUpdate();
				}
	
//			    //transfer
//			    PreparedStatement stmt2 = conConcept_Extraction.prepareStatement("SELECT RTRIM(LTrim(q.text)) as aText, c.* "
//			    		+ "FROM Concepts c join Questions q on c.idQuestion = q.idQuestion "
//						+ "WHERE "
//						+ "engine = 'ConceptExtractor-L6-typeAll-Answers' "
//						+ "and c.idQuestion = "
//						+ "(select top 1 idQuestion "
//						+ "from Questions "
//						+ "where idQuestion BETWEEN 27140 AND 30126 "
//						+ "and source = 'L6-typeAll-Answers' "
//						+ "and wordCount >2 ;");
//			    ResultSet rs2 = stmt2.executeQuery();
//
//		      
//		    }
//		    
//
////		    while (rs2.next()) {
////		      String postId = realQuestionIndexes.get(rs2.getString("aText"));
////
////		      if(postId == null) {
////		    	  System.out.println("No post in Interest mining: "+rs2.getString("aText"));
////		    	  continue;
////		      }
//
//		      PreparedStatement stmt4 = conInterestminingL6typeALL.prepareStatement("INSERT INTO Evidence (userId, keyword, informationSourceId, trust, weight, typeOfEvidence)"
//		      		+ "VALUES (?, ?, 'ConceptExtractor', '1.0,1.0,1.0', ? , 'Answer')" );
//				stmt4.setString(1, userId);
//				stmt4.setString(2, rs2.getString("text"));
//				stmt4.setDouble(3, rs2.getDouble("weight"));
//				//stmt4.setString(4, postId);
//				stmt4.executeUpdate();
//				
//				System.out.println(postId + " " + userId);
//			    PreparedStatement stmt5 = conInterestminingL6typeALL.prepareStatement("insert into EvidencePost (idEvidence, idPost) values (@@IDENTITY, ?);");
//						stmt5.setString(1, postId);
//						stmt5.executeUpdate();

		    }


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


}
