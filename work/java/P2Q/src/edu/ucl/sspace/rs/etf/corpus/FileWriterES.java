/*
 * to make easier file writing
 * 
 */

package edu.ucl.sspace.rs.etf.corpus;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

/**
 *
 * @author Davor Jovanovic, ES
 */
public class FileWriterES {
    
    private Writer output=null;
    
    public FileWriterES(File file) throws IOException{
        output = new BufferedWriter(new FileWriter(file,true));  // append to file(true)
    }


    public void writeLine(String line){
        try{
            output.write(line);
            output.write("\r\n");
        }catch(Exception ex){}


    }

    public void close() throws IOException{
        if(output!=null) output.close();
    }

}
