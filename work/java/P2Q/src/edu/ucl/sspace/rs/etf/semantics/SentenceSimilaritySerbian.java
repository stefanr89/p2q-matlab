/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ucl.sspace.rs.etf.semantics;

import edu.ucl.sspace.rs.etf.corpus.ESPreprocessor;
import edu.ucl.sspace.rs.etf.corpus.SrCorpusProcessor;
import edu.ucla.sspace.text.DocumentPreprocessor;
import java.io.IOException;

/**
 *
 * @author Vuk Batanovic
 */
public class SentenceSimilaritySerbian extends Thread{
    
    public void getSimilarity(String sent1,String sent2) throws IOException // static double
    {
        sent1 = SrCorpusProcessor.processSrString(sent1);
        DocumentPreprocessor dp=new DocumentPreprocessor();
        sent1 = dp.process(sent1);
        sent1 = ESPreprocessor.Singleton.INSTANCE.getSingleton().filter(sent1, false, true);
        sent1 = ESPreprocessor.Singleton.INSTANCE.getSingleton().stemSerbian(sent1);
        
        sent2 = SrCorpusProcessor.processSrString(sent2);
        dp=new DocumentPreprocessor();
        sent2 = dp.process(sent2);
        sent2 = ESPreprocessor.Singleton.INSTANCE.getSingleton().filter(sent2, false, true);
        sent2 = ESPreprocessor.Singleton.INSTANCE.getSingleton().stemSerbian(sent2);
        
        //return SentenceSimilarity.getSimilarity(sent1.split(" "), sent2.split(" "));
        result = SentenceSimilarity.getSimilarity(sent1.split(" "), sent2.split(" "));
    }
    
    private String initialGrade;
    private String sent1, sent2;
    private String originalSent1, originalSent2;
    private int lineCnt;
    private double result = 0;
    private String [] stringVector;
    private int [] currentThreads;
    
    public SentenceSimilaritySerbian (String initialGrade, String sent1, String sent2, int lineCnt, String [] stringVector, int [] currentThreads)
    {
    	this.initialGrade = initialGrade;
    	this.sent1 = sent1;
    	this.sent2 = sent2;
    	originalSent1 = sent1;
    	originalSent2 = sent2;
    	this.lineCnt = lineCnt;
    	this.stringVector = stringVector;
    	this.currentThreads = currentThreads;
    }

	@Override
	public void run() {
		try {
			getSimilarity(sent1, sent2);
			String newLine = initialGrade + "\t" + result + "\t" + originalSent1 + "\t" + originalSent2 +"\r\nRAM";
			stringVector[lineCnt-1] = newLine;
            System.out.println ("Line: " + lineCnt);
            System.out.println (newLine);
			synchronized (currentThreads)
			{
				currentThreads[0]--;
			}
			synchronized (stringVector)
			{
				stringVector.notifyAll();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
