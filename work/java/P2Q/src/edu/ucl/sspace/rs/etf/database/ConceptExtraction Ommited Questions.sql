/****** Script for SelectTopNRows command from SSMS  ******/
use Concept_Extraction

---uporedi text
SELECT *
  FROM [Concept_Extraction].[dbo].[Questions] cnet, interestminingL6typeALL.dbo.posts interest
  where source = 'L6-typeALL-Answers' and LTRIM(body) collate SQL_Latin1_General_CP1_CI_AS =  LTRIM(text) collate SQL_Latin1_General_CP1_CI_AS  -- like '%I would say Spanish...French are passionate just like spaniards and their joy of eating is similar to that of spaniards%'
and body <> ''
and idQuestion >27140 and idQuestion <27145    --30126 
 

---uporedi text
insert into [Concept_Extraction].[dbo].Missing
SELECT distinct OriginalID
  FROM [Concept_Extraction].[dbo].Rescue r
  where OriginalID not in (
SELECT distinct OriginalID
  FROM [Concept_Extraction].[dbo].Rescue r, interestminingL6typeALL.dbo.posts interest
  where OriginalID collate SQL_Latin1_General_CP1_CI_AS = id collate SQL_Latin1_General_CP1_CI_AS and postTypeId='1'
)
-- id=27140 

use interestminingL6typeALL
SELECT *
  FROM Posts
  where  LTrim(body) = 'I would say Spanish...French are passionate just like spaniards and their joy of eating is similar to that of spaniards.' --'you are from spain!'  -- '%you are from spain!' -- %French and Spanish are both Romance languages.' --%I would say Spanish...French are passionate just like spaniards and their joy of eating is similar to that of spaniards%'
-- id=27140
-- id = 1879659a1

  SELECT MIN(idQuestion) ,max(idQuestion), max(idQuestion) - MIN(idQuestion) as ukupno, COUNT(*)  
  FROM [Concept_Extraction].[dbo].[Questions]
  where source = 'L6-typeALL-Answers'
-- minId = 11110	maxId = 49143	ukupno = 38034

-- k = 38034/4= 9508,5 
-- za log1 treba izvrsiti 20618 do 30126
-- stao 27140 fali jos 2986


SELECT *
  FROM [Concept_Extraction].[dbo].Concepts
  where source = 'L6-typeALL-Answers' -- and

use [Concept_Extraction]
SELECT count(distinct idQuestion)
  FROM [Concept_Extraction].[dbo].[Concepts]
  where engine = 'ConceptExtractor-L6-typeALL-Answers' and idQuestion >27140 and idQuestion <30127

  -- nadji iste tekstove za obradu !!! 
  use [Concept_Extraction]
  SELECT * --count(distinct q1.idQuestion)
  FROM Questions q1, Questions q2
  WHERE q2.source = 'L6-typeAll-Answers' and LTrim(q1.text) = LTrim(q2.text) and q1.idQuestion <> q2.idQuestion

  -- id od koga pocinje

  use interestminingL6typeALL

  select min(id) FROM Posts where postTypeId = '2'


SELECT *
  FROM Posts
  where id = (27140 -11110) + (select min(id) FROM Posts where postTypeId = '2')
  and postTypeId = '2'
  
 -- LTrim(body) = 'you are from spain!'  -- '%you are from spain!' -- %French and Spanish are both Romance languages.' --%I would say Spanish...French are passionate just like spaniards and their joy of eating is similar to that of spaniards%'
-- id=27140
-- id = 1879659a1

  
-- oni koji su obrisani
 SELECT TOP 1000 *
  FROM [interestminingL6].[dbo].[Posts]
  where id in (select OriginalID collate SQL_Latin1_General_CP1_CI_AS from Concept_Extraction.dbo.Missing)



GO
use interestminingL6typeALL
SELECT id FROM Posts WHERE postTypeId = 2 ORDER BY id DESC;

use Concept_Extraction

-- dohvati sve koji imaju pitanje koje je izbrisano i nisu procesirani
WITH OrderedPosts AS
(
	--postavi ROW_NUMBER
    SELECT *, ROW_NUMBER() OVER (ORDER BY id DESC) AS RowNumber
    FROM interestminingL6typeALL.dbo.Posts
	WHERE postTypeId = 2
) 
SELECT * -- min(idQuestion), Max(idQuestion) -- *
FROM OrderedPosts, Concept_Extraction.dbo.missing, Concept_Extraction.dbo.Questions 
WHERE parentId = OriginalID collate SQL_Latin1_General_CP1_CI_AS and RowNumber BETWEEN 16030 AND 19016
and source = 'L6-typeAll-Answers' and LTrim(text) collate SQL_Latin1_General_CP1_CI_AS = LTrim(body) collate SQL_Latin1_General_CP1_CI_AS 
--and idQuestion BETWEEN 27140 AND 30126

--nema ovakvih


-- dohvati sve koji imaju pitanje koje je izbrisano i nisu procesirani
WITH OrderedPosts AS
(
	--postavi ROW_NUMBER
    SELECT *, ROW_NUMBER() OVER (ORDER BY id DESC) AS RowNumber
    FROM interestminingL6typeALL.dbo.Posts
	WHERE postTypeId = 2
) 
SELECT distinct * -- min(idQuestion), Max(idQuestion) -- *
FROM OrderedPosts, 
(select distinct RTRIM(LTrim(text)) from Concept_Extraction.dbo.Questions 
where idQuestion BETWEEN 27140 AND 30126
and source = 'L6-typeAll-Answers'
and wordCount >2) as Cnet
WHERE RowNumber BETWEEN 16030 AND 23072 --19016 --BETWEEN 20086 AND 23072
 and DATALENGTH(RTRIM(LTrim(text)))>7 
and RTRIM(LTrim(text)) collate SQL_Latin1_General_CP1_CI_AS = RTRIM(LTrim(body)) collate SQL_Latin1_General_CP1_CI_AS 
--order by RowNumber




--and LTrim(body) = 'I would say Spanish...French are passionate just like spaniards and their joy of eating is similar to that of spaniards.' 


-- dohvati sve koji imaju pitanje koje je izbrisano i nisu procesirani
WITH OrderedPosts AS
(
	--postavi ROW_NUMBER
    SELECT *, ROW_NUMBER() OVER (ORDER BY id DESC) AS RowNumber
    FROM interestminingL6typeALL.dbo.Posts
	WHERE postTypeId = 2
) 
SELECT * -- min(idQuestion), Max(idQuestion) -- *
FROM OrderedPosts
WHERE RowNumber BETWEEN 16030 AND 19016



SELECT distinct * 
FROM 
(SELECT  * 
FROM interestminingL6typeALL.dbo.Posts
where postTypeId = 2
and id not in (
select distinct idPost from interestminingL6typeALL.dbo.EvidencePost inner join Evidence on id = idEvidence where informationSourceId = 'ConceptExtractor'
)) as NotProcessedPosts,
(select distinct RTRIM(LTrim(text)) as aText from Concept_Extraction.dbo.Questions 
where idQuestion BETWEEN 27140 AND 30126
and source = 'L6-typeAll-Answers'
and wordCount >2) as DistinctCnetProecessedAnswers
where DATALENGTH(RTRIM(LTrim(aText)))>7 
and RTRIM(LTrim(aText)) collate Latin1_General_CI_AI = RTRIM(LTrim(body)) collate Latin1_General_CI_AI 

select * 
from
(select distinct RTRIM(LTrim(text)) as aText from Concept_Extraction.dbo.Questions 
where idQuestion BETWEEN 27140 AND 30126
and source = 'L6-typeAll-Answers'
and wordCount >2) as DistinctCnetProecessedAnswers
inner join Concept_Extraction.dbo.Questions 
on atext = RTRIM(LTrim(text))


use interestminingL6typeALL
SELECT  * -- min(idQuestion), Max(idQuestion) -- *
FROM interestminingL6typeALL.dbo.Posts
where postTypeId = 2
and id not in (
select distinct idPost from interestminingL6typeALL.dbo.EvidencePost inner join Evidence on id = idEvidence where informationSourceId = 'ConceptExtractor'
)
and LTrim(body) = 'I would say Spanish...French are passionate just like spaniards and their joy of eating is similar to that of spaniards.'

use Concept_Extraction
SELECT c.* 
FROM Concepts c 
WHERE 
engine = 'ConceptExtractor-L6-typeAll-Answers' 
and c.idQuestion = 
(select top 1 idQuestion
from Questions
where idQuestion BETWEEN 27140 AND 30126
and source = 'L6-typeAll-Answers'
and wordCount >2
and RTRIM(LTrim(text)) = 'you are from spain!' -- 'I would say Spanish...French are passionate just like spaniards and their joy of eating is similar to that of spaniards.'
)

SELECT  id FROM interestminingL6typeALL.dbo.Posts where postTypeId = 2 and id not in ( select distinct idPost from interestminingL6typeALL.dbo.EvidencePost inner join interestminingL6typeALL.dbo.Evidence on id = idEvidence where informationSourceId = 'ConceptExtractor' and DATALENGTH(RTRIM(LTrim(aText)))>7 and RTRIM(LTrim(body)) = ? ;

select *
from [Concept_Extraction].[dbo].[Concepts]
WHERE engine = 'ConceptExtractor-L6-typeALL-Answers'
and idQuestion BETWEEN 27140 AND 30126
and idQuestion in (
-- dohvati sve ne procesirane postove koji su isti kao oni u ConceptExtraction
SELECT idQuestion --distinct * 
					FROM 
					(SELECT  * 
					FROM interestminingL6typeALL.dbo.Posts
					where postTypeId = 2
					and id not in (
					select distinct idPost from interestminingL6typeALL.dbo.EvidencePost inner join interestminingL6typeALL.dbo.Evidence 
					on id = idEvidence 
					where informationSourceId = 'ConceptExtractor'
					)) as NotProcessedPosts,
					(
					select RTRIM(LTrim(text)) as aText, min(idQuestion) as idQuestion 
					from Concept_Extraction.dbo.Questions 
					where idQuestion BETWEEN 27140 AND 30126
					and source = 'L6-typeAll-Answers'
					and wordCount >2
					group by RTRIM(LTrim(text)) 
					) as DistinctCnetProecessedAnswers
					where DATALENGTH(RTRIM(LTrim(aText)))>7 
					and RTRIM(LTrim(aText)) collate Latin1_General_CI_AI = RTRIM(LTrim(body)) collate Latin1_General_CI_AI

)

SELECT RTRIM(LTrim(q.text)) as aText, c.* 
FROM Concepts c join Questions q on c.idQuestion = q.idQuestion 
WHERE engine = 'ConceptExtractor-L6-typeAll-Answers' 
and q.idQuestion = 
(select top 1 idQuestion from Questions q1
 where idQuestion BETWEEN 27140 AND 30126 and source = 'L6-typeAll-Answers' and wordCount >2) 

