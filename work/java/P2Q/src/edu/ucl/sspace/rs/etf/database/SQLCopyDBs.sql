/****** Script for SelectTopNRows command from SSMS  ******/




use Concept_Extraction
SELECT count(*) FROM dbo.Concepts WHERE engine = 'ConceptExtractor-L6-typeAll-Questions'
-- 22913

use interestminingL6typeALL
  SELECT count(*)
  FROM Evidence e, [EvidencePost] ep
  WHERE e.[typeOfEvidence]='Question' and  e.[informationSourceId]= 'ConceptExtractor' and ep.idEvidence = e.id 
  -- 22 913
 
 SELECT count(*)
  FROM [Concept_Extraction].[dbo].Concepts
  WHERE engine = 'ConceptExtractor-L6-typeAll-Questions' 
  group by idQuestion
  --22913

 
 --Test idenciticnosti - Promeniti Answer ili Questions
SELECT count(*)
  FROM  
  [interestminingl6typeall].[dbo].[Evidence] e, Concept_Extraction.dbo.Concepts as c, [interestminingl6typeall].[dbo].[EvidencePost] ep 
   --iz obe baze dohvati ids postova koji su identicni 
, (SELECT id, idQuestion
FROM [interestminingl6typeall].[dbo].Posts inner join Concept_Extraction.dbo.Questions as q
on CONCAT(title collate SQL_Latin1_General_CP1_CI_AS  , ' ', body collate SQL_Latin1_General_CP1_CI_AS)  = text collate SQL_Latin1_General_CP1_CI_AS
WHERE source = 'L6-typeALL-Answers' and postTypeId = 2) as MappedIds --55 768
  where e.[typeOfEvidence]='Answer' and  e.[informationSourceId]= 'ConceptExtractor' and c.engine = 'ConceptExtractor-L6-typeAll-Answers' 
  and MappedIds.id = ep.idPost and ep.idEvidence=e.id and MappedIds.idQuestion = c.idQuestion and c.text collate Latin1_General_CI_AI = e.keyword collate Latin1_General_CI_AI 
  -- Questions = 23247, Answers = 252 370

SELECT count(*)
  FROM [Concept_Extraction].[dbo].Concepts
  WHERE engine = 'ConceptExtractor-L6-typeAll-Answers' 
  group by idQuestion
  --33 537

SELECT count(*)
  FROM  
  [interestminingl6typeall].[dbo].[Evidence] e, [interestminingl6typeall].[dbo].[EvidencePost] ep 
  where e.[typeOfEvidence]='Answer' and  e.[informationSourceId] = 'ConceptExtractor' and ep.idEvidence=e.id
-- 222 102

SELECT count(*)
FROM [interestminingl6typeall].[dbo].Posts inner join Concept_Extraction.dbo.Questions as q
on CONCAT(title collate SQL_Latin1_General_CP1_CI_AS  , ' ', body collate SQL_Latin1_General_CP1_CI_AS)  = text collate SQL_Latin1_General_CP1_CI_AS
WHERE source = 'L6-typeALL-Answers' and postTypeId = 2



SELECT count(*)
  FROM [Concept_Extraction].[dbo].[Questions]
  WHERE source = 'L6-typeALL-Answers'
  -- L6-typeALL-Questions = 3275
  -- L6-typeALL-Answers = 38 034 

 SELECT count(*)
FROM [interestminingl6typeall].[dbo].Posts inner join Concept_Extraction.dbo.Questions as q
on (select CONCAT(title collate SQL_Latin1_General_CP1_CI_AS , ' ', body collate SQL_Latin1_General_CP1_CI_AS))  = text collate SQL_Latin1_General_CP1_CI_AS
WHERE source = 'L6-typeALL-Answers' and postTypeId = 2
-- Q: 3263
-- A: 55 768 -- L6-typeALL-Answers = 38 034 

delete [interestminingl6typeall].[dbo].Posts 
where id in (
 SELECT p1.id as id --, p2.id, p1.title, p2.title, p1.acceptedAnswerId, p2.acceptedAnswerId, p1.*, p2.*
FROM [interestminingl6typeall].[dbo].Posts as p1 inner join [interestminingl6typeall].[dbo].Posts as p2
on CONCAT(p1.title collate SQL_Latin1_General_CP1_CI_AS , ' ', p1.body collate SQL_Latin1_General_CP1_CI_AS)  = CONCAT(p2.title collate SQL_Latin1_General_CP1_CI_AS , ' ', p2.body collate SQL_Latin1_General_CP1_CI_AS)
WHERE p1.postTypeId = 1 and p2.postTypeId = 1 and p1.id <> p2.id) 
-- 12

use Concept_Extraction
delete Questions
where idQuestion in (
 SELECT q1.idQuestion as id --, p2.id, p1.title, p2.title, p1.acceptedAnswerId, p2.acceptedAnswerId, p1.*, p2.*
FROM Questions as q1 inner join Questions as q2
on q1.text = q2.text and q1.idQuestion <> q2.idQuestion
WHERE q1.source = 'L6-typeALL-Questions' and q2.source = 'L6-typeALL-Questions') 
--12


  --del same posts (3278 row(s) affected)
  use interestminingL6typeALL
DELETE FROM Posts WHERE id IN
(SELECT id FROM  
(SELECT p2.id AS id
FROM 
	Posts p1,
	Posts p2
WHERE
	p1.postTypeId = 2 AND
	p2.postTypeId = 2 AND
	p1.parentId = p2.parentId AND
	LEN(p1.ownerUserId) > 0  AND
	p2.ownerUserId = '' AND
	p1.body = p2.body) t);

use [interestminingL6typeALL]
SELECT count(*)  --[informationSourceId] --TOP 1000 e.id,keyword
  FROM [interestminingL6typeALL].[dbo].[Evidence] e, [EvidencePost] ep, Posts p
   where e.[typeOfEvidence]='Question' and  e.[informationSourceId]= 'ConceptExtractor'
   and ep.idEvidence = e.id and ep.idPost=p.id and p.postTypeId = '1'
     --group by informationSourceId

  SELECT [informationSourceId]
  FROM [interestminingL6typeALL].[dbo].[Evidence] e
  where e.[typeOfEvidence]='Answer'
  group by informationSourceId


  USE master;
GO
CREATE DATABASE interestminingl6typeall 
    ON (FILENAME = 'D:\DataBases\interestminingl6typeall.mdf'),
    (FILENAME = 'D:\DataBases\interestminingl6typeall_log.ldf')
    FOR ATTACH;
GO

use interestminingl6typeall

SET IDENTITY_INSERT [Evidence] OFF

INSERT INTO Evidence (userId, keyword, informationSourceId, trust, weight, typeOfEvidence) VALUES ('u543633', 'aa', 'ConceptExtractor', '1.0,1.0,1.0', 0.2, 'Answer')


  insert into EvidencePost (idEvidence, idPost) values (@@IDENTITY, '2103653a3')
  
  select @@IDENTITY
  select * from EvidencePost 
    where  idEvidence = '3277010'
  
  delete EvidencePost 
  where  idEvidence = '3277009'

  delete Evidence 
  where  id = '3277009'



  -- nadji iste tekstove za obradu !!! 
  use [Concept_Extraction]
  SELECT count(distinct q1.idQuestion)
  FROM Questions q1, Questions q2
  WHERE q2.source = 'L6-typeAll-Answers' and q1.text = q2.text and q1.idQuestion <> q2.idQuestion
  

  use [interestminingL6typeALL]
  select count(distinct userId)
  FROM [interestminingL6typeALL].[dbo].[Evidence]
  where informationSourceId = 'ConceptExtractor'
