/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.ucl.sspace.rs.etf.database;

import edu.ucl.sspace.rs.etf.corpus.ESPreprocessor;
import edu.ucla.sspace.common.Similarity;
import edu.ucla.sspace.common.Similarity.SimType;
import edu.ucla.sspace.vector.CompactSparseVector;
import edu.ucla.sspace.vector.Vector;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author Davor, Bojan Furlan
 */
public class SemanticSpaceDBReader {
    
    private int dimension=0;
    private String type=null;
    private ConcurrentHashMap<String,Vector> vectorCash = new ConcurrentHashMap<String, Vector>();
    protected Connection con=null;

    public static enum Singleton {
		INSTANCE;

		private static final SemanticSpaceDBReader singleton = new SemanticSpaceDBReader(14000,0);

		public SemanticSpaceDBReader getSingleton() {
			return singleton;
		}
	}
	
    public void reOpen(){
    	 
    	try {
			con=ESConnection.getConnection();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    /**
     *
     * @param dimension vector dimension
     * 14000- COALS, 4000-RI
     * @param type which type of alg. was used for creating semantic space
     * 0-coals, 1-randomindexing, 2-lsa
     */
    public SemanticSpaceDBReader(int dimension,int type){
        this.dimension=dimension;
        if(type==0) this.type="coals";
        else if(type==1) this.type="randomindexing";
        else this.type="lsa";
        try {
			con=ESConnection.getConnection();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    public Vector getVector(String word){
    	if (vectorCash.containsKey(word)) {

    		return vectorCash.get(word);
			
		} else {

		
        Vector rezVector=null;
        try{
            
            PreparedStatement ps=null;
            ResultSet rs=null;

            
            String sqlString="SELECT * FROM "+type+" WHERE keyword=?;";
            ps=con.prepareStatement(sqlString);
            ps.setString(1, word);
            rs=ps.executeQuery();

            if(rs.next()){
                double[] row=new double[dimension];
                String strVector=rs.getString("vector");
                String [] values=strVector.split(",");
                for (int i = 0; i < values.length; i +=2 ) {
                    int col = Integer.parseInt(values[i]);
                    double val = Double.parseDouble(values[i+1]);
                    row[col] = val;
                }
               // forming vector
               rezVector=new CompactSparseVector(row);

               vectorCash.put(word, rezVector);
               
            }

            if(rs!=null) rs.close();
            if(ps!=null) ps.close();

        }catch(SQLException sqlEx){
            sqlEx.printStackTrace();
        }


        return rezVector;
		}
    }
    
    public double getTermFrequency (String word)
    {
    	double val = 0;
    	try{
            Connection con=null;
            PreparedStatement ps=null;
            ResultSet rs=null;

            con=ESConnection.getConnection();
            String sqlString="SELECT tfnorm0_1 FROM lexicon WHERE term=?;";
            ps=con.prepareStatement(sqlString);
            ps.setString(1, word);
            rs=ps.executeQuery();

            if(rs.next()){
                String temp=rs.getString("tfnorm0_1");
                val = Double.parseDouble(temp);
            }

            if(rs!=null) rs.close();
            if(ps!=null) ps.close();

        }catch(SQLException sqlEx){
            sqlEx.printStackTrace();
        }
    	
    	return val;

    }
    
    public double getTermTFIDF (String word)
    {
    	double val = 0;
    	try{
            Connection con=ESConnection.getConnection();
            String sqlString = "SELECT freq FROM FreqCleaned WHERE informationSourceId='TFIDF' and word=?;";
			PreparedStatement preparedStatementSel =con.prepareStatement(sqlString);
			preparedStatementSel.setString(1, word);
			ResultSet rs=preparedStatementSel.executeQuery();
			if (rs.next()) {
				val = rs.getDouble(1);
            }

            if(rs!=null) rs.close();
            if(preparedStatementSel!=null) preparedStatementSel.close();

        }catch(SQLException sqlEx){
            sqlEx.printStackTrace();
        }
    	
    	return val;

    }

    public static void main(String[] args) throws IOException{
        SemanticSpaceDBReader spdb=SemanticSpaceDBReader.Singleton.INSTANCE.getSingleton();
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        boolean isEnd=false;
        while(!isEnd){
            System.out.println();
            System.out.print("Prva rec: ");
            String sent1[]=new String[1];
            String sent2[]=new String[1];
            sent1[0]="automot" ; //in.readLine();
            //sent1=ESPreprocessor.filter(sent1, false,false);
            System.out.println();
            System.out.print("Druga rec: ");
            sent2[0]="autorout";//in.readLine();
            //sent2=ESPreprocessor.filter(sent2,false,false);

            Vector v1=spdb.getVector(sent1[0]);
            Vector v2=spdb.getVector(sent2[0]);
            if(v1!=null && v2!=null){
                System.out.println();
                System.out.println("Slicnost je: "+Similarity.getSimilarity(SimType.COSINE, v1, v2));
            }
    }
    }


}
