/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ucl.sspace.rs.etf.semantics;

import edu.ucl.sspace.rs.etf.corpus.ESPreprocessor;
import edu.ucl.sspace.rs.etf.corpus.SrCorpusProcessor;
import edu.ucl.sspace.rs.etf.database.SemanticSpaceDBReader;
import edu.ucla.sspace.common.Similarity;
import edu.ucla.sspace.common.Similarity.SimType;
import edu.ucla.sspace.text.DocumentPreprocessor;
import edu.ucla.sspace.vector.Vector;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Bojan Furlan
 */
public class SentenceSimilarityEng implements Runnable {

	public double getSimilarity(String sent1,String sent2) throws IOException // static double
	{


		//sent1 = SrCorpusProcessor.processSrString(sent1);
		DocumentPreprocessor dp=new DocumentPreprocessor();
		sent1 = dp.process(sent1);
		sent1 = ESPreprocessor.Singleton.INSTANCE.getSingleton().filter(sent1, false, removeStopWords);
		//sent1 = ESPreprocessor.stemSerbian(sent1);

		//sent2 = SrCorpusProcessor.processSrString(sent2);
		dp=new DocumentPreprocessor();
		sent2 = dp.process(sent2);
		sent2 = ESPreprocessor.Singleton.INSTANCE.getSingleton().filter(sent2, false, removeStopWords);
		//sent2 = ESPreprocessor.Singleton.INSTANCE.getSingleton().stemSerbian(sent2);

		return result = getSimilarity(sent1.split(" "), sent2.split(" "));
		//result = SentenceSimilarity.getSimilarity(sent1.split(" "), sent2.split(" "));
	}

	private String initialGrade;
	private String sent1, sent2;
	private String originalSent1, originalSent2;
	private int lineCnt;
	private double result = 0;
	private String [] stringVector;
	private int [] currentThreads;
	HashMap<String, Double> termFrequencies;
	boolean isOriginal = false;
	boolean isMax = true;
	boolean removeStopWords = true;
	SemanticSpaceDBReader ssDBr=SemanticSpaceDBReader.Singleton.INSTANCE.getSingleton();//new SemanticSpaceDBReader(14000,0);

	public SentenceSimilarityEng(String initialGrade, String sent1, String sent2, int lineCnt, String [] stringVector, int [] currentThreads, HashMap<String, Double> tfHashMap, boolean isOriginal, boolean isMax, boolean removeStopWords)
	{
		this(tfHashMap,isOriginal,isMax);
		this.initialGrade = initialGrade;
		this.sent1 = sent1;
		this.sent2 = sent2;
		originalSent1 = sent1;
		originalSent2 = sent2;
		this.lineCnt = lineCnt;
		this.stringVector = stringVector;
		this.currentThreads = currentThreads;
		this.isOriginal = isOriginal;
		this.removeStopWords = removeStopWords;
	}

	public SentenceSimilarityEng(HashMap<String, Double> tfHashMap, boolean isOriginal, boolean isMax)
	{
		this.isOriginal = isOriginal;
		this.isMax = isMax;
		termFrequencies = tfHashMap;
	}

	public double getSimilarity(String[] sent1,String[] sent2) throws IOException{

		double dSim=0.0;


		// Step1: Here we assume that all stoped words in sent 1 and sent2
		//        are cleaned, so this step is skiped

		// revizija u phyton-u pre nego sto se proslede sen1, sent2 treba ih
		// preprocesirati preko ESPreprocessor-a

		// Step2: we count the number of delta(num of exact similarity strings)
		//        in sentence. We remove all delta tokens from both P and R
		//        if all the terms match we go to step 6
		//        it is donig through StringSimilirity.getCommonWordOrderSimilarity
		//        and at this time we count S0,and P,R are formed too.

		StringSimilarity sSim=new StringSimilarity(sent1, sent2);

		// Ova f.ja bi trebala da se pozove iz phyton-a i da se prosledi
		// getSimilarity s0
		//double s0=sSim.getCommonWordOrderSimilarity(sent1, sent2);

		//System.out.println("Common worder step2 passed");

		// step3: we construct m-sigma x n-sigma StringSimilarity matrix
		//        m is number of strings in the shorter sentence
		//        n is number of string  in the longer  sentence
		double[][] stringSimilarityMatrix=sSim.getStringSimilarityMatrix();

		//System.out.println("String Similarity Matrux has been created step3 passed");



		// step4:we construct m-sigma x n-sigma semantic similarity matrix
		//       wich alg. will be used?
		double [][] semanticSimilarityMatrix=new double[sSim.m-sSim.sigmaM][sSim.n-sSim.sigmaN];

		// DODATO
		double [][] termFrequencyMatrix = new double[sSim.m-sSim.sigmaM][sSim.n-sSim.sigmaN];




		//System.out.println("In process is semanticSimilarity matrix construction");
		ArrayList<Vector> vectorList=new ArrayList<Vector>();

		for(int i=0;i<sSim.m-sSim.sigmaM;i++){
			Vector v2=ssDBr.getVector(sSim.R.get(i));
			double tfnorm2 = 1, tfnorm1 = 1;
			if (!isOriginal && termFrequencies.containsKey(sSim.R.get(i))) 
				tfnorm2 = termFrequencies.get(sSim.R.get(i));
			// za normTF if (v2 != null) tfnorm2 = ssDBr.getTermFrequency(sSim.R.get(i));
			//if (v2 != null) tfnorm2 = ssDBr.getTermTFIDF(sSim.R.get(i));
			for(int j=0;j<sSim.n-sSim.sigmaN;j++){
				Vector v1=null;
				if(i==0){
					// only first time
					v1=ssDBr.getVector(sSim.P.get(j));
					vectorList.add(v1);
				}else v1=vectorList.get(j);

				if(v1!=null && v2!=null) 
					semanticSimilarityMatrix[i][j]=Similarity.getSimilarity(SimType.COSINE, v1, v2);
				else semanticSimilarityMatrix[i][j]=0.0;

				if (!isOriginal && termFrequencies.containsKey(sSim.P.get(j))) 
					tfnorm1 = termFrequencies.get(sSim.P.get(j));
				else tfnorm1 = 1;
				//za normTF if (v1 != null) tfnorm1 = ssDBr.getTermFrequency(sSim.P.get(j));      				        				
				//if (v1 != null) tfnorm1 = ssDBr.getTermTFIDF(sSim.P.get(j));
				termFrequencyMatrix[i][j] = tfnorm1*tfnorm2;
			}
		}


		// step5: we construct another joint matrix wich dimension are: m-sigma X n-sigma
		//        as: M=psi*M1+fi*M2
		//        where psi is string similarity weight, and fi is semantic similarity weight

		double psi=0.45;
		double fiCorpusBased=0.55;
		double [][] jointMatrix=null;
		double [][] psiM1=MatrixES.scalarMultiplication(stringSimilarityMatrix,psi,sSim.m-sSim.sigmaM, sSim.n-sSim.sigmaN);
		double [][] fiCorpusBasedM2=MatrixES.scalarMultiplication(semanticSimilarityMatrix,fiCorpusBased,sSim.m-sSim.sigmaM, sSim.n-sSim.sigmaN);

		jointMatrix=MatrixES.adition(psiM1,fiCorpusBasedM2,sSim.m-sSim.sigmaM, sSim.n-sSim.sigmaN);


		// DODATO ////////////////////////////////////////////////////////////

		for(int i=0;i<sSim.m-sSim.sigmaM;i++){
			for(int j=0;j<sSim.n-sSim.sigmaN;j++){
				termFrequencyMatrix[i][j] = Math.pow(2, termFrequencyMatrix[i][j]-1);
				jointMatrix[i][j] *= termFrequencyMatrix[i][j];
			}
		}
		///////////////////////////////////////////////////////////////////////



		//System.out.println("Joint Matrix has been created, wait for finish");
		double roSum = 0;

		if (isMax) {
			roSum= (MatrixES.findMaxElemColumnsSum(jointMatrix,sSim.m-sSim.sigmaM, sSim.n-sSim.sigmaN) 
					+ MatrixES.findMaxElemRowsSum(jointMatrix,sSim.m-sSim.sigmaM, sSim.n-sSim.sigmaN))/2;
		}else {

			// step6:construct ro list, wich is consisted of maximal elemnts from joint matrix
			ArrayList<Double> ro=new ArrayList<Double>();
			boolean isOver=false;
			int i=0;
			while(!isOver){
				jointMatrix=MatrixES.findMaxElemAndRemove(jointMatrix,sSim.m-sSim.sigmaM-i, sSim.n-sSim.sigmaN-i, ro);
				if(jointMatrix==null) break;
				i++;
			}

			/* STARI NACIN RACUNANJA
	        // step7: final exec. eq. to eq. from paper

	        // define wf- weight for common word similarity between 0 and 0.5
	        double wf=0.0;
	        // make sum of all ro elemnts
	        double roSum=0;
	        for(i=0;i<ro.size();i++) roSum+=ro.get(i);
	        // make factor wich is important for common word similarity
	        // sigma*(1-wf+wf*S0);
	        double cowSimF=sSim.sigmaM*(1-wf+wf*s0);
	        // make numerator wich is: (roSum+cowSimF)x(m+n)
	        double numerator=(roSum+cowSimF)*(sSim.m+sSim.n);
	        // make denominator
	        double denominator=2*sSim.m*sSim.n;
	        // finaly
	        dSim=numerator/denominator;
			 */


			// NOV NACIN RACUNANJA
			// step7: final exec. eq. to eq. from paper

			// make sum of all ro elemnts
			for(i=0;i<ro.size();i++) roSum+=ro.get(i);

		}

		double delta=sSim.sigmaM;
		double deltaValue = 0; 
		for (int i=0; i<delta; i++)
		{
			String word = sSim.sameWords[i];
			double tfnorm = 1;
			//za normTF if (ssDBr.getVector(word) != null) tfnorm = ssDBr.getTermFrequency(word);
			//if (ssDBr.getVector(word) != null) tfnorm = ssDBr.getTermTFIDF(word);
			if (!isOriginal && termFrequencies.containsKey(word)) tfnorm = termFrequencies.get(word);
			tfnorm = tfnorm * tfnorm;
			tfnorm = Math.pow(2, tfnorm-1);
			deltaValue += tfnorm;
		}
		// make numerator wich is: (roSum+deltaValue)x(m+n)
		double numerator=(roSum+deltaValue)*(sSim.m+sSim.n);
		// make denominator
		double denominator=2*sSim.m*sSim.n;
		// finally
		dSim=numerator/denominator;

		return dSim;
	}



	@Override
	public void run() {
		try {
			double res = getSimilarity(sent1, sent2);
			String newLine = initialGrade + "\t" + res + "\t" + originalSent1 + "\t" + originalSent2;
			stringVector[lineCnt-1] = newLine;
			System.out.println ("Line: " + lineCnt);
			System.out.println (newLine);
			synchronized (currentThreads)
			{
				currentThreads[0]--;
			}
			synchronized (stringVector)
			{
				stringVector.notifyAll();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
