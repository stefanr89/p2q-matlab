package edu.ucl.sspace.rs.etf.IQRSEvaluation;

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 7/6/13
 * Time: 12:40 PM
 * To change this template use File | Settings | File Templates.
 */
public enum EvidenceType {
    QUESTION,
    ANSWER,
    THREAD
}
