package edu.ucl.sspace.rs.etf.semantics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class StringSimilarityQuestionToProfile extends StringSimilarity {

	
	 public StringSimilarityQuestionToProfile(HashMap<String, Double> question, HashMap<String, Double> user){
	    	
	    	this.refP = question;
	    	this.refR = user;
	    	P = new ArrayList<>(question.keySet());
	    	R = new ArrayList<>(user.keySet());
	    	
	    	
	    	m = P.size();
	    	n = R.size();
	    	
	    	
	 }	        

	 @Override
	 public double[][] getStringSimilarityMatrix(){

	        // m and n are counted before with calling getCommonWordOrderSimilarity
	        // wich was done in step 2 in SentenceSimilarity class
	        // this.P and this.R are formed too
	        double [][] simMatrix=new double[m][n];

	        for(int i=0;i<m;i++)
	            for(int j=0;j<n;j++){
	                // for all P
	                double alfa=getStringSimilarity(R.get(j),P.get(i));
	                simMatrix[i][j]=alfa;
	            }

	        return simMatrix;
	    }
	 
	    public static void main(String[] args){

			HashMap<String, Double> sent1 = new HashMap<String, Double>(){
				{
					put("computer",0.1);  //"computer science is looking for hight tech improvment";
					put("is",0.1); 
					put("looking",0.5);
					put("for",0.1); 
					put("hight",0.5);
					put("tech",0.1); 
					put("improvment",0.5);

				}
			};
			HashMap<String, Double> sent2 = new HashMap<String, Double>(){
				{
					put("history",0.1); //history science try to prove what happen a long time ago"
					put("try",0.1); 
					put("to",0.5);
					put("prove",0.1); 
					put("what",0.5);
					put("happen",0.1); 
					put("a",0.5);
					put("long",0.1); 
					put("time",0.5);
					put("ago",0.1); 

				}
			};
				
			StringSimilarity ss=new StringSimilarity(sent1, sent2);
			StringSimilarityQuestionToProfile ssQ2P = new StringSimilarityQuestionToProfile(sent1, sent2);
			
			double[][] ssMatrix = ss.getStringSimilarityMatrix();
			double[][] ssQ2PMatrix = ssQ2P.getStringSimilarityMatrix();

			System.out.println(Arrays.deepEquals(ssMatrix, ssQ2PMatrix));
			
			
	    }
}
