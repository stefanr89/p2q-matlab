/*
 *  to make easier file reading
 * 
 */

package edu.ucl.sspace.rs.etf.corpus;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author Davor Jovanovic, ES
 */
public class FileReaderES {
    
    private FileInputStream fis = null;
    private BufferedInputStream bis = null;
    //private DataInputStream dis = null;

    private long pointer;

    public FileReaderES(File file) throws FileNotFoundException{
        fis = new FileInputStream(file);
        // Here BufferedInputStream is added for fast reading.
        bis = new BufferedInputStream(fis);
        //dis = new DataInputStream(bis);
    }

    public String readLine(){


        String retString=null;
        try {
            //BufferedInputStream bis = new BufferedInputStream(fis);
            DataInputStream dis = new DataInputStream(bis);


            if(dis.available()!=0){
                try{
                    retString=dis.readLine();
                }catch(Exception ex){
                    ex.printStackTrace();
                    System.out.println("NAPOMENA: greska, string je predugacak=> preduzeta akcija zaobilazenja");
                }
                if(retString==null) System.out.println("GRESKA: String je null");
            }
            else close();
            //bis.close();
            //dis.close();

        }catch(IOException ex){}

        return retString;

    }

    public void close() throws IOException{
        if(fis!=null){
                fis.close();
                //bis.close();
                //dis.close();
        }

    }

    public static void main(String [] args) throws FileNotFoundException{

    }

}
