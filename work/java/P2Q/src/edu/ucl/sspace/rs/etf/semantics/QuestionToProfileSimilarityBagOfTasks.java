package edu.ucl.sspace.rs.etf.semantics;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import edu.ucl.sspace.rs.etf.IQRSEvaluation.Evaluation;
import edu.ucl.sspace.rs.etf.IQRSEvaluation.EvidenceJoiner;
import edu.ucl.sspace.rs.etf.IQRSEvaluation.EvidenceToHashMapMapper;
import edu.ucl.sspace.rs.etf.IQRSEvaluation.EvidenceType;

public class QuestionToProfileSimilarityBagOfTasks extends
QuestionToProfileSimilarity {


	ArrayList<String> usersList;
	HashMap<String, Double> question;
	ConcurrentHashMap<String, Double> results;
	List<EvidenceToHashMapMapper> mapperList;
	String questionId;
	EvidenceType evidenceType;
	//static int line =0;
	
	final static Logger logger = Logger.getLogger("myLogger");
	 


	public QuestionToProfileSimilarityBagOfTasks(String questionId, HashMap<String, Double> questionEvidences, ArrayList<String> usersList, 
			ConcurrentHashMap<String, Double> results, List<EvidenceToHashMapMapper> mapperList, EvidenceType evidenceType, boolean isOriginal, boolean isMax) {
		super(isOriginal, isMax);
		this.question = questionEvidences;
		this.results = results;
		this.usersList = usersList;
		this.mapperList= mapperList;
		this.questionId = questionId;
		this.evidenceType = evidenceType;

	}


	@Override
	public void run() {

		try {

			while (true) {

				String userId;
				synchronized (usersList) {

					if (usersList.isEmpty()) {
						break;
					}
					else {
						userId = usersList.remove(0);
					}
				}

				List<HashMap<String, Double>> hashMaps = new ArrayList<HashMap<String,Double>>();
				
				for (EvidenceToHashMapMapper mapper : mapperList) {
					hashMaps.add(mapper.generateUserHashMapForPost(userId, evidenceType , questionId));
				}

				HashMap<String, Double> userEvidences =  EvidenceJoiner.joinEvidences(hashMaps);

				if (userEvidences.size()==0) {
					logger.log(Level.WARNING, "User without evidences: " + userId);
					//System.err.println("User without evidences: " + userId);
				}

				double res = getWeightedSimilarity(userEvidences, question);
				results.put(userId, res);

				//				int ln;
				//				synchronized (QuestionToProfileSimilarityBagOfTasks.class) {
				//					ln = line++;
				//				}

				String newLine ="User: "+ userId + "\t" + res + "\t" + user + "\t" + question;
				logger.log(Level.ALL, newLine);
				//System.out.println (newLine);
			}


		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	public static void main(String[] args) {
		
		int maxThreads = 2;

		while (true) {
			ExecutorService executor = Executors.newFixedThreadPool(maxThreads);

			for (int i = 0; i < maxThreads; i++) {

				executor.execute(new Runnable() {
					public void run() {
						System.out.println("Test");
					}
				});

			}
			executor.shutdown();
			try {
				executor.awaitTermination(Long.MAX_VALUE,
						TimeUnit.NANOSECONDS);
			} catch (InterruptedException e) {

			}
		}
		
	}



}
