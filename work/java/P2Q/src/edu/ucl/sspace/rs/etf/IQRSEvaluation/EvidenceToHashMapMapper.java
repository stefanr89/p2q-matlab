package edu.ucl.sspace.rs.etf.IQRSEvaluation;
import java.sql.*;
import java.util.*;

import edu.ucl.sspace.rs.etf.database.ESConnection;

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 7/4/13
 * Time: 4:43 PM
 * To change this template use File | Settings | File Templates.
 */
public class EvidenceToHashMapMapper {
    //MSSQL Example
    //private final String connectionUrl =  ESConnection.getConnection("interestminingl6typeall"); // "jdbc:sqlserver://localhost:1433;databaseName=interestminingl6typeall;integratedSecurity=true";
    // "jdbc:mysql://localhost:3306/interestminingl6typeall?user=interestmining&password=6hGKsfpdC4aCXC75";
    private String informationSourceId = null;
    private static Connection connection = ESConnection.getConnection("interestminingl6typeall");

    public EvidenceToHashMapMapper(String informationSourceId) throws SQLException {
        this.informationSourceId = informationSourceId;
    }

	@Override
	public String toString() {
		return informationSourceId+"";
	}
    /**
     * Returns all evidences from questions.
     *
     * @return
     * @throws Exception
     */
    public HashMap<String, HashMap<String, Double>> generateQuestionHashMaps() throws Exception {
        HashMap<String, HashMap<String, Double>> retVal = new HashMap<String, HashMap<String, Double>>();
        for (String postId : getAllQuestionIds()) {
            retVal.put(postId, getEvidences(postId));
        }
        return retVal;
    }

    /**
     * Returns all evidences from answers.
     *
     * @return
     * @throws Exception
     */
    public HashMap<String, HashMap<String, Double>> generateAnswersHashMaps() throws Exception {
        HashMap<String, HashMap<String, Double>> retVal = new HashMap<String, HashMap<String, Double>>();
        for (String postId : getAllAnswerIds()) {
             retVal.put(postId, getEvidences(postId));
         }

        return retVal;
    }

    /**
     * Return all evidences for users from questions. For specific user, all evidences for his questions are aggregated.
     *
     * Takes Evidences ONLY from questions.
     *
     * @return
     * @throws Exception
     */
    public HashMap<String, HashMap<String, Double>> generateQuestionUserHashMaps() throws Exception {
        HashMap<String, HashMap<String, Double>> retVal = new HashMap<String, HashMap<String, Double>>();

        for (String userId : getAllUserIds()) {
            HashMap<String, Double> userMap = new HashMap<String, Double>();
            ResultSet rs = getAllQuestionEvidencesByUserId(userId);
            while (rs.next()) {
                String keyword = rs.getString("keyword");
                Double weight = rs.getDouble("weight");
                if (userMap.containsKey(keyword)) {
                    userMap.put(keyword, probTCoNorm(weight, userMap.get(keyword)));
                } else {
                    userMap.put(keyword, weight);
                }
            }
            retVal.put(userId, userMap);
            rs.close();
        }

        return retVal;
    }

    /**
     * Return all evidences for users from answers. For specific user, all evidences for his answers are aggregated.
     * (In L6 we have only data for best answerers)
     *
     * Takes Evidences ONLY from answers.
     *
     * @return
     * @throws Exception
     */
    public HashMap<String, HashMap<String, Double>> generateAnswerUserHashMaps() throws Exception {
        HashMap<String, HashMap<String, Double>> retVal = new HashMap<String, HashMap<String, Double>>();

        for (String userId : getAllUserIds()) {
            HashMap<String, Double> userMap = new HashMap<String, Double>();
            ResultSet rs = getAllAnswerEvidencesByUserId(userId);
            while (rs.next()) {
                String keyword = rs.getString("keyword");
                Double weight = rs.getDouble("weight");
                if (userMap.containsKey(keyword)) {
                    userMap.put(keyword, probTCoNorm(weight, userMap.get(keyword)));
                } else {
                    userMap.put(keyword, weight);
                }
            }
            retVal.put(userId, userMap);
            rs.close();
        }

        return retVal;
    }


    /**
     * Return all evidences for specific user without those, related to questionId.
     *
     * Takes Evidences according to enum.
     *
     * @return
     * @throws Exception
     */
    public HashMap<String, Double> generateUserHashMapForPost(String userId, EvidenceType evidenceType, String questionIdToRemove) throws Exception {
        HashMap<String, Double> userMap = new HashMap<String, Double>();

        if(evidenceType == EvidenceType.QUESTION) {
            //Evidences from all questions, that this user asked \ question with questionId
            for (String questionId : getAllQuestionIdsForUser(userId)) {
                if (!questionId.equals(questionIdToRemove)) {
                    HashMap<String, Double> postMap = generatePostHashMap(questionId);
                    for (String keyword : postMap.keySet()) {
                        if (userMap.containsKey(keyword)) {
                            userMap.put(keyword, probTCoNorm(postMap.get(keyword), userMap.get(keyword)));
                        } else {
                            userMap.put(keyword, postMap.get(keyword));
                        }
                    }
                }
            }
        }
        else if(evidenceType == EvidenceType.ANSWER) {
            //Evidences from all answers (best), that this user answered without those related to  questionId
            for (String answerId : getAllAnswerIdsForUser(userId)) {
                if (!getParentId(answerId).equals(questionIdToRemove)) {
                    HashMap<String, Double> postMap = generatePostHashMap(answerId);
                    for (String keyword : postMap.keySet()) {
                        if (userMap.containsKey(keyword)) {
                            userMap.put(keyword, probTCoNorm(postMap.get(keyword), userMap.get(keyword)));
                        } else {
                            userMap.put(keyword, postMap.get(keyword));
                        }
                    }
                }
            }
        }
        else if(evidenceType == EvidenceType.THREAD) {
            //Evidences from all threads(best answer and other + question), that this user answered/asked without those related to questionId
            //combine ids and remove the one
            Set<String> userQuestionIds = new HashSet<String>(getAllQuestionIdsForUser(userId));
            for (String answerId : getAllAnswerIdsForUser(userId)) {
                userQuestionIds.add(getParentId(answerId));
            }
            userQuestionIds.remove(questionIdToRemove);

            //get ids for threads
            HashSet<String> postsIds = new HashSet<String>();
            for (String questionId : userQuestionIds) {
                postsIds.add(questionId);
                postsIds.addAll(getAllAnswerIdsForQuestion(questionId));
            }

            //do final
            for (String postId : postsIds) {
                HashMap<String, Double> postMap = generatePostHashMap(postId);
                for (String keyword : postMap.keySet()) {
                    if (userMap.containsKey(keyword)) {
                        userMap.put(keyword, probTCoNorm(postMap.get(keyword), userMap.get(keyword)));
                    } else {
                        userMap.put(keyword, postMap.get(keyword));
                    }
                }
            }
        }


        return userMap;
    }


    /**
     * Returns all evidences from a specific post.
     *
     * @return
     * @throws Exception
     */
    public HashMap<String, Double> generatePostHashMap(String postId) throws Exception {
        return getEvidences(postId);
    }



    	
    
    //DATABASE STUFF
    private List<String> getAllUserIds() throws SQLException {
        ArrayList<String> retVal = new ArrayList<String>();

        PreparedStatement stmt = connection.prepareStatement("SELECT id FROM Users;");
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            retVal.add(rs.getString("id"));
        }
        rs.close();
        stmt.close();

        return retVal;
    }


    public static List<String> getAllQuestionOwnerUserIds() throws SQLException {
        ArrayList<String> retVal = new ArrayList<String>();

        PreparedStatement stmt = connection.prepareStatement("SELECT DISTINCT ownerUserId FROM Posts WHERE postTypeId=1;");
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            retVal.add(rs.getString("ownerUserId"));
        }
        rs.close();
        stmt.close();

        return retVal;
    }

    public static List<String> getAllUserIdsForType(int type, String[] userCategories) throws SQLException {
        HashSet<String> retVal = new HashSet<String>();

        for (String category2 : userCategories) {
            PreparedStatement stmt = connection.prepareStatement("SELECT DISTINCT u.id FROM Users u, Posts p WHERE p.ownerUserId = u.id AND u.type = "+type+" AND p.cat2 LIKE '%" + category2 + "%';");
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                retVal.add(rs.getString("id"));
            }
            rs.close();
            stmt.close();
        }



        return new ArrayList<String>(retVal);
    }
    
    
    public static List<String> getAllUserIdsForALLTypes() throws SQLException {
        ArrayList<String> retVal = new ArrayList<String>();

        PreparedStatement stmt = connection.prepareStatement("SELECT id FROM Users;");
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            retVal.add(rs.getString("id"));
        }
        rs.close();
        stmt.close();

        return retVal;
    }
    
    private static String getParentId(String answerId) throws SQLException {

        PreparedStatement stmt = connection.prepareStatement("SELECT parentId FROM Posts WHERE id = ?;");
        stmt.setString(1, answerId);
        ResultSet rs = stmt.executeQuery();
        rs.next();
        String retVal = rs.getString("parentId");
        rs.close();
        stmt.close();
        return retVal;
    }


    public static String getUserOwnerId(String postId) throws SQLException {
        PreparedStatement stmt = connection.prepareStatement("SELECT ownerUserId FROM Posts WHERE id = ?;");
        stmt.setString(1, postId);
        ResultSet rs = stmt.executeQuery();
        rs.next();
        String retVal = rs.getString("ownerUserId");
        rs.close();
        stmt.close();
        return retVal;
    }

    private List<String> getAllQuestionIdsForUser(String userId) throws SQLException {
        ArrayList<String> retVal = new ArrayList<String>();

        PreparedStatement stmt = connection.prepareStatement("" +
                "SELECT p.id AS id " +
                "FROM " +
                "   Users u, " +
                "   Posts p " +
                "WHERE " +
                "   u.id = ? AND " +
                "   u.id = p.ownerUserId AND" +
                "   p.postTypeId = 1;");
        stmt.setString(1, userId);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            retVal.add(rs.getString("id"));
        }
        rs.close();
        stmt.close();

        return retVal;
    }

    private List<String> getAllAnswerIdsForUser(String userId) throws SQLException {
        ArrayList<String> retVal = new ArrayList<String>();

        PreparedStatement stmt = connection.prepareStatement("" +
                "SELECT p.id AS id " +
                "FROM " +
                "   Users u, " +
                "   Posts p " +
                "WHERE " +
                "   u.id = ? AND " +
                "   u.id = p.ownerUserId AND" +
                "   p.postTypeId = 2;");
        stmt.setString(1, userId);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            retVal.add(rs.getString("id"));
        }
        rs.close();
        stmt.close();

        return retVal;
    }

    private List<String> getAllAnswerIdsForQuestion(String questionId) throws SQLException {
        ArrayList<String> retVal = new ArrayList<String>();

        PreparedStatement stmt = connection.prepareStatement("SELECT id FROM Posts WHERE postTypeId = 2 AND parentId = ?;");
        stmt.setString(1, questionId);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            retVal.add(rs.getString("id"));
        }
        rs.close();
        stmt.close();

        return retVal;
    }

    private List<String> getAllAnswerIds() throws SQLException {
        ArrayList<String> retVal = new ArrayList<String>();

        PreparedStatement stmt = connection.prepareStatement("SELECT id FROM Posts WHERE postTypeId = 2;");
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            retVal.add(rs.getString("id"));
        }
        rs.close();
        stmt.close();

        return retVal;
    }

    public static String getBestAnswererIdForQuestion(String questionId) throws SQLException {

        PreparedStatement stmt = connection.prepareStatement("select bestAnswer.ownerUserId from Posts as question inner join Posts as bestAnswer on question.acceptedAnswerId = bestAnswer.id "
        		+ "WHERE question.id = ?;");

        stmt.setString(1, questionId);
        ResultSet rs = stmt.executeQuery();
        if (rs.next()) {
            String retVal =  rs.getString("ownerUserId");
            rs.close();
            stmt.close();
            return retVal;
        }
        else {
            System.err.println("NO bestAnswerOwnerUserId");
        }
        rs.close();
        stmt.close();
        	
        return null;
    }


    public static ArrayList<String> getAllQuestionIdsForUserForEvaluation(String userId) throws SQLException {
        ArrayList<String> retVal = new ArrayList<String>();

        PreparedStatement stmt = connection.prepareStatement("select question.id from Posts as question inner join Posts as bestAnswer on question.acceptedAnswerId = bestAnswer.id WHERE question.acceptedAnswerId <> '' and bestAnswer.ownerUserId = ? ;");
        stmt.setString(1, userId);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            retVal.add(rs.getString("id"));
        }
        rs.close();
        stmt.close();

        return retVal;
    }

    private ArrayList<String> getAllQuestionIds() throws SQLException {
        ArrayList<String> retVal = new ArrayList<String>();

        PreparedStatement stmt = connection.prepareStatement("SELECT id FROM Posts WHERE postTypeId = 1;");
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            retVal.add(rs.getString("id"));
        }
        rs.close();
        stmt.close();

        return retVal;
    }

    private ResultSet getAllQuestionEvidencesByUserId(String userId) throws SQLException {
        return getEvidences(userId, 1);
    }

    private ResultSet getAllAnswerEvidencesByUserId(String userId) throws SQLException {
        return getEvidences(userId, 2);
    }

    private ResultSet getEvidences(String userId, int postType) throws SQLException {
        PreparedStatement stmt = connection.prepareStatement(
                "SELECT e.keyword AS keyword, e.weight AS weight " +
                        "FROM " +
                        "   Evidence e, " +
                        "   EvidencePost ep, " +
                        "   Posts p, " +
                        "   Users u " +
                        "WHERE " +
                        "   u.id = ? AND " +
                        "   p.postTypeId = ? AND " +
                        "   e.informationSourceId = ? AND " +
                        "   u.id = p.ownerUserId AND " +
                        "   p.id = ep.idPost AND " +
                        "   ep.idEvidence = e.id " +
                        "ORDER BY e.keyword ASC; ");
        stmt.setString(1, userId);
        stmt.setInt(2, postType);
        stmt.setString(3, informationSourceId);
        stmt.closeOnCompletion();
        return stmt.executeQuery();
    }

    private HashMap<String, Double> getEvidences(String postId) throws SQLException {
        PreparedStatement stmt = connection.prepareStatement(
                "SELECT e.keyword AS keyword, e.weight AS weight " +
                        "FROM " +
                        "   Evidence e, " +
                        "   EvidencePost ep, " +
                        "   Posts p " +
                        "WHERE " +
                        "   p.id = ? AND " +
                        "   e.informationSourceId = ? AND " +
                        "   p.id = ep.idPost AND " +
                        "   ep.idEvidence = e.id " +
                        "ORDER BY e.keyword ASC; ");
        stmt.setString(1, postId);
        stmt.setString(2, informationSourceId);
        //stmt.closeOnCompletion(); //throws err for SQL SRV
        ResultSet rs = stmt.executeQuery();
        
        HashMap<String, Double> postMap = new HashMap<String, Double>();
        while (rs.next()) {
            String keyword = rs.getString("keyword");
            Double weight = rs.getDouble("weight");
            postMap.put(keyword, weight);
        }
        
        return postMap;

    }

    //UTILS
    public static double probTCoNorm(double a, double b){
        return a+b - a*b;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        //connection.close();
    }

    //MAIN METHOD
    public static void main(String[] args) throws Exception {
        EvidenceToHashMapMapper mapper = new EvidenceToHashMapMapper("ConceptExtractor");

        /*
        //post evidences
        HashMap<String, HashMap<String, Double>> map1 = mapper.generateQuestionHashMaps();
        HashMap<String, HashMap<String, Double>> map2 = mapper.generateAnswersHashMaps();
        //user evidences
        HashMap<String, HashMap<String, Double>> map3 = mapper.generateQuestionUserHashMaps();
        HashMap<String, HashMap<String, Double>> map4 = mapper.generateAnswerUserHashMaps();
        */

        /*
        HashMap<String, Double> map5 = mapper.generatePostHashMap("2421111");
        HashMap<String, Double> map6 = mapper.generateUserHashMapForPost("u1503147", EvidenceType.ANSWER, "2421111");
        HashMap<String, Double> map7 = mapper.generateUserHashMapForPost("u1503147", EvidenceType.QUESTION, "2421111");
        HashMap<String, Double> map8 = mapper.generateUserHashMapForPost("u1503147", EvidenceType.THREAD, "2421111");
         */
        
        System.out.println(mapper.getBestAnswererIdForQuestion("3501383"));
        System.out.println("TEST FINISHED");
    }
}