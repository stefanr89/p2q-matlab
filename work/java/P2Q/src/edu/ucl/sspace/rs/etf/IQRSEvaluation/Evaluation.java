package edu.ucl.sspace.rs.etf.IQRSEvaluation;

import java.io.IOException;
import java.io.PrintStream;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.logging.XMLFormatter;

import edu.ucl.sspace.rs.etf.corpus.ESPreprocessor;
import edu.ucl.sspace.rs.etf.database.SemanticSpaceDBReader;
import edu.ucl.sspace.rs.etf.semantics.QuestionToProfileSimilarity;
import edu.ucl.sspace.rs.etf.semantics.QuestionToProfileSimilarityBagOfTasks;

public class Evaluation {

	final static Logger logger = Logger.getLogger("myLogger");
	static String startTime;

	public static void init(){
		try {

			DateFormat dateFormat = new SimpleDateFormat("dd-MM--HH-mm");
			Calendar cal = Calendar.getInstance();

			startTime = dateFormat.format(cal.getTime());

			FileHandler fh =new FileHandler("output/logger"+startTime+".log", false);
			fh.setFormatter(new XMLFormatter());
			logger.addHandler(fh);
			logger.setLevel(Level.OFF); // Put level.OFF for running

			System.setOut(new PrintStream("output/jobs-results-"+startTime+".txt"));


		} catch (SecurityException | IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws Exception {


		init();
		//System.setErr(new PrintStream("output/outputERR.txt"));

		//All evidence types:
		String[] aEs = new String[]{"Cat1","Cat2","Cat3","ConceptExtractor","SemNet","TFIDF"};
		//All categories
		String[] aCs = new String[]{"Cars & Transportation", "Computers & Internet", "Food & Drink", "Society & Culture", "Travel"};

		//System.out.println("--------------------------------------");
		
		

		/*System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
		//System.out.println("ConceptExtractor, SemNet, TFIDF");
		ExecutionJob[] jobs = new ExecutionJob[]{
				//original

				new ExecutionJob(true, true,EvidenceType.QUESTION, 1, new String[]{"ConceptExtractor"}, 20, new String[]{"Cars & Transportation", "Computers & Internet"}),	

				new ExecutionJob(true, true,EvidenceType.ANSWER, 2, new String[]{"ConceptExtractor"}, 20, new String[]{"Cars & Transportation", "Computers & Internet"}),	

				new ExecutionJob(true, true,EvidenceType.THREAD, 2, new String[]{"ConceptExtractor"}, 20, new String[]{"Cars & Transportation", "Computers & Internet"}),	

				new ExecutionJob(true, true,EvidenceType.ANSWER, 3, new String[]{"ConceptExtractor"}, 20, new String[]{"Cars & Transportation", "Computers & Internet"}),	

				new ExecutionJob(true, true,EvidenceType.THREAD, 3, new String[]{"ConceptExtractor"}, 20, new String[]{"Cars & Transportation", "Computers & Internet"}),	

				//new ExecutionJob(true, true,EvidenceType.QUESTION, 1, new String[]{"Cat1","Cat2","Cat3"}, 20, aCs),				

		};

		execute(jobs);
		//*/
		
		
		/*
		 * Execution plan
		 * ConceptExtractor, SemNet, TFIDF
		 * ConceptExtractor 
		 * Cat1
		 * Cat2
		 * Cat3
		 * Cat1, Cat2, Cat3
		 * Cat1&ConceptExtractor
		 * Cat1, Cat2, Cat3, ConceptExtractor
		 * TFIDF
		 * TFIDF & SemNet
		 * SemNet
		 * 
		 * DB Type: 2
		 * EvidenceType ANSWER,THREAD
		 *  
		 * DB Type: 3
		 * EvidenceType QUESTION,ANSWER,THREAD
		 * 
		 * TODO - Try different weights to Categories: 1, 1/2, 1/3
		 */	

System.out.println("--------------------------------------");
		
		
		System.out.println("DB Type: 1 **** EvidenceType QUESTION");

		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println("ConceptExtractor, SemNet, TFIDF");
		ExecutionJob[] jobs = new ExecutionJob[]{
				// Max2 unweighted
				new ExecutionJob(true,true, EvidenceType.QUESTION, 1, new String[]{"ConceptExtractor","SemNet","TFIDF"}, 20, aCs),	
				// Max2 weighted
				new ExecutionJob(false,true, EvidenceType.QUESTION, 1, new String[]{"ConceptExtractor","SemNet","TFIDF"}, 20, aCs),	
				
		};

		execute(jobs);
		
		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println("ConceptExtractor");
		jobs = new ExecutionJob[]{
				// Max2 unweighted
				new ExecutionJob(true,true, EvidenceType.QUESTION, 1, new String[]{"ConceptExtractor"}, 20, aCs),					
				// Max2 weighted
				new ExecutionJob(false,true, EvidenceType.QUESTION, 1, new String[]{"ConceptExtractor"}, 20, aCs),	
		};

		execute(jobs);

		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println("Cat1, Cat2, Cat3 - no weighted");
		jobs = new ExecutionJob[]{
				// Max2 unweighted
				new ExecutionJob(true,true, EvidenceType.QUESTION, 1, new String[]{"Cat1","Cat2","Cat3"}, 20, aCs),					
				// Max2 weighted
				new ExecutionJob(false,true, EvidenceType.QUESTION, 1, new String[]{"Cat1","Cat2","Cat3"}, 20, aCs),	
		};

		execute(jobs);

		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println("ConceptExtractor + Cat1,Cat2,Cat3");
		jobs = new ExecutionJob[]{				
				// Max2 unweighted
				new ExecutionJob(true,true, EvidenceType.QUESTION, 1, new String[]{"ConceptExtractor","Cat1","Cat2","Cat3"}, 20, aCs),									
				// Max2 weighted
				new ExecutionJob(false,true, EvidenceType.QUESTION, 1, new String[]{"ConceptExtractor","Cat1","Cat2","Cat3"}, 20, aCs),	
		};

		execute(jobs);

		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println("TFIDF");
		jobs = new ExecutionJob[]{
				// Max2 unweighted
				new ExecutionJob(true,true, EvidenceType.QUESTION, 1, new String[]{"TFIDF"}, 20, aCs),													
				// Max2 weighted
				new ExecutionJob(false,true, EvidenceType.QUESTION, 1, new String[]{"TFIDF"}, 20, aCs),	
		};

		execute(jobs);

		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println("TFIDF & SemNet");
		jobs = new ExecutionJob[]{
				// Max2 unweighted
				new ExecutionJob(true,true, EvidenceType.QUESTION, 1, new String[]{"SemNet","TFIDF"}, 20, aCs),													
				// Max2 weighted
				new ExecutionJob(false,true, EvidenceType.QUESTION, 1, new String[]{"SemNet","TFIDF"}, 20, aCs),	
		};

		execute(jobs);
		
		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println("SemNet");
		jobs = new ExecutionJob[]{
				// Max2 unweighted
				new ExecutionJob(true,true, EvidenceType.QUESTION, 1, new String[]{"SemNet"}, 20, aCs),																	
				// Max2 weighted
				new ExecutionJob(false,true, EvidenceType.QUESTION, 1, new String[]{"SemNet"}, 20, aCs),	
		};

		execute(jobs);

		System.out.println("--------------------------------------");
		
		
		System.out.println("DB Type: 2 **** EvidenceType ANSWER");

		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println("ConceptExtractor, SemNet, TFIDF");
		jobs = new ExecutionJob[]{
				//Max2 unweighted
				new ExecutionJob(true, true, EvidenceType.ANSWER, 2, new String[]{"ConceptExtractor","SemNet","TFIDF"}, 20, aCs),	
				// Max2 weighted
				new ExecutionJob(false,true, EvidenceType.ANSWER, 2, new String[]{"ConceptExtractor","SemNet","TFIDF"}, 20, aCs)	
				
		};

		execute(jobs);
		
		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println("ConceptExtractor");
		jobs = new ExecutionJob[]{
				//Max2 unweighted
				new ExecutionJob(true, true, EvidenceType.ANSWER, 2, new String[]{"ConceptExtractor"}, 20, aCs),	
				// Max2 weighted
				new ExecutionJob(false,true, EvidenceType.ANSWER, 2, new String[]{"ConceptExtractor"}, 20, aCs)	
				
		};

		execute(jobs);

		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println("Cat1, Cat2, Cat3 - no weighted");
		jobs = new ExecutionJob[]{
				//Max2 unweighted
				new ExecutionJob(true, true,EvidenceType.ANSWER, 2, new String[]{"Cat1","Cat2","Cat3"}, 20, aCs),	
				// Max2 weighted
				new ExecutionJob(false,true, EvidenceType.ANSWER, 2, new String[]{"Cat1","Cat2","Cat3"}, 20, aCs)				
		};

		execute(jobs);

		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println("ConceptExtractor + Cat1,Cat2,Cat3");
		jobs = new ExecutionJob[]{
				// Max2 unweighted
				new ExecutionJob(true, true, EvidenceType.ANSWER, 2, new String[]{"ConceptExtractor","Cat1","Cat2","Cat3"}, 20, aCs),	
				// Max2 weighted
				new ExecutionJob(false,true, EvidenceType.ANSWER, 2, new String[]{"ConceptExtractor","Cat1","Cat2","Cat3"}, 20, aCs)	
				
		};

		execute(jobs);

		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println("TFIDF");
		jobs = new ExecutionJob[]{
				// Max2 unweighted
				new ExecutionJob(true, true, EvidenceType.ANSWER, 2, new String[]{"TFIDF"}, 20, aCs),	
				// Max2 weighted
				new ExecutionJob(false,true, EvidenceType.ANSWER, 2, new String[]{"TFIDF"}, 20, aCs)	
				
		};

		execute(jobs);

		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println("TFIDF & SemNet");
		jobs = new ExecutionJob[]{
				// Max2 unweighted
				new ExecutionJob(true, true, EvidenceType.ANSWER, 2, new String[]{"SemNet","TFIDF"}, 20, aCs),	
				// Max2 weighted
				new ExecutionJob(false,true, EvidenceType.ANSWER, 2, new String[]{"SemNet","TFIDF"}, 20, aCs)	
				
		};

		execute(jobs);
		
		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println("SemNet");
		jobs = new ExecutionJob[]{
				// Max2 unweighted
				new ExecutionJob(true, true, EvidenceType.ANSWER, 2, new String[]{"SemNet"}, 20, aCs),	
				// Max2 weighted
				new ExecutionJob(false,true, EvidenceType.ANSWER, 2, new String[]{"SemNet"}, 20, aCs)	
				
		};

		execute(jobs);

		System.out.println("--------------------------------------");
		
		System.out.println("DB Type: 2 **** EvidenceType THREAD");

		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println("ConceptExtractor, SemNet, TFIDF");
		jobs = new ExecutionJob[]{
				// Max2 unweighted
				new ExecutionJob(true, true, EvidenceType.THREAD, 2, new String[]{"ConceptExtractor","SemNet","TFIDF"}, 20, aCs),	
				// Max2 weighted
				new ExecutionJob(false,true, EvidenceType.THREAD, 2, new String[]{"ConceptExtractor","SemNet","TFIDF"}, 20, aCs)	
				
		};

		execute(jobs);
		
		

		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println("ConceptExtractor");
		jobs = new ExecutionJob[]{
				// Max2 unweighted
				new ExecutionJob(true, true, EvidenceType.THREAD, 2, new String[]{"ConceptExtractor"}, 20, aCs),	
				// Max2 weighted
				new ExecutionJob(false,true, EvidenceType.THREAD, 2, new String[]{"ConceptExtractor"}, 20, aCs)	
				
		};

		execute(jobs);

		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println("Cat1, Cat2, Cat3 - no weighted");
		jobs = new ExecutionJob[]{
				// Max2 unweighted
				new ExecutionJob(false,true, EvidenceType.THREAD, 2, new String[]{"Cat1","Cat2","Cat3"}, 20, aCs)				
		};

		execute(jobs);

		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println("ConceptExtractor + Cat1,Cat2,Cat3");
		jobs = new ExecutionJob[]{
				// Max2 unweighted
				new ExecutionJob(true, true, EvidenceType.THREAD, 2, new String[]{"ConceptExtractor","Cat1","Cat2","Cat3"}, 20, aCs),	
				// Max2 weighted
				new ExecutionJob(false,true, EvidenceType.THREAD, 2, new String[]{"ConceptExtractor","Cat1","Cat2","Cat3"}, 20, aCs)	
				
		};

		execute(jobs);

		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println("TFIDF");
		jobs = new ExecutionJob[]{
				// Max2 unweighted
				new ExecutionJob(true, true, EvidenceType.THREAD, 2, new String[]{"TFIDF"}, 20, aCs),	
				// Max2 weighted
				new ExecutionJob(false,true, EvidenceType.THREAD, 2, new String[]{"TFIDF"}, 20, aCs)	
				
		};

		execute(jobs);

		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println("TFIDF & SemNet");
		jobs = new ExecutionJob[]{
				// Max2 unweighted
				new ExecutionJob(true, true, EvidenceType.THREAD, 2, new String[]{"SemNet","TFIDF"}, 20, aCs),	
				// Max2 weighted
				new ExecutionJob(false,true, EvidenceType.THREAD, 2, new String[]{"SemNet","TFIDF"}, 20, aCs)	
				
		};

		execute(jobs);
		
		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println("SemNet");
		jobs = new ExecutionJob[]{
				// Max2 unweighted
				new ExecutionJob(true, true, EvidenceType.THREAD, 2, new String[]{"SemNet"}, 20, aCs),	
				// Max2 weighted
				new ExecutionJob(false,true, EvidenceType.THREAD, 2, new String[]{"SemNet"}, 20, aCs)	
				
		};

		execute(jobs);

		System.out.println("--------------------------------------");
		
		
		System.out.println("DB Type: 3 **** EvidenceType ANSWER");

		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println("ConceptExtractor, SemNet, TFIDF");
		jobs = new ExecutionJob[]{
				// Max2 unweighted
				new ExecutionJob(true, true, EvidenceType.ANSWER, 3, new String[]{"ConceptExtractor","SemNet","TFIDF"}, 20, aCs),	
				// Max2 weighted
				new ExecutionJob(false,true, EvidenceType.ANSWER, 3, new String[]{"ConceptExtractor","SemNet","TFIDF"}, 20, aCs)	
				
		};

		execute(jobs);

		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println("ConceptExtractor");
		jobs = new ExecutionJob[]{
				// Max2 unweighted
				new ExecutionJob(true, true, EvidenceType.ANSWER, 3, new String[]{"ConceptExtractor"}, 20, aCs),	
				// Max2 weighted
				new ExecutionJob(false,true, EvidenceType.ANSWER, 3, new String[]{"ConceptExtractor"}, 20, aCs)	
				
		};

		execute(jobs);

		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println("Cat1, Cat2, Cat3 - no weighted");
		jobs = new ExecutionJob[]{
				// Max2 unweighted
				new ExecutionJob(true, true, EvidenceType.ANSWER, 3, new String[]{"Cat1","Cat2","Cat3"}, 20, aCs),
		};

		execute(jobs);

		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println("ConceptExtractor + Cat1,Cat2,Cat3");
		jobs = new ExecutionJob[]{
				// Max2 unweighted
				new ExecutionJob(true, true, EvidenceType.ANSWER, 3, new String[]{"ConceptExtractor","Cat1","Cat2","Cat3"}, 20, aCs),	
				// Max2 weighted
				new ExecutionJob(false,true, EvidenceType.ANSWER, 3, new String[]{"ConceptExtractor","Cat1","Cat2","Cat3"}, 20, aCs)	
				
		};

		execute(jobs);

		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println("TFIDF");
		jobs = new ExecutionJob[]{
				// Max2 unweighted
				new ExecutionJob(true, true, EvidenceType.ANSWER, 3, new String[]{"TFIDF"}, 20, aCs),	
				// Max2 weighted
				new ExecutionJob(false,true, EvidenceType.ANSWER, 3, new String[]{"TFIDF"}, 20, aCs)	
				
		};

		execute(jobs);

		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println("TFIDF & SemNet");
		jobs = new ExecutionJob[]{
				// Max2 unweighted
				new ExecutionJob(true, true, EvidenceType.ANSWER, 3, new String[]{"SemNet","TFIDF"}, 20, aCs),	
				// Max2 weighted
				new ExecutionJob(false,true, EvidenceType.ANSWER, 3, new String[]{"SemNet","TFIDF"}, 20, aCs)	
				
		};

		execute(jobs);
		
		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println("SemNet");
		jobs = new ExecutionJob[]{
				// Max2 unweighted
				new ExecutionJob(true, true, EvidenceType.ANSWER, 3, new String[]{"SemNet"}, 20, aCs),	
				// Max2 weighted
				new ExecutionJob(false,true, EvidenceType.ANSWER, 3, new String[]{"SemNet"}, 20, aCs)	
				
		};

		execute(jobs);

		System.out.println("--------------------------------------");
		
		System.out.println("DB Type: 3 **** EvidenceType THREAD");

		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println("ConceptExtractor, SemNet, TFIDF");
		jobs = new ExecutionJob[]{
				// Max2 unweighted
				new ExecutionJob(true, true, EvidenceType.THREAD, 3, new String[]{"ConceptExtractor","SemNet","TFIDF"}, 20, aCs),	
				// Max2 weighted
				new ExecutionJob(false,true, EvidenceType.THREAD, 3, new String[]{"ConceptExtractor","SemNet","TFIDF"}, 20, aCs)	
				
		};

		execute(jobs);

		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println("ConceptExtractor");
		jobs = new ExecutionJob[]{
				// Max2 unweighted
				new ExecutionJob(true, true, EvidenceType.THREAD, 3, new String[]{"ConceptExtractor"}, 20, aCs),	
				// Max2 weighted
				new ExecutionJob(false,true, EvidenceType.THREAD, 3, new String[]{"ConceptExtractor"}, 20, aCs)	
				
		};

		execute(jobs);

		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println("Cat1, Cat2, Cat3 - no weighted");
		jobs = new ExecutionJob[]{
				// Max2 unweighted
				new ExecutionJob(true,true, EvidenceType.THREAD, 3, new String[]{"Cat1","Cat2","Cat3"}, 20, aCs)				
		};

		execute(jobs);

		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println("ConceptExtractor + Cat1,Cat2,Cat3");
		jobs = new ExecutionJob[]{
				// Max2 unweighted
				new ExecutionJob(true, true, EvidenceType.THREAD, 3, new String[]{"ConceptExtractor","Cat1","Cat2","Cat3"}, 20, aCs),	
				// Max2 weighted
				new ExecutionJob(false,true, EvidenceType.THREAD, 3, new String[]{"ConceptExtractor","Cat1","Cat2","Cat3"}, 20, aCs)	
				
		};

		execute(jobs);

		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println("TFIDF");
		jobs = new ExecutionJob[]{
				// Max2 unweighted
				new ExecutionJob(true, true, EvidenceType.THREAD, 3, new String[]{"TFIDF"}, 20, aCs),	
				// Max2 weighted
				new ExecutionJob(false,true, EvidenceType.THREAD, 3, new String[]{"TFIDF"}, 20, aCs)	
				
		};

		execute(jobs);

		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println("TFIDF & SemNet");
		jobs = new ExecutionJob[]{
				// Max2 unweighted
				new ExecutionJob(true, true, EvidenceType.THREAD, 3, new String[]{"SemNet","TFIDF"}, 20, aCs),	
				// Max2 weighted
				new ExecutionJob(false,true, EvidenceType.THREAD, 3, new String[]{"SemNet","TFIDF"}, 20, aCs)	
				
		};

		execute(jobs);
		
		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println("SemNet");
		jobs = new ExecutionJob[]{
				// Max2 unweighted
				new ExecutionJob(true, true, EvidenceType.THREAD, 3, new String[]{"SemNet"}, 20, aCs),	
				// Max2 weighted
				new ExecutionJob(false,true, EvidenceType.THREAD, 3, new String[]{"SemNet"}, 20, aCs)	
				
		};

		execute(jobs);
	//*/
		
	}

	static void execute(ExecutionJob[] jobs) throws IOException,
			InterruptedException {
		for (ExecutionJob job : jobs) {
			System.out.println("Executing job: " + job);
			long time1 = System.currentTimeMillis();

			//seqExecution();
			parallelExecutionBagOfTasks(job);
            
			SemanticSpaceDBReader.Singleton.INSTANCE.getSingleton().reOpen();

            long time2 = System.currentTimeMillis();
			time2-=time1;
			System.out.println("Execution time:" + time2);
		}
	}


	public static void parallelExecutionBagOfTasks (ExecutionJob job) throws IOException, InterruptedException
	{


		List<Double> evalMRRValues = new ArrayList<Double>(), evalPrecisionValues = new ArrayList<Double>();

		int questionCount =0;


		try {


			//String questionId = "1022176";
			ArrayList<String> questions = new ArrayList<String>();
			for (String userId:EvidenceToHashMapMapper.getAllUserIdsForType(job.DBType, job.userCategories)){


				questions.add(EvidenceToHashMapMapper.getAllQuestionIdsForUserForEvaluation(userId).remove(0));
			}


			for (String questionId: questions) {

				//System.out.println("Start QUESTION Processing!!!");
				logger.log(Level.ALL, "Start QUESTION Processing!!!");
				long time1 = System.currentTimeMillis();

				ArrayList<String> usersList = new ArrayList<String>(EvidenceToHashMapMapper.getAllUserIdsForType(job.DBType, job.userCategories)); // getAllUserIdsForALLTypes());


				List<HashMap<String, Double>> hashMaps = new ArrayList<HashMap<String,Double>>();

				for (EvidenceToHashMapMapper mapper : job.mapperList) {
					hashMaps.add(mapper.generatePostHashMap(questionId));
				}

				HashMap<String, Double> questionEvidences =  EvidenceJoiner.joinEvidences(hashMaps); //mapperTFIDF.generatePostHashMap(questionId);

				//System.out.println("q: " + questionEvidences);

				questionEvidences = ESPreprocessor.Singleton.INSTANCE
						.getSingleton().filterWeightedWords(questionEvidences,
								false, true);

				//System.out.println("preproc q: " + questionEvidences);
				logger.log(Level.ALL, "preproc q: " + questionEvidences);


				ConcurrentHashMap<String, Double> results = new ConcurrentHashMap<String, Double>();

				int maxThreads = job.maxThreads;
				ExecutorService executor = Executors.newFixedThreadPool(maxThreads);
				for (int i = 0; i < maxThreads; i++) {

					QuestionToProfileSimilarityBagOfTasks sThread = new QuestionToProfileSimilarityBagOfTasks(
							questionId, questionEvidences, usersList, results,job.mapperList,
							job.evidenceType, job.isOriginal, job.isMax);
					executor.execute(sThread);

				}
				executor.shutdown();
				try {
					executor.awaitTermination(Long.MAX_VALUE,
							TimeUnit.NANOSECONDS);
				} catch (InterruptedException e) {

				}

				String bestAnswererIdForQuestion = EvidenceToHashMapMapper.getBestAnswererIdForQuestion(questionId);
				double rank = Evaluator.calculateRank(bestAnswererIdForQuestion, results);
				double resultMRRRank = Evaluator.calculateMRRRank(rank);
				evalMRRValues.add(resultMRRRank);
				evalPrecisionValues.add(rank);

				long time2 = System.currentTimeMillis();
				time2-=time1;
				logger.log(Level.INFO,"Question Processing Execution time:" + time2);
				logger.log(Level.INFO,"Result calculateMRRRank: " + resultMRRRank);
				logger.log(Level.INFO,"Rank is: "+rank+" out of :"+results.size());
				logger.log(Level.INFO,"Question Count: " + ++questionCount);				

			}



			//			System.out.println("******ORDERD LIST OF USERS*******");
			//			System.out.println("q: "+questionEvidences);


			//System.out.println("RES For the best answerer (u1517486) : "+results.get("u1517486").doubleValue());

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("The final MRR result is: " + Evaluator.calculateMRR(evalMRRValues));
		System.out.println("The final P@5 result is: " + Evaluator.calculatePrecisionAtN(evalPrecisionValues, 5));
		System.out.println("The final P@10 result is: " + Evaluator.calculatePrecisionAtN(evalPrecisionValues, 10));
		System.out.println("The final P@15 result is: " + Evaluator.calculatePrecisionAtN(evalPrecisionValues, 15));
		System.out.println("The final P@20 result is: " + Evaluator.calculatePrecisionAtN(evalPrecisionValues, 20));
		System.out.println("The final P@30 result is: " + Evaluator.calculatePrecisionAtN(evalPrecisionValues, 30));
		System.out.println("Number of questions: " + questionCount);

	}

	/*	static void seqExecution() throws Exception, SQLException, IOException {
	QuestionToProfileSimilarity sSim = new QuestionToProfileSimilarity(false, false);

	EvidenceToHashMapMapper mapper = new EvidenceToHashMapMapper("Cat1"); //TODO InfoSourceId commented
	HashMap<String, Double> q= mapper.generatePostHashMap("1022176");

	//HashMap<String, HashMap<String, Double>> userMap = mapper.generateAnswerUserHashMaps();

	System.out.println("q: "+q);
	q = ESPreprocessor.Singleton.INSTANCE.getSingleton().filterWeightedWords(q, false, true);     	  
	System.out.println("preproc q: "+q);

	TreeMap<String, Double> result = new TreeMap<String, Double>();


	for (String user : mapper.getAllUserIds()) {

		HashMap<String, Double> u= mapper.generateUserHashMapForPost(user, EvidenceType.THREAD, "1022176");

		u = ESPreprocessor.Singleton.INSTANCE.getSingleton().filterWeightedWords(u, false, true);    

		System.out.println(user+": "+u);

		double val = sSim.getWeightedSimilarity(q, u);
		result.put(user, val);

		System.out.println("Sim: "+val);

	}

	System.out.println("******ORDERD LIST OF USERS*******");
	System.out.println("q: "+q);

	for (Iterator iterator = result.navigableKeySet().iterator(); iterator.hasNext();) {
		System.out.println(iterator.next());

	}

	// bestAnswer userId='u1517486'
}
	 */
	/*	public static void parallelExecution () throws IOException, InterruptedException
	{

		try {
			EvidenceToHashMapMapper mapper = new EvidenceToHashMapMapper("TFIDF"); //TODO InfoSourceId commented
			HashMap<String, Double> q= mapper.generatePostHashMap("1022176");

			//HashMap<String, HashMap<String, Double>> userMap = mapper.generateAnswerUserHashMaps();

			System.out.println("q: "+q);
			q = ESPreprocessor.Singleton.INSTANCE.getSingleton().filterWeightedWords(q, false, true);     	  
			System.out.println("preproc q: "+q);

			ConcurrentHashMap<String, Double> result = new ConcurrentHashMap<String, Double>();

			int maxThreads = 5;//, currentThreads = 0;

			ExecutorService executor = Executors.newFixedThreadPool(maxThreads);

			ConcurrentHashMap<String,HashMap<String, Double>> users = new ConcurrentHashMap<String, HashMap<String,Double>>(mapper.generateAnswerUserHashMaps()); //generateUserHashMapForPost

			for (int i = 0; i < maxThreads; i++) {

				QuestionToProfileSimilarity sThread = new QuestionToProfileSimilarity(q, users, result, false, true); 

				executor.execute(sThread);

			}




			executor.shutdown();

			try {
				executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
			} catch (InterruptedException e) {

			}




			System.out.println("******ORDERD LIST OF USERS*******");
			System.out.println("q: "+q);

			//		for (Iterator iterator = result.iterator(); iterator.hasNext();) {
			//			System.out.println(iterator.next());
			//			
			//		}


			//			List<String> al = new ArrayList<String>(result.values());
			//			Collections.sort(al);


			System.out.println("RES :"+result.get("u1517486").doubleValue());

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	 */
}

class ExecutionJob {

	//static int id =0;
	boolean isOriginal;
	boolean isMax;
	EvidenceType evidenceType;
	int DBType;
	List<EvidenceToHashMapMapper> mapperList = new ArrayList<EvidenceToHashMapMapper>();
	int maxThreads;
	String[] userCategories;
	String note;

	ExecutionJob( boolean original, boolean max, EvidenceType evidenceType, int DBType, String[] listOfEvidenceTypes, int maxThreads, String[] userCategories) {
		isOriginal = original;
		isMax = max;
		this.evidenceType = evidenceType;
		this.DBType = DBType;
		this.maxThreads = maxThreads;
		this.userCategories = userCategories;
		try {

			for (String string : listOfEvidenceTypes) {

				mapperList.add(new EvidenceToHashMapMapper(string));

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public String toString() {
		return "JOB(isOriginal="+isOriginal+", isMax="+isMax+", evidenceType="+evidenceType+", DBType="+DBType+", InformationSourceIDs="+ Arrays.toString(mapperList.toArray()) +", UserCategories="+ Arrays.toString(userCategories) +")";
	}
}


