/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.ucl.sspace.rs.etf.semantics;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Davor, Bojan Furlan
 */
public class StringSimilarity {

    public int sigmaM=0;    // kraci
    public int sigmaN=0;    // duzi
    // dimension of matrix
    public int m=0;         // kraci
    public int n=0;         // duzi
    // List of P and R
    ArrayList<String> P=new ArrayList<String>();
    ArrayList<String> R=new ArrayList<String>();
    
    //longer sentence
    HashMap<String, Double> refP;
    //shorter sentence
    HashMap<String, Double> refR;
    public String[] sameWords;
    
    public StringSimilarity(){
    	//throw new Error("Use parametrized constructor");
    }

    public StringSimilarity(HashMap<String, Double> sent1, HashMap<String, Double> sent2){
    	
    	String[] strSentence1 = (String[]) sent1.keySet().toArray(new String[sent1.keySet().size()]);
        String[] strSentence2 = (String[])sent2.keySet().toArray(new String[sent2.keySet().size()]);
        
        String strShorterSent[]=null;
        String strLongerSent[]=null;

        if(strSentence1.length>=strSentence2.length){
            strLongerSent=strSentence1;
            strShorterSent=strSentence2;
            refP=sent1;
            refR=sent2;
        }else{
            strLongerSent=strSentence2;
            strShorterSent=strSentence1;
            refP=sent2;
            refR=sent1;
        }

        ajustParams(strShorterSent, strLongerSent);	
    }
 
	void ajustParams(String[] strShorterSent, String[] strLongerSent) {
		// IZMENJENO - Stajalo String[] R = ...
        sameWords = new String[strShorterSent.length];    // maximal

        int k=0;
        boolean isFounded=false;


        // Nadji iste stringove, popuni lok. R sa tim stringovima
        // i popuni this.R sa razlicitim stringovima
        for(int i=0;i<strShorterSent.length;i++){
            for(int j=0;j<strLongerSent.length;j++){
                if(strLongerSent[j].equals(strShorterSent[i])){
                   isFounded=true;
                   sameWords[k++]=strShorterSent[i];
                   break;                    // Obavezno zbog prekoracenja
                }
            }
            if(!isFounded) this.R.add(strShorterSent[i]);
            isFounded=false;
        }
        sigmaM=k;
        k=0;

        // Isto to samo za this.P
        isFounded=false;
        for(int i=0;i<strLongerSent.length;i++){
            for(int j=0;j<strShorterSent.length;j++){
                if(strShorterSent[j].equals(strLongerSent[i])){
                    isFounded=true;
                    k++;
                    break;
                }
            }
            if(!isFounded) this.P.add(strLongerSent[i]);
            isFounded=false;
        }
        sigmaN=k;

        this.m=strShorterSent.length;
        this.n=strLongerSent.length;

        
        // DODATO - sabijanje niza sameWords
        String [] temp = new String[sigmaM];
        for (int i=0; i<sigmaM; i++)
        	temp[i] = sameWords[i];
        sameWords = temp;
	}
    
public StringSimilarity(String[] strSentence1, String[] strSentence2){

	String strShorterSent[]=null;
        String strLongerSent[]=null;

        if(strSentence1.length>=strSentence2.length){
            strLongerSent=strSentence1;
            strShorterSent=strSentence2;
        }else{
            strLongerSent=strSentence2;
            strShorterSent=strSentence1;
        }

        ajustParams(strShorterSent, strLongerSent);	
    }
    
    public int getM() {
        return m;
    }

    public int getN() {
        return n;
    }

    public int getSigmaM() {
        return sigmaM;
    }

    public int getSigmaN() {
        return sigmaN;
    }

    

   /*  LSC(longest common subsequence) taken from wikipedia               */
    private String lcs(String a, String b) {
        int[][] lengths = new int[a.length()+1][b.length()+1];
        // row 0 and column 0 are initialized to 0 already
        for (int i = 0; i < a.length(); i++)
            for (int j = 0; j < b.length(); j++)
                if (a.charAt(i) == b.charAt(j))
                    lengths[i+1][j+1] = lengths[i][j] + 1;
                else
                    lengths[i+1][j+1] = Math.max(lengths[i+1][j], lengths[i][j+1]);
                    // read the substring out from the matrix
        StringBuffer sb = new StringBuffer();
        for (int x = a.length(), y = b.length();
        x != 0 && y != 0; )
        {
            if (lengths[x][y] == lengths[x-1][y])
                x--;
            else if (lengths[x][y] == lengths[x][y-1])
                y--;
            else {
                assert a.charAt(x-1) == b.charAt(y-1);
                sb.append(a.charAt(x-1));
                x--;
                y--;
            }
        }     return sb.reverse().toString();
    }

    /* maximal consecutive longest common subsequence(MCLCS1)                */
    /* takes two strings                                                     */
    /*as input and returns the shorter string or maximal consecutive portions*/
    /* of the shorter string that consecutively match with the longer string,*/
    /* where matching                                                        */
    /* must be from first character (character 1) for both strings           */

    private String mclcs1(String str1,String str2){
        String shorterString=null;
        String longerString=null;
        if(str1.length()>=str2.length()){
            shorterString=str2;
            longerString=str1;
        }
        else{
            shorterString=str1;
            longerString=str2;
        }

        int endIndex=0;
        for(int i=0;i<shorterString.length();i++){
            if(shorterString.charAt(i)==longerString.charAt(i)) endIndex++;
            else break;
        }

        return longerString.substring(0,endIndex);
    }

    /* maximal consecutive longest common subsequence(MCLCSn)                 */
    /* The same as perevios but matching can start of any character           */
    /* Nisam siguran da ovo radi                                              */
    /* Sredi malo ovaj kod                                                    */

    private String mclcsN(String str1,String str2){
        String shorterString=null;
        String longerString=null;
        if(str1.length()>=str2.length()){
            shorterString=str2;
            longerString=str1;
        }
        else{
            shorterString=str1;
            longerString=str2;
        }

        String retString="";
        int beginIndex=0,endIndex=0;
        int max=0;
        int sum=0;
        boolean bSubSeq=false;

        for(int i=0;i<shorterString.length();i++){
            int k=i;
            sum=0;
            beginIndex=0;
            for(int j=0;j<longerString.length();j++){
                
                if(longerString.charAt(j)==shorterString.charAt(k)){
                    if(!bSubSeq) beginIndex=j;
                    k++;
                    sum++;
                    bSubSeq=true;
                }else if(bSubSeq){
                    
                    bSubSeq=false;
                    
                    if(sum>max){
                        max=sum;
                        try{
                        retString=longerString.substring(beginIndex,j);
                        }
                        catch(Exception ex){
                          System.out.println(beginIndex+"   "+j);
                        }
                        // return k to i ?
                       

                    }
                    k=i;
                    sum=0;
                    beginIndex=0;
                }
                    // mora i ovo da se proveri, ako se slicnost poklapa
                    // do kraceg stringa, jer onda ne ulazi u else granu
                if( ( (j+1)==longerString.length() || k==shorterString.length() )&& bSubSeq  ){
                     bSubSeq=false;

                    if(sum>max){
                        max=sum;
                        try{
                        retString=longerString.substring(beginIndex,j+1);
                        }
                        catch(Exception ex){
                          System.out.println(beginIndex+"   "+j);
                        }
                        // return k to i ?


                    }
                    k=i;
                    sum=0;
                    beginIndex=0;

                }
            }

        }

        return retString;

    }

    /* Get string similarity- using lcs,mcls1,and mclsN and normalize it */

    public double getStringSimilarity(String word1,String word2){
        String strLCS=lcs(word1,word2);
        String strMCLCS1=mclcs1(word1,word2);
        String strMCLCSN=mclcsN(word1,word2);

        // Normalize it
        double v1= ( strLCS.length()*strLCS.length() );
        v1/=( word1.length()*word2.length() );
        double v2= (strMCLCS1.length()*strMCLCS1.length() );
        v2/=( word1.length()*word2.length() );
        double v3= (strMCLCSN.length()*strMCLCSN.length() );
        v3/=( word1.length()*word2.length() );

        // default value for weight is 0.33
        double alfa=v1*0.33+v2*0.33+v3*0.33;

        return alfa;

    }

    /* Get common word order similarity S0 is the result. See documentation */
    /* this method evaluate sigma and fill P and R array list too           */
    /* wich is needed for step 2(alg.for overall similirity)                */
    /* Some bugs are asserted here, see line 13 from Microsoft Paraphrase   */

   
    public double getCommonWordOrderSimilarity(String[] strSentence1,String[] strSentence2){
        double dSim=0.0;

        String strShorterSent[]=null;
        String strLongerSent[]=null;

        if(strSentence1.length>=strSentence2.length){
            strLongerSent=strSentence1;
            strShorterSent=strSentence2;
        }else{
            strLongerSent=strSentence2;
            strShorterSent=strSentence1;
        }

        ajustParams(strShorterSent, strLongerSent);
        
        /* Za sada ne racunamo common worder */
        /*
        int[] X=new int[k];
        int[] Y=new int[k];
        // forming X, k is sigma
        for(int i=0;i<k;i++) X[i]=i+1;
        // forming Y
        // searching P
        int m=0;
        for(int i=0;i<strLongerSent.length;i++){
            for(int j=0;j<R.length;j++){
                if(strLongerSent[i].equals(R[j])){
                    Y[m++]=j+1;
                    break;                // Mora zbog prekoracenja
                }
            }
        }

        // forming sum
        double sum1=0;
        double sum2=0;
        for(int i=0;i<k;i++){
            sum1+=Math.abs(X[i]-Y[i]);
            sum2+=Math.abs(X[i]-X[(k-1)-i]);
        }

        // finaly dSum is S0
        dSim=1-sum1/sum2;
        sigma=k;

        this.m=strShorterSent.length;
        this.n=strLongerSent.length;
         *
         */


        dSim=1.0;
        return dSim;
    }

    public double[][] getStringSimilarityMatrix(){

        // m and n are counted before with calling getCommonWordOrderSimilarity
        // wich was done in step 2 in SentenceSimilarity class
        // this.P and this.R are formed too
        double [][] simMatrix=new double[m-sigmaM][n-sigmaN];

        for(int i=0;i<m-sigmaM;i++)
            for(int j=0;j<n-sigmaN;j++){
                // for all P
                double alfa=getStringSimilarity(P.get(j),R.get(i));
                simMatrix[i][j]=alfa;
            }


        return simMatrix;
    }
    

    public static void main(String[] args){

        String[] str1={"many","consider","maradona","as","the","best","player","in","soccer","history"};
        String[] str2={"maradona","is","one","of","the","best","soccer","player"};

        StringSimilarity ss=new StringSimilarity();
        ss.getCommonWordOrderSimilarity(str1, str2);
        int f;

    }

}
