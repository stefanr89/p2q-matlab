/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.ucl.sspace.rs.etf.semantics;

import edu.ucl.sspace.rs.etf.corpus.AutomaticStopWordGenerator;
import edu.ucl.sspace.rs.etf.corpus.CorpusManagementES;
import edu.ucl.sspace.rs.etf.corpus.ESPreprocessor;
import edu.ucl.sspace.rs.etf.corpus.FileProcessingInfoES;
import edu.ucl.sspace.rs.etf.corpus.FileReaderES;
import edu.ucl.sspace.rs.etf.corpus.FileWriterES;
import edu.ucl.sspace.rs.etf.corpus.SrCorpusProcessor;
import edu.ucl.sspace.rs.etf.corpus.StopWordListBuilder;
import edu.ucl.sspace.rs.etf.corpus.WikiCorpusReader;
import edu.ucl.sspace.rs.etf.semantics.SemanticAnalysesObtainedAlgs.SAAlgType;
import edu.ucl.sspace.rs.etf.stemmer.SerbianStemmer;
import edu.ucl.sspace.rs.etf.database.ESConnection;
import edu.ucl.sspace.rs.etf.database.SemanticSpaceDBCreator;
import edu.ucl.sspace.rs.etf.database.SemanticSpaceDBReader;
import edu.ucl.sspace.rs.etf.paraphrase.Article;
import edu.ucl.sspace.rs.etf.paraphrase.NewsPageProcessor;
import edu.ucl.sspace.rs.etf.paraphrase.ParaphraseCorpusBuilder;
import edu.ucl.sspace.rs.etf.paraphrase.ParaphraseGrader;
import edu.ucla.sspace.coals.Coals;
import edu.ucla.sspace.common.SemanticSpace;
import edu.ucla.sspace.common.SemanticSpaceIO;
import edu.ucla.sspace.common.Similarity;
import edu.ucla.sspace.lsa.LatentSemanticAnalysis;
import edu.ucla.sspace.text.DocumentPreprocessor;
import edu.ucla.sspace.vector.DoubleVector;
import edu.ucla.sspace.vector.Vector;
//import edu.ucla.sspace.vector.VectorIOES;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Writer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author Vuk Batanovic, Bojan Furlan
 */
public class TestES {

	public static void main(String[] args) throws IOException, InterruptedException{

		System.out.println("Start Processing!!!");
		long time1 = System.currentTimeMillis();

		//        preprocesCorpusAndBuildStopWordsList();
		
		// makeSSpaceToFile();

		//String str1 = "Prvi teniser sveta Španac Rafael Nadal trijumfovao je na Otvorenom prvenstvu Engleske u Vimbldonu, buduci da je danas u finalu pobedio Ceha Tomaša Berdiha rezultatom 6:3, 7:5, 6:4.";
		//String str2 = "Španski teniser Rafael Nadal osvojio je danas titulu na turniru u Vimbldonu, pošto je savladao Ceha Tomaša Berdiha sa 3:0 (6:3, 7:5, 6:4).";

		//        String sent1 = "Podizno-spuštajuća platforma za osobe sa posebnim potrebama, juče je puštena u rad u pešačkim prolazima na Terazijama";// ispred Igumanove palate.";
		//        String sent2 = "Osobe sa invaliditetom i svi koji imaju poteškoće sa kretanjem od juče mogu da koriste podizno-spuštajuću platformu u podzemnom pešačkom prolazu na Terazijama u Beogradu.";


//		String sent1 = "computer science is looking for hight tech improvment"; 
//		String sent2 = "history science try to prove what happen a long time ago"; 


		//simpleTestENGTwoSentances(sent1, sent2);
		//simpleTestENGTwoWeightedSentances();
		//simpleTestSRBTwoSentancesRAM(sent1, sent2);
		//simpleTestENGTwoSentancesRAM(sent1, sent2);

		
		
		StemStopWordsList();

	//    Komande za evaluaciju tj. ocenjivanje korpusa parafraza
	//    Imena fajlova treba uskladiti sa semantickim algoritmom selektovanim u klasi SentenceSimilarity

//		boolean isOriginal = true, isMax=false, removeStopWords = true;  
//		//ESPreprocessor.Singleton.INSTANCE.getSingleton().initializeListOfStopWords("resource/eng/english-stemmed-stop-words.txt");
//
//		String folder = "Rez/I&I-BEZStopWords/";
//		File theDir = new File(folder);
//
//		  // if the directory does not exist, create it
//		  if (!theDir.exists())
//		  {
//		    boolean result = theDir.mkdir();  
//		    if(result){    
//		       System.out.println("DIR created "+folder);  
//		     }
//
//		  }
//		  
//
//		HashMap<String, Double> tfHashMap = tfFileToHashMap("CorpusENGStemTermFrequencies.txt");
//	    gradeFile ("resource/MSPRC/MSPRC-Train-Cleaned.txt",folder+ "ResultsMSPRC-Train-Cleaned-COALS.txt",tfHashMap, isOriginal, isMax, removeStopWords);
//        extractResults (folder+ "ResultsMSPRC-Train-Cleaned-COALS.txt",folder+  "ResultNumbersTrainSet-COALS.txt");
//	    gradeFile ("resource/MSPRC/MSPRC-Test-Cleaned.txt",folder+ "ResultsMSPRC-Test-Cleaned-COALS.txt", tfHashMap, isOriginal, isMax, removeStopWords);
//        extractResults (folder+"ResultsMSPRC-Test-Cleaned-COALS.txt", folder+"ResultNumbersTestSet-COALS.txt");
//		ResultsGraderENG rg = new ResultsGraderENG(folder+"ResultNumbersTrainSet-COALS", false);
//        rg.produceBestThreshold();


		
		/*
		StemStopWordsList();
		
		
        //saveSemanticSpace ("SSFILE_COALS_StopWordsStdDev8.txt", "SSFILE_RI_StopWordsStdDev8.txt");
 

        gradeFileRAM ("resource/ParaphraseTrainSet.txt", "ResultsTrainSetCoalsWikiStopWordsStdDev8+TF.txt", "SSFILE_COALS_StopWordsStdDev8.txt", "resource/CorpusStemTermFrequencies.txt");
        gradeFileRAM ("resource/ParaphraseTestSet.txt", "ResultsTestSetCoalsWikiStopWordsStdDev8+TF.txt", "SSFILE_COALS_StopWordsStdDev8.txt", "resource/CorpusStemTermFrequencies.txt");

       

        extractResults ("ResultsTrainSetCoalsWikiStopWordsStdDev8+TF.txt", "ResultNumbersTrainSetCoalsWikiStopWordsStdDev8+TF.txt");
        extractResults ("ResultsTestSetCoalsWikiStopWordsStdDev8+TF.txt", "ResultNumbersTestSetCoalsWikiStopWordsStdDev8+TF.txt");
        ResultsGrader rg = new ResultsGrader ("ResultNumbersTrainSetCoalsWikiStopWordsStdDev8+TF", false);
        rg.produceBestThreshold();

    	// Procena stepena poklapanja ocena dvoje ocenjivaca
    	/*
        File fajl1 = new File ("ParaphraseTestSet.txt");
        File fajl2 = new File ("ParaphraseTestSetGradedRC.txt");
        BufferedReader br1 = new BufferedReader (new InputStreamReader (new FileInputStream (fajl1)));
        BufferedReader br2 = new BufferedReader (new InputStreamReader (new FileInputStream (fajl2)));
        String str1 = null, str2 = null;
        int sameCount = 0, totalCount = 0;
        while ((str1 = br1.readLine()) != null && (str2 = br2.readLine()) != null)
        {
            totalCount++;
            if ((str1.startsWith("0") && str2.startsWith("0")) || (str1.startsWith("1") && str2.startsWith("1")))
                sameCount++;
        }
        double percentage = sameCount*1.0 / totalCount;
        System.out.println("Poklapanje rezultata iznosi: " + percentage + " procenata");
        System.out.println("Poklapa se " + sameCount + " od ukupno " + totalCount + " recenica");
		 */

		/*
  //     Komande za pretprocesiranje, procesiranje, postprocesiranje korpusa i snimanje semantickog prostora u bazu
         preprocessCorpus ("srwiki-20110826-abstract.xml", "CorpusStemmerIzlaz.txt");
         processCorpus ("CorpusStemmerIzlaz.txt", "SSFILE_COALS.txt", "SSFILE_RI.txt");
         saveSemanticSpace ("SSFILE_COALS.txt", "SSFILE_RI.txt");


  //     Komanda za pribavljanje korpusa parafraza sa sajta www.vesti.rs     
  //     NewsPageProcessor.buildParaphraseCorpus("2011BestParaphraseCorpus.txt");

  //     Jednostavno okruzenje za ocenjivanje parafraza


  //     Komanda za podelu korpusa parafraza na train i test skup       
  //     ParaphraseCorpusBuilder pcb = new ParaphraseCorpusBuilder ();
  //     pcb.buildTrainAndTestCorpus("ParaphraseCompleteSet.txt");  

  //    Komande za evaluaciju tj. ocenjivanje korpusa parafraza
  //    Imena fajlova treba uskladiti sa semantickim algoritmom selektovanim u klasi SentenceSimilarity
        gradeFile ("ParaphraseTrainSet.txt", "ResultsTrainSetRIWiki.txt");
        extractResults ("ResultsTrainSetRIWiki.txt", "ResultNumbersTrainSetRIWiki.txt");

		 */
		
		 long time2 = System.currentTimeMillis();
	       time2-=time1;
	       System.out.println("Execution time:" + time2);

	}


	static void makeSSpaceToFile(){
		//     Komande za pretprocesiranje, procesiranje, postprocesiranje korpusa i snimanje semantickog prostora u bazu
		//        preprocessCorpus ("srwiki-20110826-abstract.xml", "CorpusStemmerIzlaz.txt");
		//        processCorpus ("CorpusStemmerIzlaz.txt", "SSFILE_COALS.txt", "SSFILE_RI.txt");
		//        saveSemanticSpace ("SSFILE_COALS.txt", "SSFILE_RI.txt");

		try {
			
			preprocessENGCorpus("corpusfinalENG.txt", "corpusfinalENGStemmerIzlaz.txt","resource/eng/english-stop-words.txt");
			processCorpus ("corpusfinalENGStemmerIzlaz.txt", "SSFILE_COALS-ENG.txt", "SSFILE_RI-ENG.txt");
			
			// saveSemanticSpace ("SSFILE_COALS-ENG.txt", "SSFILE_RI-ENG.txt"); ukoliko treba u bazu
			
			//freq and TFnorm
			AutomaticStopWordGenerator.buildLexiconAndCalculateFreq("corpusfinalENGStemmerIzlaz.txt","CorpusENGStemLexicon.txt", "CorpusENGStemTermFrequencies.txt");

			//AutomaticStopWordGenerator.rankLexiconAndFillDB();
			
		} catch (Exception e) {
		}
	}
	
	static void simpleTestENGTwoSentancesRAM(String sent1, String sent2)
			throws IOException, FileNotFoundException {
		
		//suvise dugo traje!!!
		SemanticSpace sspace = SemanticSpaceIO.load("SSFILE_RI-ENG.txt");

		// Ucitavanje ucestanosti svih stemova u hash mapu
		HashMap<String, Double> termFrequencies = new HashMap<String, Double>();

		try {
			Connection con=null;
			PreparedStatement ps=null;
			ResultSet rs=null;

			con=ESConnection.getConnection();
			String sqlString="SELECT word, freq FROM Freq WHERE informationSourceId='TFIDF';";
			ps=con.prepareStatement(sqlString);
			rs=ps.executeQuery();

			while(rs.next()){
				String word=rs.getString("word");
				double freq = rs.getDouble("freq"); 
				termFrequencies.put(word, freq);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		double rez = SentenceSimilarityRAM.getSimilarity(sent1.split(" "), sent2.split(" "), sspace, termFrequencies);
		System.out.println(rez);
	}

	static void simpleTestSRBTwoSentancesRAM(String sent1, String sent2,String sspaceFile, String tfidfFile)
			throws IOException, FileNotFoundException {
		SemanticSpace sspace = SemanticSpaceIO.load(sspaceFile); //"SSFILE_COALS_StopWordsStdDev8.txt");

		HashMap<String, Double> termFrequencies = tfFileToHashMap(tfidfFile);

		double rez = SentenceSimilarityRAM.getSimilarity(sent1.split(" "), sent2.split(" "), sspace, termFrequencies);
		System.out.println(rez);
	}


	static HashMap<String, Double> tfFileToHashMap(String tfidfFile)
			throws FileNotFoundException, IOException {
		// Ucitavanje ucestanosti svih stemova u hash mapu
		HashMap<String, Double> termFrequencies = new HashMap<String, Double>();
		File tfFile = new File (tfidfFile);	// "resource/CorpusStemTermFrequencies.txt");
		BufferedReader br =  new BufferedReader (new InputStreamReader (new FileInputStream (tfFile)));
		String line = null;
		while ((line = br.readLine()) != null)
		{
			String [] niz = line.split("\\s");
			termFrequencies.put(niz[0], Double.parseDouble(niz[3]));
		}
		br.close();
		return termFrequencies;
	}

	static void preprocesCorpusAndBuildStopWordsList()
			throws FileNotFoundException, IOException {
		preprocessCorpus ("srwiki-20110826-abstract.xml", "CorpusStemmerIzlazStopWordsStdDev8.txt");
		preprocessCorpus ("CorpusCleanerIzlazTest.txt", "CorpusStemmerIzlazStopWordsTest10.txt");
		processCorpus ("CorpusStemmerIzlazStopWordsStdDev8.txt", "SSFILE_COALS_StopWordsStdDev8.txt", "SSFILE_RI_StopWordsStdDev8.txt");

		AutomaticStopWordGenerator.buildLexiconAndCalculateFreq("resource/CorpusStemmerIzlaz_WithStopWords.txt","CorpusStemLexicon.txt", "CorpusStemTermFrequencies.txt");
		AutomaticStopWordGenerator.rankLexiconAndFillDB();
		AutomaticStopWordGenerator.generateStopWordList("StopWordsListStdDev8.txt", 8, 1);
	}

	static void simpleTestENGTwoWeightedSentances() throws IOException {
		HashMap<String, Double> sent1 = new HashMap<String, Double>(){
			{
				put("computer",0.1);  //"computer science is looking for hight tech improvment";
				put("science",0.5);
				put("is",0.1); 
				put("looking",0.5);
				put("for",0.1); 
				put("hight",0.5);
				put("tech",0.1); 
				put("improvment",0.5);

			}
		};
		HashMap<String, Double> sent2 = new HashMap<String, Double>(){
			{
				put("history",0.1); //history science try to prove what happen a long time ago"
				put("science",0.5);
				put("try",0.1); 
				put("to",0.5);
				put("prove",0.1); 
				put("what",0.5);
				put("happen",0.1); 
				put("a",0.5);
				put("long",0.1); 
				put("time",0.5);
				put("ago",0.1); 

			}
		};

		System.out.println("Some random weights to words in sentence");	
		System.out.println(sent1);
		System.out.println(sent2);


		//DocumentPreprocessor dp=new DocumentPreprocessor();
		//sent1 = dp.process(sent1);
		sent1 = ESPreprocessor.Singleton.INSTANCE.getSingleton().filterWeightedWords(sent1, false, true);     	  
		sent2 = ESPreprocessor.Singleton.INSTANCE.getSingleton().filterWeightedWords(sent2, false, true);    

		System.out.println("After preprocessing:");

		System.out.println("Sentence1 :"+sent1);
		System.out.println("Sentence2 :"+sent2);

		//System.out.println("Similarity is: "+ SentenceSimilarityEng.getMaxWeightedSimilarity(sent1, sent2)); 
		System.out.println("Similarity is: "+ SentenceSimilarity.getWeightedSimilarity(sent1, sent2));
	}

	//Test obican ENG   	
	static void simpleTestENGTwoSentances(String sent1, String sent2) throws IOException {


		//String sent1 = SrCorpusProcessor.processSrString(str1);
		DocumentPreprocessor dp=new DocumentPreprocessor();
		sent1 = dp.process(sent1);
		sent1 = ESPreprocessor.Singleton.INSTANCE.getSingleton().filter(sent1, false, true);    

		//String sent2 = SrCorpusProcessor.processSrString(str2);
		dp=new DocumentPreprocessor();
		sent2 = dp.process(sent2);
		sent2 = ESPreprocessor.Singleton.INSTANCE.getSingleton().filter(sent2, false, true);

		System.out.println(sent1);
		System.out.println();
		System.out.println(sent2);

		System.out.println("Similarity is: "+ SentenceSimilarity.getSimilarity(sent1.split("\\s"), sent2.split("\\s")));
	}

	public static void preprocessCorpus (String corpusInput, String corpusOutput) throws FileNotFoundException, IOException
	{
		//SrCorpusProcessor.processSrCorpus(corpusInput, "srProcessorIzlaz.txt");
		//WikiCorpusReader.parseWikiCorpora("srProcessorIzlaz.txt", "enWikiReaderIzlaz.txt");

		File inputCorpusFile = new File ("enWikiReaderIzlaz.txt");
		File outputCorpusFile = new File ("CorpusCleanerIzlazStopWordsStdDev8.txt");
		if (outputCorpusFile.exists())
		{
			outputCorpusFile.delete();
			outputCorpusFile.createNewFile();
		}
		CorpusManagementES.cleanCorpus (inputCorpusFile, outputCorpusFile);


		inputCorpusFile = new File ("CorpusCleanerIzlazStopWordsStdDev8.txt");
		outputCorpusFile = new File (corpusOutput);
		if (outputCorpusFile.exists())
		{
			outputCorpusFile.delete();
			outputCorpusFile.createNewFile();
		}
		CorpusManagementES.stemCorpus (inputCorpusFile, outputCorpusFile);

	}
	
	

	public static void preprocessENGCorpus (String corpusInput, String corpusOutput, String stWords) throws FileNotFoundException, IOException
	{
		try{ 
			File inputCorpusFile = new File (corpusInput);//"enWikiReaderIzlaz.txt");
			File outputCorpusFile = new File (corpusOutput);//"CorpusCleanerIzlazStopWordsStdDev8.txt");
			if (outputCorpusFile.exists())
			{
				outputCorpusFile.delete();
				outputCorpusFile.createNewFile();
			}

			FileProcessingInfoES fpi=new FileProcessingInfoES(inputCorpusFile);
			FileWriterES fw=new FileWriterES(outputCorpusFile);
			FileReaderES fr=new FileReaderES(inputCorpusFile);

			// DODATO - izbacivanje stop reci
			ESPreprocessor.Singleton.INSTANCE.getSingleton().printStopWords();

			String line="";
			while((line=fr.readLine())!=null){
				line = ESPreprocessor.Singleton.INSTANCE.getSingleton().filter(line, false, true);
				if (line.split(" ").length < 2) continue; // izbacuju se linije u kojima postoji manje od 2 reci

				if(line.length()>0) 
				{
					fw.writeLine(" " + line);
					// koliko je procesirano
					fpi.printInfo(line);
				}

			}

			fw.close();
			System.out.println ("\nCorpus cleaning successful!\n");
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}

	public static void StemStopWordsList () throws FileNotFoundException, IOException
	{
		try{ 
			File inputCorpusFile = new File ("resource/eng/MySQL-english-stop-words.txt");
			File outputCorpusFile = new File ("resource/eng/english-stemmed-stop-words.txt");
			if (outputCorpusFile.exists())
			{
				outputCorpusFile.delete();
				outputCorpusFile.createNewFile();
			}

			FileProcessingInfoES fpi=new FileProcessingInfoES(inputCorpusFile);
			FileWriterES fw=new FileWriterES(outputCorpusFile);
			FileReaderES fr=new FileReaderES(inputCorpusFile);

			String line="";
			while((line=fr.readLine())!=null){
				line = ESPreprocessor.Singleton.INSTANCE.getSingleton().filter(line, false, false);

				if(line.length()>0) 
				{
					fw.writeLine(line);
					// koliko je procesirano
					fpi.printInfo(line);
				}

			}

			fw.close();
			System.out.println ("\nCorpus cleaning successful!\n");
			
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}

	public static void processCorpus (String corpusInput, String coalsOutput, String riOutput) throws FileNotFoundException, IOException
	{

		SemanticAnalysesObtainedAlgs analizator = new SemanticAnalysesObtainedAlgs (SAAlgType.COALS);
		File corpusFile = new File (corpusInput);
		BufferedReader document = new BufferedReader (new FileReader(corpusFile));

		/*analizator.processDocument(document);
         analizator.processSpace(System.getProperties());
         System.out.println("Fajl procesiran!\n");
         File coalsFile = new File(coalsOutput);
         if (coalsFile.exists())
         {
            coalsFile.delete();
            coalsFile.createNewFile();
         }

         SemanticSpaceIO.save(analizator.getSemanticSpace(), coalsFile, SemanticSpaceIO.SSpaceFormat.SPARSE_TEXT);
		*/
		 
		analizator = new SemanticAnalysesObtainedAlgs (SAAlgType.RI);     
		document = new BufferedReader (new FileReader(corpusFile));
		analizator.processDocument(document);
		analizator.processSpace(System.getProperties());
		System.out.println("Fajl procesiran!\n");
		File riFile = new File(riOutput);
		if (riFile.exists())
		{
			riFile.delete();
			riFile.createNewFile();
		}
		SemanticSpaceIO.save(analizator.getSemanticSpace(), riFile, SemanticSpaceIO.SSpaceFormat.SPARSE_TEXT);
		
	}

	public static void saveSemanticSpace (String coalsInput, String riInput) throws FileNotFoundException, IOException
	{

		File coalsFile = new File(coalsInput);
		SemanticSpaceDBCreator dbCreator = new SemanticSpaceDBCreator (coalsFile, 0);
		dbCreator.fillDB(1);        

		File riFile = new File(riInput);
		dbCreator = new SemanticSpaceDBCreator (riFile, 1);
		dbCreator.fillDB(1);

	}



	public static void gradeFile (String inputFileString, String outputFileString, HashMap<String, Double> tfHashMap, boolean isOriginal, boolean isMax, boolean removeStopWords) throws IOException, InterruptedException
	{
		File inputFajl = new File (inputFileString);
		File resultsFajl = new File (outputFileString);
		if (resultsFajl.exists())
		{
			resultsFajl.delete();
			resultsFajl.createNewFile();
		}
		BufferedReader br = new BufferedReader (new InputStreamReader (new FileInputStream (inputFajl)));
		FileWriterES fw = new FileWriterES (resultsFajl);

		// Sekvencijalan rad

		/*
        SentenceSimilarityEng sSim = new SentenceSimilarityEng(tfHashMap,false,true); 

        String line = null;
        int lineCnt = 1;
        while ((line = br.readLine()) != null)
        {
            String [] niz = line.split("\t");
            String sent1 = niz[1];
            String sent2 = niz[2];
            double dSim = sSim.getSimilarity(sent1, sent2);
            String newLine = niz[0] + "\t" + dSim + "\t" + sent1 + "\t" + sent2;
            fw.writeLine(newLine);
            System.out.println ("Line: " + lineCnt++);
            System.out.println (newLine);
        }
		 
        */
        
		String [] stringVector;
		if (inputFileString.contains("Train"))
			stringVector = new String [4076];
		else
			stringVector = new String [1725];
		int maxThreads = 5;//, currentThreads = 0;
		
		ExecutorService executor = Executors.newFixedThreadPool(maxThreads);
		
		String line = null;
		int lineCnt = 1;
		int [] currentThreads = new int [1];
		currentThreads[0] = 0;
		while ((line = br.readLine()) != null)
		{
			String [] niz = line.split("\t");
			String sent1 = niz[1];
			String sent2 = niz[2];

			while (true)
			{
				if (currentThreads[0] < maxThreads)
				{
					synchronized(currentThreads)
					{
						currentThreads[0]++;
					}
					SentenceSimilarityEng sThread = new SentenceSimilarityEng(niz[0], sent1, sent2, lineCnt, stringVector, currentThreads, tfHashMap, 
							isOriginal, isMax, removeStopWords);
					executor.execute(sThread);
					//sThread.start();
					break;
				}
				else
				{
					synchronized (stringVector)
					{
						stringVector.wait();
					}
				}

			}
			lineCnt++;
		}

		while (currentThreads[0] > 0)
			synchronized (stringVector)
			{
				stringVector.wait();
			}

		executor.shutdown();
		
		for (int i=0; i < stringVector.length; i++)
			fw.writeLine(stringVector[i]);
/*
		String [] stringVector;
		if (inputFileString.contains("Train"))
			stringVector = new String [835];
		else
			stringVector = new String [359];
		int maxThreads = 5;//, currentThreads = 0;
		String line = null;
		int lineCnt = 1;
		int [] currentThreads = new int [1];
		currentThreads[0] = 0;
		while ((line = br.readLine()) != null)
		{
			String [] niz = line.split("\t");
			String sent1 = niz[1];
			String sent2 = niz[2];

			while (true)
			{
				if (currentThreads[0] < maxThreads)
				{
					synchronized(currentThreads)
					{
						currentThreads[0]++;
					}
					SentenceSimilaritySerbian sThread = new SentenceSimilaritySerbian (niz[0], sent1, sent2, lineCnt, stringVector, currentThreads);
					sThread.start();
					break;
				}
				else
				{
					synchronized (stringVector)
					{
						stringVector.wait();
					}
				}

			}
			lineCnt++;
		}

		while (currentThreads[0] > 0)
			synchronized (stringVector)
			{
				stringVector.wait();
			}

		for (int i=0; i < stringVector.length; i++)
			fw.writeLine(stringVector[i]);
*/

		fw.close();
		br.close();
	}



	public static void gradeFileRAM (String inputFileString, String outputFileString, String semanticSpaceFile, String termFrequenciesFile) throws IOException, InterruptedException
	{
		// Ucitavanja fajlova se rade radi izbegavanja pristupa bazi podataka
		// Ucitavanje semantickog prostora
		SemanticSpace sspace = SemanticSpaceIO.load(semanticSpaceFile);

		HashMap<String, Double> termFrequencies = tfFileToHashMap(termFrequenciesFile);

		File inputFajl = new File (inputFileString);
		File resultsFajl = new File (outputFileString);
		if (resultsFajl.exists())
		{
			resultsFajl.delete();
			resultsFajl.createNewFile();
		}
		BufferedReader br = new BufferedReader (new InputStreamReader (new FileInputStream (inputFajl)));
		FileWriterES fw = new FileWriterES (resultsFajl);

		String [] stringVector;
		if (inputFileString.contains("Train"))
			stringVector = new String [835];
		else
			stringVector = new String [359];
		int maxThreads = 5;
		String line = null;
		int lineCnt = 1;
		int [] currentThreads = new int [1];
		currentThreads[0] = 0;
		while ((line = br.readLine()) != null)
		{
			String [] niz = line.split("\t");
			String sent1 = niz[1];
			String sent2 = niz[2];

			while (true)
			{
				if (currentThreads[0] < maxThreads)
				{
					synchronized(currentThreads)
					{
						currentThreads[0]++;
					}
					SentenceSimilaritySerbianRAM sThread = new SentenceSimilaritySerbianRAM (niz[0], sent1, sent2, lineCnt, stringVector, currentThreads, sspace, termFrequencies);
					sThread.start();
					break;
				}
				else
				{
					synchronized (stringVector)
					{
						stringVector.wait();
					}
				}

			}
			lineCnt++;
		}

		while (currentThreads[0] > 0)
			synchronized (stringVector)
			{
				stringVector.wait();
			}

		for (int i=0; i < stringVector.length; i++)
			fw.writeLine(stringVector[i]);


		fw.close();
		br.close();
	}

	public static void extractResults (String inputFileString, String outputFileString) throws IOException
	{
		File inputFile = new File (inputFileString);
		File totalResultsFile = new File (outputFileString.substring(0, outputFileString.length()-4) + "Total" + outputFileString.substring(outputFileString.length()-4));
		File noneqResultsFile = new File (outputFileString.substring(0, outputFileString.length()-4) + "NonEq" + outputFileString.substring(outputFileString.length()-4));
		File eqResultsFile = new File (outputFileString.substring(0, outputFileString.length()-4) + "Eq" + outputFileString.substring(outputFileString.length()-4));
		if (totalResultsFile.exists())
		{
			totalResultsFile.delete();
			totalResultsFile.createNewFile();
		}
		if (noneqResultsFile.exists())
		{
			noneqResultsFile.delete();
			noneqResultsFile.createNewFile();
		}
		if (eqResultsFile.exists())
		{
			eqResultsFile.delete();
			eqResultsFile.createNewFile();
		}

		BufferedReader br = new BufferedReader (new InputStreamReader (new FileInputStream (inputFile)));
		FileWriterES fwtotal = new FileWriterES (totalResultsFile);
		FileWriterES fweq = new FileWriterES (eqResultsFile);
		FileWriterES fwnoneq = new FileWriterES (noneqResultsFile);

		String line = null;
		while ((line = br.readLine()) != null)
		{
			String [] strings = line.split("\t");
			if (strings[0].contains("0"))
				fwnoneq.writeLine(strings[1]);
			else if (strings[0].contains("1"))
				fweq.writeLine(strings[1]);
			fwtotal.writeLine(strings[1]);
		}

		br.close();
		fwtotal.close();
		fweq.close();
		fwnoneq.close();
	}

	public static void printNumberOfEqualsAndNonEquals (String paraphraseCorpus) throws FileNotFoundException
	{
		File input = new File (paraphraseCorpus);
		FileReaderES fr = new FileReaderES (input);
		String line = null;
		int cnt1=0, cnt0=0;
		while ((line = fr.readLine()) != null)
		{
			if (line.charAt(0) == '0')
				cnt0++;
			else if (line.charAt(0) == '1' && line.charAt(1) != '0')
				cnt1++;
		}
		System.out.println("Broj istih: " + cnt1);
		System.out.println("Broj razlicitih " + cnt0);
	}
}
