/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.ucl.sspace.rs.etf.semantics;

import java.util.ArrayList;

/**
 *
 * @author Davor, Bojan Furlan
 */
/**
 * @author Furlan
 *
 */
/**
 * @author Furlan
 *
 */
/**
 * @author Furlan
 *
 */
/**
 * @author Furlan
 *
 */
/**
 * @author Furlan
 *
 */
/**
 * @author Furlan
 *
 */
public class MatrixES {


	/* multiplay matrix with factor                                           */
	public static double [][] scalarMultiplication(double[][] matrix,double factor,int m,int n){
		for(int i=0;i<m;i++ )
			for(int j=0;j<n;j++) matrix[i][j]*=factor;
		return matrix;
	}

	public static double [][] adition(double[][] matrix1,double[][]matrix2,int m,int n){
		double[][] rezMatrix=new double[m][n];
		for(int i=0;i<m;i++)
			for(int j=0;j<n;j++) rezMatrix[i][j]=matrix1[i][j]+matrix2[i][j];
		return rezMatrix;
	}

	public static double[][] findMaxElemAndRemove(double[][] matrix,int m,int n,ArrayList<Double> ro){
		if(m==0 || n==0) return null;

		double[][] rezMatrix=new double[m-1][n-1];
		int skipI=0;
		int skipJ=0;
		double max=0.0;

		for(int i=0;i<m;i++)
			for(int j=0;j<n;j++)
				if(matrix[i][j]>max){
					skipI=i;
					skipJ=j;
					max=matrix[i][j];
				}

		// construct rezMatrix
		if(max==0.0) return null;
		int ii=0;
		int jj=0;

		for(int i=0;i<m;i++){
			for(int j=0;j<n;j++){
				if(skipI!=i && skipJ!=j) rezMatrix[ii][jj++]=matrix[i][j];
			}
			if(skipI!=i) ii++;
			// and reser jj to zero
			jj=0;
		}

		// add to ro list
		ro.add(max);
		return rezMatrix;
	}

	public static double findMaxElemRowsSum(double[][] matrix,int m,int n){
		if(m==0 || n==0) return 0;

		//ArrayList<Double> ro = new ArrayList<Double>();

		double res=0.0;
		

		for(int i=0;i<m;i++){
			double max=0.0;
			for(int j=0;j<n;j++)
				if(matrix[i][j]>max){
					max=matrix[i][j];
				}
			// add to ro list
						//ro.add(max);
						res+=max;
		}

		return res;
	}
	
 
	/**
	 * 
	 * @param matrixS - sim matrix
	 * @param matrixW - weight matrix
	 * @param m
	 * @param n
	 * @return
	 */
	public static double findMaxElemRowsSumAndMultiplyWithWeight(double[][] matrixS,double[][] matrixW, int m,int n){
		if(m==0 || n==0) return 0;

		double res=0.0;
		

		for(int i=0;i<m;i++){
			double max=0.0;
			double maxW=0.0;
			for(int j=0;j<n;j++)
				if(matrixS[i][j]>max){
					max=matrixS[i][j];
					maxW=matrixW[i][j];
				}
			// add to ro list
						//ro.add(max);
						res+=max*maxW;
		}

		return res;
	}

	public static double findMaxElemColumnsSum(double[][] matrix,int m,int n){
		if(m==0 || n==0) return 0;

		//ArrayList<Double> ro = new ArrayList<Double>();
		double res=0.0;
		

		for(int i=0;i<n;i++){
			double max=0.0;
			for(int j=0;j<m;j++)
				if(matrix[j][i]>max){
					max=matrix[j][i];
				}
			// add to ro list
			//ro.add(max);
			res+=max;
		}

		return res;
	}
	public static void main(String[] args){
		int m=4;
		int n=6;
		double[][] matrix=new double[m][n];
		// and fill it
		matrix[0][0]=0.505;
		matrix[0][1]=0.010;
		matrix[0][2]=0.195;
		matrix[0][3]=0.162;
		matrix[0][4]=0.297;
		matrix[0][5]=0.449;

		matrix[1][0]=0.018;
		matrix[1][1]=0.248;
		matrix[1][2]=0.204;
		matrix[1][3]=0.083;
		matrix[1][4]=0.017;
		matrix[1][5]=0.011;

		matrix[2][0]=0.242;
		matrix[2][1]=0;
		matrix[2][2]=0.039;
		matrix[2][3]=0.071;
		matrix[2][4]=0.032;
		matrix[2][5]=0.044;

		matrix[3][0]=0.416;
		matrix[3][1]=0.041;
		matrix[3][2]=0.134;
		matrix[3][3]=0.133;
		matrix[3][4]=0.225;
		matrix[3][5]=0.124;
		
		double[][] matrixW=new double[m][n];
		
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				matrixW[i][j] =  Math.pow(2, (i*j)*1.0/(m*n)- 1);
			}
		}

		//        boolean isOver=false;
		//        int i=0;
		//        while(!isOver){
		//            //matrix=MatrixES.findMaxElemAndRemove(matrix, m-i, n-i);
		//            if(matrix==null) break;
		//
		//            // stampaj matricu
		//            System.out.println("--------------------------------");
		//            i++;
		//            for(int j=0;j<m-i;j++){
		//                for(int k=0;k<n-i;k++) System.out.print(matrix[j][k]+"  ");
		//                System.out.println();
		//
		//            }
		//        }

		
		// stampaj matricu
		System.out.println("--------------------------------");
		for(int j=0;j<m;j++){
			for(int k=0;k<n;k++) {
				System.out.print(matrix[j][k]+"  ");
			}
			System.out.println();

		}
		
		// stampaj matricu
		System.out.println("--------------------------------");
				for(int j=0;j<m;j++){
					for(int k=0;k<n;k++) {
						System.out.print(matrixW[j][k]+"  ");
					}
					System.out.println();
				}
//		System.out.println("ColumnMAX: "+MatrixES.findMaxElemColumnsSum(matrix, m, n));
//		System.out.println("RowMAX: "+MatrixES.findMaxElemRowsSum(matrix, m, n));
				
		System.out.println("RowMAX&MultiplyWithWeight: "+MatrixES.findMaxElemRowsSumAndMultiplyWithWeight(matrix, matrixW, m, n)); //0,505*0,5+ 0,248*0,514651118321746 + 0,242*0,5 + 0,416*0,5 = 0,709133477343793008

	}

}




