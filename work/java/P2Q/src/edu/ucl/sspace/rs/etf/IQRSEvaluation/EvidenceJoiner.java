package edu.ucl.sspace.rs.etf.IQRSEvaluation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 7/7/13
 * Time: 8:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class EvidenceJoiner {

    public static HashMap<String, Double> joinEvidences(List<HashMap<String, Double>> hashMaps) {
        HashMap<String, Double> joined = new HashMap<String, Double>();

        for (int i = 0; i<hashMaps.size(); i++) {
            for (String keyword : hashMaps.get(i).keySet()) {
                if (joined.containsKey(keyword)) {
                    joined.put(keyword, probTCoNorm(hashMaps.get(i).get(keyword), joined.get(keyword)));
                } else {
                    joined.put(keyword, hashMaps.get(i).get(keyword));
                }
            }
        }

        return joined;
    }

    private static double probTCoNorm(double a, double b){
        return a+b - a*b;
    }


    public static void main(String[] args) {
        HashMap<String, Double> map1 = new HashMap<String, Double>();
        map1.put("catten", 0.4);
        map1.put("cati", 0.7);
        map1.put("cats", 0.9);

        HashMap<String, Double> map2 = new HashMap<String, Double>();
        map2.put("catten1", 0.4);
        map2.put("cati", 0.7);
        map2.put("cats3", 0.9);

        HashMap<String, Double> map3 = new HashMap<String, Double>();
        map3.put("catte3n", 0.4);
        map3.put("cat3i", 0.7);
        map3.put("cats", 0.9);

        List<HashMap<String, Double>> hashMaps = new ArrayList<HashMap<String, Double>>();
        hashMaps.add(map1);
        hashMaps.add(map2);
        hashMaps.add(map3);

        System.out.println(EvidenceJoiner.joinEvidences(hashMaps));
    }
}
