/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ucl.sspace.rs.etf.paraphrase;

/**
 *
 * @author Vuk Batanovic
 */
public class Paraphrase {
    
    private String sentence1;
    private String sentence2;
    private ParaphraseStatistics statistics;
    private double grade;
    private double sentenceLengthDiff, sameWordsPercentage;
    
    public Paraphrase (String str1, String str2, ParaphraseStatistics ps)
    {
        sentence1 = str1;
        sentence2 = str2;
        statistics = ps;
        
        // Favorizuju se recenice slicne duzine koje imaju oko 50% istih reci
        // Kada su recenice dosta razlicitih duzina, smanjuje se verovatnoca da su zaista u pitanju parafraze
        // Kada je procenat istih reci nizak, verovatnije je u pitanju drugi deo vesti nego prava parafraza
        // Kada je procenat istih reci visok, verovatno je u pitanju ista recenica prosirena nekom semanticki nebitnom informacijom (npr. ko je dao izjavu)
        // Dodatna tezina se daje parovima recenica koje su kratke jer kod kracih recenica osrednja ocena ima vecu tezinu nego kod dugackih
        // Kriterijumi za ponderaciju su dobijeni eksperimentalnim putem kao najbolji moguci
        
        sentenceLengthDiff = (double) (statistics.getShorterSentenceWordCount())/statistics.getLongerSentenceWordCount();
        sentenceLengthDiff *= 5;
        sameWordsPercentage = (double) (statistics.getSameWordCount())/statistics.getShorterSentenceWordCount();
        if (sameWordsPercentage > 0.5) sameWordsPercentage = 1 - sameWordsPercentage; // optimalno je ako je oko 50% reci isto
        sameWordsPercentage *= 5.5;
        if (statistics.getShorterSentenceWordCount() <= 6)
        {
            sentenceLengthDiff *= 1.7;
            sameWordsPercentage *= 1.4;
        }
        else if (statistics.getShorterSentenceWordCount() <= 8)
            sameWordsPercentage *= 1.2;
        else if (statistics.getShorterSentenceWordCount() <= 10)
            sameWordsPercentage *= 1.1;
        

        grade = sentenceLengthDiff + sameWordsPercentage;
    } 
    
    public String getSentence1 () { return sentence1;}
    public String getSentence2 () { return sentence2;}
    public ParaphraseStatistics getStatistics () { return statistics;}
    
    public double gradeParaphrase () { return grade;}
    
    public String toString ()
    {       
        //return grade + "\t" + sentenceLengthDiff + "\t" + sameWordsPercentage + "\t" + sentence1 + ".\t" + sentence2 + ".";
        return sentence1 + ".\t" + sentence2 + ".";
    }
}
