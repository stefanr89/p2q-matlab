/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.ucl.sspace.rs.etf.corpus;

import java.io.File;

/**
 *
 * @author Davor
 */
public class FileProcessingInfoES {

    private int numOfB=0;
    private int numOfKB=0;
    private int numOfMB=0;

    private long totalSize=0;

    public FileProcessingInfoES(File file){
        if(file!=null) totalSize=file.length();
        // in MB
        totalSize=totalSize/(1024*1024);
    }

    public void printInfo(String line){
        numOfB+=line.length();
        if(numOfB>=1024){
            numOfB=0;
            switch(numOfKB){
                case 100: System.out.print("[100] KB ");
                          break;
                case 200: System.out.print("[200] KB ");
                          break;
                case 300: System.out.print("[300] KB ");
                          break;
                case 400: System.out.print("[400] KB ");
                          break;
                case 500: System.out.print("[500] KB ");
                          break;
                case 600: System.out.print("[600] KB ");
                          break;
                case 700: System.out.print("[700] KB ");
                          break;
                case 800: System.out.print("[800] KB ");
                          break;
                case 900: System.out.print("[900] KB ");
                          break;
            }
            numOfKB++;
            if(numOfKB>=1024){
                numOfKB=0;
                numOfMB++;
                System.out.println();
                System.out.println("-----------------------------------------------------------");
                System.out.println("Processing in MB: Output file size at "+numOfMB+" MB. Input file size is approximately "+ totalSize+" MB");
            }
        }

    }

    public int getNumOfMB(){
        return numOfMB;
    }
    
    void printInfo (char ch)
    {
        printInfo (Character.toString(ch));
    }

}
