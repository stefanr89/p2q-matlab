/*
executor.shutdown();executor.shutdown(); * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ucl.sspace.rs.etf.database;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 *
 * @author Davor, Bojan Furlan
 */
public class ESConnection {

//    private static Connection connection = null;
//    private static String username = "root";
//    private static String password = "12345";
//    private static String driverClassName = "com.mysql.jdbc.Driver";
//    private static String dburl = "jdbc:mysql://localhost:3306/semantickaslicnost_stddev8";
//
//    public static Connection getConnection()
//            throws SQLException {
//
//        try {
//            if (connection == null) {
//                Class.forName(driverClassName);
//                connection = DriverManager.getConnection(dburl, username, password);
//            }
//        } catch (ClassNotFoundException ex) {
//            throw new SQLException(ex.getLocalizedMessage() + "ge");
//            //pitanje za studente: cemu sluzi "ge"?
//        }
//        return connection;
//    }
    
	
	public static Connection getConnection()
			throws SQLException {

		return getConnection("SemanticSimilarity");
	}
	
	public static Connection getConnection(String db) {

		Connection con = null;
//		String username = "pera";
//		String password = "zdera";
        //String dburl = "jdbc:mysql://localhost:3306/"+db+"?user=slavkoz&password=xs";
		//String driverClassName = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
		//String dburl = "jdbc:sqlserver://kraus\\SQLEXPRESS:1433;databaseName="+db+";integratedSecurity=true"; 
//		String dburl = "jdbc:sqlserver://localhost:1433;databaseName="+db+";integratedSecurity=true";
	
		try {
			
		Properties prop = new Properties();
		prop.load(new FileInputStream("config.properties"));
		String dburl = prop.getProperty("databaseString").replace("SOME_DB", db); //config.properties -> databaseString=jdbc:sqlserver://localhost:1433;databaseName=SOME_DB;integratedSecurity=true
		

		
			//Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			con=DriverManager.getConnection(dburl);//, username, password);
		} catch (Exception ex) {
			ex.printStackTrace();
			//throw new SQLException(ex.getLocalizedMessage()+"ge");
			//pitanje za studente: cemu sluzi "ge"?
		}
		return con;
	}
}
