package edu.ucl.sspace.rs.etf.semantics;


import edu.ucl.sspace.rs.etf.corpus.ESPreprocessor;
import edu.ucl.sspace.rs.etf.corpus.SrCorpusProcessor;
import edu.ucl.sspace.rs.etf.database.SemanticSpaceDBReader;
import edu.ucla.sspace.common.Similarity;
import edu.ucla.sspace.common.Similarity.SimType;
import edu.ucla.sspace.text.DocumentPreprocessor;
import edu.ucla.sspace.vector.Vector;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author Bojan Furlan
 */
public class QuestionToProfileSimilarity implements Runnable {

	/*	public double getSimilarity(String sent1,String sent2) throws IOException // static double
	{


		//sent1 = SrCorpusProcessor.processSrString(sent1);
		DocumentPreprocessor dp=new DocumentPreprocessor();
		sent1 = dp.process(sent1);
		sent1 = ESPreprocessor.Singleton.INSTANCE.getSingleton().filter(sent1, false, true);
		//sent1 = ESPreprocessor.stemSerbian(sent1);

		//sent2 = SrCorpusProcessor.processSrString(sent2);
		dp=new DocumentPreprocessor();
		sent2 = dp.process(sent2);
		sent2 = ESPreprocessor.Singleton.INSTANCE.getSingleton().filter(sent2, false, true);
		//sent2 = ESPreprocessor.Singleton.INSTANCE.getSingleton().stemSerbian(sent2);

		//return result = getSimilarity(sent1.split(" "), sent2.split(" "));
		//result = SentenceSimilarity.getSimilarity(sent1.split(" "), sent2.split(" "));
	}
	 */
	//private String initialGrade;
	private HashMap<String, Double> question;
	private ConcurrentHashMap<String,HashMap<String, Double>> users;

	//private HashMap<String, Double> originalP, originalQ;
	private ConcurrentHashMap<String, Double> resMap;
	private int [] currentThreads;
	boolean isOriginal = false;
	boolean isMax = true;
	SemanticSpaceDBReader ssDBr=SemanticSpaceDBReader.Singleton.INSTANCE.getSingleton();//new SemanticSpaceDBReader(14000,0);
	String user;

	public QuestionToProfileSimilarity(HashMap<String, Double> question, ConcurrentHashMap<String,HashMap<String, Double>> users, ConcurrentHashMap<String, Double> resuts, boolean isOriginal, boolean isMax)
	{
		this(isOriginal,isMax);
		this.question = question;
		this.resMap = resuts;
		this.isOriginal = isOriginal;
		this.users = users;
	}

	public QuestionToProfileSimilarity(boolean isOriginal, boolean isMax)
	{
		this.isOriginal = isOriginal;
		this.isMax = isMax;
	}


	/*
	 * Prvo ide pitanje pa onda korisnik!!!!
	 */
	public double getWeightedSimilarity(HashMap<String, Double> question,HashMap<String, Double> user) throws IOException{
		double dSim=0.0;

		if(question.size()==0||user.size()==0) return dSim;

		if(isMax)
			return maxSemSim(question, user);
		else		
			return weightedSemSim(question, user);
	}

	double weightedSemSim(HashMap<String, Double> sent1, HashMap<String, Double> sent2) {
		double dSim;
		// Step1: Here we assume that all stoped words in sent 1 and sent2
		//        are cleaned, so this step is skiped

		// Step2: we count the number of delta(num of exact similarity strings)
		//        in sentence. We remove all delta tokens from both P and R
		//        if all the terms match we go to step 6
		//        it is donig through StringSimilirity.getCommonWordOrderSimilarity
		//        and at this time we count S0,and P,R are formed too.

		StringSimilarity sSim=new StringSimilarity(sent1, sent2);

		// if there are only the same words
		if(sSim.R.size()==0||sSim.P.size()==0) return 1;

		//double s0=sSim.getCommonWordOrderSimilarity(s1,s2);

		//System.out.println("Common worder step2 passed");

		// step3: we construct m-sigma x n-sigma StringSimilarity matrix
		//        m is number of strings in the shorter sentence
		//        n is number of string  in the longer  sentence
		double[][] stringSimilarityMatrix=sSim.getStringSimilarityMatrix();

		//System.out.println("String Similarity Matrux has been created step3 passed");



		// step4:we construct m-sigma x n-sigma semantic similarity matrix
		//       wich alg. will be used?
		double [][] semanticSimilarityMatrix=new double[sSim.m-sSim.sigmaM][sSim.n-sSim.sigmaN];

		// ADDED construct m-sigma x n-sigma termFrequencyMatrix
		double [][] termFrequencyMatrix = new double[sSim.m-sSim.sigmaM][sSim.n-sSim.sigmaN];


		SemanticSpaceDBReader ssDBr= SemanticSpaceDBReader.Singleton.INSTANCE.getSingleton();	//new SemanticSpaceDBReader(14000,0);


		//System.out.println("In process is semanticSimilarity matrix construction");
		ArrayList<Vector> vectorList=new ArrayList<Vector>();

		for(int i=0;i<sSim.m-sSim.sigmaM;i++){
			Vector v2=ssDBr.getVector(sSim.R.get(i));
			double tfnorm2 = 1, tfnorm1 = 1;
			if (!isOriginal && v2 != null) 
				tfnorm2 = sSim.refR.get(sSim.R.get(i)).doubleValue(); //get weight
			//if (v2 != null) tfnorm2 = ssDBr.getTermFrequency(sSim.R.get(i));
			for(int j=0;j<sSim.n-sSim.sigmaN;j++){
				Vector v1=null;
				if(i==0){
					// only first time
					v1=ssDBr.getVector(sSim.P.get(j));
					vectorList.add(v1);
				}
				else 
					v1=vectorList.get(j);

				if(v1!=null && v2!=null) 
					semanticSimilarityMatrix[i][j]=Similarity.getSimilarity(SimType.COSINE, v1, v2);
				else 
					semanticSimilarityMatrix[i][j]=0.0;

				// if (v1 != null) tfnorm1 = ssDBr.getTermFrequency(sSim.P.get(j));
				if (!isOriginal && v1 != null) 
					tfnorm1 = sSim.refP.get(sSim.P.get(j)).doubleValue(); //get weight
				else 
					tfnorm1 = 1;

				termFrequencyMatrix[i][j] = tfnorm1*tfnorm2;
			}
		}


		// step5: we construct another joint matrix wich dimension are: m-sigma X n-sigma
		//        as: M=psi*M1+fi*M2
		//        where psi is string similarity weight, and fi is semantic similarity weight

		double psi=0.45;
		double fiCorpusBased=0.55;
		double [][] jointMatrix=null;
		double [][] psiM1=MatrixES.scalarMultiplication(stringSimilarityMatrix,psi,sSim.m-sSim.sigmaM, sSim.n-sSim.sigmaN);
		double [][] fiCorpusBasedM2=MatrixES.scalarMultiplication(semanticSimilarityMatrix,fiCorpusBased,sSim.m-sSim.sigmaM, sSim.n-sSim.sigmaN);

		jointMatrix=MatrixES.adition(psiM1,fiCorpusBasedM2,sSim.m-sSim.sigmaM, sSim.n-sSim.sigmaN);


		
		// DODATO ////////////////////////////////////////////////////////////

		if (!isOriginal) {
			for (int i = 0; i < sSim.m - sSim.sigmaM; i++) {
				for (int j = 0; j < sSim.n - sSim.sigmaN; j++) {
					termFrequencyMatrix[i][j] = Math.pow(2,
							termFrequencyMatrix[i][j] - 1);
					jointMatrix[i][j] *= termFrequencyMatrix[i][j];
				}
			}
			///////////////////////////////////////////////////////////////////////
		}




		//System.out.println("Joint Matrix has been created, wait for finish");

		double roSum = 0;

		// step6:construct ro list, which is consisted of maximal elements from joint matrix
		ArrayList<Double> ro=new ArrayList<Double>();
		boolean isOver=false;
		int i=0;
		while(!isOver){
			jointMatrix=MatrixES.findMaxElemAndRemove(jointMatrix,sSim.m-sSim.sigmaM-i, sSim.n-sSim.sigmaN-i, ro);
			if(jointMatrix==null) break;
			i++;
		}

		//			if (isOriginal) { //TODO proveriti ovo - da li je isto
		//				// STARI NACIN RACUNANJA
		//				// step7: final exec. eq. to eq. from paper
		//
		//				// define wf- weight for common word similarity between 0 and 0.5
		//				double wf=0.0;
		//				// make sum of all ro elemnts
		//				for(i=0;i<ro.size();i++) roSum+=ro.get(i);
		//				// make factor wich is important for common word similarity
		//				// sigma*(1-wf+wf*S0);
		//				double cowSimF=sSim.sigmaM; // -ukoliko se uzima u obzir common word order onda *(1-wf+wf*s0);
		//				// make numerator wich is: (roSum+cowSimF)x(m+n)
		//				double numerator=(roSum+cowSimF)*(sSim.m+sSim.n);
		//				// make denominator
		//				double denominator=2*sSim.m*sSim.n;
		//				// finaly
		//				dSim=numerator/denominator;
		//
		//
		//			}
		//			else{

		// NOV NACIN RACUNANJA
		// step7: final exec. eq. to eq. from paper

		// make sum of all ro elemnts
		for(i=0;i<ro.size();i++) roSum+=ro.get(i);


		double delta=sSim.sigmaM;
		double deltaValue = 0; 
		for (int i1=0; i1<delta; i1++)
		{
			String word = sSim.sameWords[i1];
			double tfnorm, tfnorm1, tfnorm2;
			// if (ssDBr.getVector(word) != null) tfnorm = ssDBr.getTermFrequency(word);

			if (isOriginal) {
				tfnorm = 1;
			} else {
				tfnorm1 = sSim.refR.get(word).doubleValue(); 
				tfnorm2 = sSim.refP.get(word).doubleValue(); 
				tfnorm = tfnorm1 * tfnorm2;
				tfnorm = Math.pow(2, tfnorm-1);
			}

			deltaValue += tfnorm;
		}
		// make numerator wich is: (roSum+deltaValue)x(m+n)
		double numerator=(roSum+deltaValue)*(sSim.m+sSim.n);
		// make denominator
		double denominator=2*sSim.m*sSim.n;
		// finaly
		dSim=numerator/denominator;

		return dSim;
	}

	/*
	 * Prvo ide pitanje pa onda korisnik!!!!
	 */
	double maxSemSim(HashMap<String, Double> question,HashMap<String, Double> user) {
		double dSim;
		// Step1: Here we assume that all stoped words in sent 1 and sent2
		//        are cleaned, so this step is skiped

		// Step2: we count the number of delta(num of exact similarity strings)
		//        in sentence. We remove all delta tokens from both P and R
		//        if all the terms match we go to step 6
		//        it is donig through StringSimilirity.getCommonWordOrderSimilarity
		//        and at this time we count S0,and P,R are formed too.

		StringSimilarityQuestionToProfile sSim=new StringSimilarityQuestionToProfile(question, user);

		// if there are only the same words
		if(sSim.R.size()==0||sSim.P.size()==0) return 1;

		//double s0=sSim.getCommonWordOrderSimilarity(s1,s2);

		//System.out.println("Common worder step2 passed");

		// step3: we construct m-sigma x n-sigma StringSimilarity matrix
		//        m is number of strings in the shorter sentence
		//        n is number of string  in the longer  sentence
		double[][] stringSimilarityMatrix=sSim.getStringSimilarityMatrix();

		//System.out.println("String Similarity Matrux has been created step3 passed");



		// step4:we construct m-sigma x n-sigma semantic similarity matrix
		//       wich alg. will be used?
		double [][] semanticSimilarityMatrix=new double[sSim.m][sSim.n];

		// ADDED construct m-sigma x n-sigma termFrequencyMatrix
		double [][] termFrequencyMatrix = new double[sSim.m][sSim.n];


		SemanticSpaceDBReader ssDBr= SemanticSpaceDBReader.Singleton.INSTANCE.getSingleton();	//new SemanticSpaceDBReader(14000,0);


		//System.out.println("In process is semanticSimilarity matrix construction");
		ArrayList<Vector> vectorList=new ArrayList<Vector>();

		for(int i=0;i<sSim.m;i++){
			Vector v2=ssDBr.getVector(sSim.P.get(i));
			double tfnorm2 = 1, tfnorm1 = 1;
			if (!isOriginal && v2 != null) 
				tfnorm2 = sSim.refP.get(sSim.P.get(i)).doubleValue(); //get weight
			//if (v2 != null) tfnorm2 = ssDBr.getTermFrequency(sSim.R.get(i));
			for(int j=0;j<sSim.n;j++){
				Vector v1=null;
				if(i==0){
					// only first time
					v1=ssDBr.getVector(sSim.R.get(j));
					vectorList.add(v1);
				}
				else 
					v1=vectorList.get(j);

				if(v1!=null && v2!=null) 
					semanticSimilarityMatrix[i][j]=Similarity.getSimilarity(SimType.COSINE, v1, v2);
				else 
					semanticSimilarityMatrix[i][j]=0.0;

				// if (v1 != null) tfnorm1 = ssDBr.getTermFrequency(sSim.P.get(j));
				if (!isOriginal && v1 != null) 
					tfnorm1 = sSim.refR.get(sSim.R.get(j)).doubleValue(); //get weight
				else 
					tfnorm1 = 1;

				termFrequencyMatrix[i][j] = tfnorm1*tfnorm2;
			}
		}


		// step5: we construct another joint matrix wich dimension are: m-sigma X n-sigma
		//        as: M=psi*M1+fi*M2
		//        where psi is string similarity weight, and fi is semantic similarity weight

		double psi=0.45;
		double fiCorpusBased=0.55;
		double [][] jointMatrix=null;
		double [][] psiM1=MatrixES.scalarMultiplication(stringSimilarityMatrix,psi,sSim.m, sSim.n);
		double [][] fiCorpusBasedM2=MatrixES.scalarMultiplication(semanticSimilarityMatrix,fiCorpusBased,sSim.m, sSim.n);

		jointMatrix=MatrixES.adition(psiM1,fiCorpusBasedM2,sSim.m, sSim.n);

		//TODO Mnoziti sa tezinama nakon odredjivanja najboljih parova za MAX SIM
		// DODATO ////////////////////////////////////////////////////////////

		if (!isOriginal) {
			for (int i = 0; i < sSim.m; i++) {
				for (int j = 0; j < sSim.n; j++) {
					termFrequencyMatrix[i][j] = Math.pow(2,
							termFrequencyMatrix[i][j] - 1);
					// Izbaciti jointMatrix[i][j] *= termFrequencyMatrix[i][j];
				}
			}
			///////////////////////////////////////////////////////////////////////
		}




		// step6:construct ro list, wich is consisted of maximal elemnts from joint matrix WITHOUT REMOVING ELEMENT
		//double roSum= MatrixES.findMaxElemRowsSum(jointMatrix,sSim.m, sSim.n) ;
		
		double roSum= MatrixES.findMaxElemRowsSumAndMultiplyWithWeight(jointMatrix, termFrequencyMatrix, sSim.m, sSim.n) ; // for Question only

//		// make numerator wich is: (roSum+deltaValue)x(m+n)
//		double numerator=roSum*(sSim.m+sSim.n);
//		// make denominator
//		double denominator=2*sSim.m*sSim.n;
//		// finaly
//		dSim=numerator/denominator;
//
//		return dSim;
		
		return roSum/sSim.m; // normalise with number of question concepts
	}

	@Override
	public void run() {
		try {

			for (Map.Entry<String,HashMap<String,Double>> user : users.entrySet() ) {


				double res = getWeightedSimilarity(user.getValue(), question);
				String newLine = res + "\t" + user.getValue() + "\t" + question;
				resMap.put(user.getKey(), res);
				System.out.println ("Line: " + resMap.size());
				System.out.println (newLine);

			}

			synchronized (resMap)
			{
				resMap.notifyAll();
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	public static void main(String[] args) throws Exception {
//		HashMap<String, Double> sent1 = new HashMap<String, Double>(){
//			{
//				put("computer",0.1);  //"computer science is looking for hight tech improvment";
//				put("science",0.5);
//				put("is",0.1); 
//				put("looking",0.5);
//				put("for",0.1); 
//				put("height",0.5);
//				put("tech",0.1); 
//				put("improvment",0.5);
//
//			}
//		};
//		HashMap<String, Double> sent2 = new HashMap<String, Double>(){
//			{
//				put("history",0.1); //history science try to prove what happen a long time ago"
//				put("science",0.5);
//				put("try",0.1); 
//				put("to",0.5);
//				put("prove",0.1); 
//				put("what",0.5);
//				put("happen",0.1); 
//				put("a",0.5);
//				put("long",0.1); 
//				put("time",0.5);
//				put("ago",0.1); 
//
//			}
//		};

		
		HashMap<String, Double> question = new HashMap<String, Double>(){
			{
				put("computer",0.7);  //"computer science is looking for hight tech improvment";
				put("tech",0.4);
				put("internet",0.4); 

			}
		};
		HashMap<String, Double> user1 = new HashMap<String, Double>(){
			{
				put("history",0.1); //history science try to prove what happen a long time ago"
				put("science",0.3);
				put("try",0.2); 
				put("informatics",0.5);
				put("prove",0.4); 
				put("what",0.5);
				put("happen",0.7); 
				put("a",0.8);
				put("long",0.9); 
				put("time",0.11);
				put("ago",0.12); 

			}
		};
		
		HashMap<String, Double> user2 = new HashMap<String, Double>(){
			{
				put("informatics",0.5);

			}
		};


		System.out.println("Some random weights to words in sentence");	
		System.out.println(question);
		System.out.println(user1);
		System.out.println(user2);

		question = ESPreprocessor.Singleton.INSTANCE.getSingleton().filterWeightedWords(question, false, true);     	  
		user1 = ESPreprocessor.Singleton.INSTANCE.getSingleton().filterWeightedWords(user1, false, true);  
		user2 = ESPreprocessor.Singleton.INSTANCE.getSingleton().filterWeightedWords(user2, false, true);    


		System.out.println("After preprocessing:");

		System.out.println("question :"+question);
		System.out.println("User1 :"+user1);
		System.out.println("User2 :"+user2);

		QuestionToProfileSimilarity simOrig = new QuestionToProfileSimilarity(true, false);
		QuestionToProfileSimilarity simWeighted = new QuestionToProfileSimilarity(false, false);
		QuestionToProfileSimilarity simMax = new QuestionToProfileSimilarity(false, true);


//		System.out.println("Similarity is: "+ SentenceSimilarityEng.getMaxWeightedSimilarity(sent1, sent2)); 
//		System.out.println("Original Similarity is: "+ simOrig.getWeightedSimilarity(question, user1));
//		System.out.println("Weighted Similarity is: "+ simWeighted.getWeightedSimilarity(question, user1));

		//dsim= 0,15813049098989948*0,5783440919526437+0,11863968040141759*0,543367431263029+0,12294397793256326*0,5743491774985174 = 0,22653154621112770420025877224611
		System.out.println("Max2 Similarity for user1  is: "+ simMax.getWeightedSimilarity(question, user1)); // nova  0.07551051540370923, stara normalizacija 0.05393608243122088 
	
		//dsim= 0,14341445103635017*0,637280313659631+0,5743491774985174*0,07461543814808679+0,5743491774985174*0,12294397793256326= 0,20486329447277775675931461222314
		System.out.println("Max2 Similarity for user2  is: "+ simMax.getWeightedSimilarity(question, user2)); // nova 0.06828776482425926, stara normalizacija 0.13657552964851852

	}
}




