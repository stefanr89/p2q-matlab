/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ucl.sspace.rs.etf.paraphrase;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;

/**
 *
 * @author Vuk Batanovic
 */
public class SentenceParser {
    
    public static String [] removeEmptyStrings (String [] recenice)
    {
        // Uklanjanje praznih stringova
        int nePrazneCnt = 0;
        for (String s: recenice)
            if (s.length() != 0) nePrazneCnt++;
        if (nePrazneCnt != recenice.length)
        {
            String [] nePrazneRecenice = new String [nePrazneCnt];
            int index = 0;
            for (String s: recenice)
                if (s.length() != 0) nePrazneRecenice[index++] = s;
            recenice = nePrazneRecenice;
        }
        return recenice;
    }
    
    public static String [] reattachBrokenSentences (String [] recenice)
    {
        // Uklanjanje pojave pogresno razdvojenih recenica kod licnih imena npr. Petar P. ili N. N. ili kod datuma npr 20. jul 2011.
        String [] sredjeneRecenice = new String [recenice.length];
        int brojPogresnoRazdvojenih = 0;
        sredjeneRecenice[0] = recenice[0];
        for (int i=1; i<recenice.length; i++)
        {
            if (recenice[i-1].length() == 0) continue;
            if (recenice[i-1].endsWith("..") && recenice[i].endsWith("<< .."))
            {
                brojPogresnoRazdvojenih+=2;
                sredjeneRecenice[i-brojPogresnoRazdvojenih+1] = recenice[i-1].substring(0, recenice[i-1].length()-2) + recenice[i+1];
                i++;
            }
            else if ((recenice[i-1].endsWith("prof")) || (recenice[i-1].endsWith("Prof")) ||
                     (recenice[i-1].endsWith("dr")) || (recenice[i-1].endsWith("Dr")) ||
                     (recenice[i-1].endsWith("npr")) || (recenice[i-1].endsWith("Npr")) ||
                     (recenice[i-1].endsWith("tzv")) || (recenice[i-1].endsWith("Tzv")))
            {
                brojPogresnoRazdvojenih++;
                sredjeneRecenice[i-brojPogresnoRazdvojenih] += (". " + recenice[i]);
            }
            else if ((Character.isUpperCase(recenice[i-1].codePointAt(recenice[i-1].length()-1))) &&
                    (recenice[i-1].length()==1 || !Character.isUpperCase(recenice[i-1].codePointAt(recenice[i-1].length()-2))))
            {
                brojPogresnoRazdvojenih++;
                sredjeneRecenice[i-brojPogresnoRazdvojenih] += (". " + recenice[i]);
            }
            else if (Character.isDigit(recenice[i-1].codePointAt(recenice[i-1].length()-1)))
            {
                int pomeraj = 1;
                while (true)
                {
                    // Ako se iduci unazad od detektovane cifre naidje na whitespace ili dodje do kraja stringa - rec je o datumu ili rednom broju - treba ponovo spojiti recenice
                    if ((recenice[i-1].length() == pomeraj) || (Character.isWhitespace(recenice[i-1].codePointAt(recenice[i-1].length()-1-pomeraj))))
                    {
                        brojPogresnoRazdvojenih++;
                        sredjeneRecenice[i-brojPogresnoRazdvojenih] += (". " + recenice[i]);
                        break;
                    }

                    // Ako se iduci unazad od detektovane cifre naidje na slovo, radi se o poslednjoj reci u recenici koja sadrzi neki broj npr. B92, te je recenica pravilno podeljena      
                    else if (Character.isLetter(recenice[i-1].codePointAt(recenice[i-1].length()-1-pomeraj)))
                    {
                        sredjeneRecenice[i-brojPogresnoRazdvojenih] = recenice[i];
                        break;
                    }  

                    // Ako se iduci unazad od detektovane cifre naidje na jos jednu cifru ili neki specijalan znak npr '/', treba nastaviti da se ide unazad
                    else
                        pomeraj++;
                }
            }
            else
                sredjeneRecenice[i-brojPogresnoRazdvojenih] = recenice[i];
        }
        if (brojPogresnoRazdvojenih > 0)
        {
            recenice = new String [recenice.length - brojPogresnoRazdvojenih];
            for (int i=0; i<recenice.length; i++)
                recenice[i] = sredjeneRecenice[i];
        }
        return recenice;
    }
    
    public static String removeSentenceDateSourcePlacePrefixes (String recenica, String [] reci)
    {
        boolean locationsLeft = true;
        SimpleDateFormat sdf1 = new SimpleDateFormat ("dd.MM.yyyy."),
                sdf2 = new SimpleDateFormat ("dd.MM.yyyy"),
                sdf3 = new SimpleDateFormat ("dd. MM. yyyy."),
                sdf4 = new SimpleDateFormat ("dd. MM. yyyy");
        ParsePosition pp = new ParsePosition (0);
        while (locationsLeft)
        {   
            pp.setIndex(0);
            if (reci.length < 8) return recenica;

            // Datum u obliku DD. MONTHTEXT YYYY.  i izvor vesti
            if (reci[0].endsWith(".") && reci[2].endsWith(".") && reci[3].startsWith("(") && reci[3].endsWith(")"))
            {
                recenica = removeFirstXWords (reci, 5);
                reci = recenica.split(" ");
            }

            // Datum format 1
            else if (sdf1.parse(recenica, pp) != null)
            {
                recenica = recenica.substring(pp.getIndex()).trim();
                reci = recenica.split(" ");
            }
            // Datum format 2
            else if (sdf2.parse(recenica, pp) != null)
            {
                recenica = recenica.substring(pp.getIndex()).trim();
                reci = recenica.split(" ");
            }
            // Datum format 3
            else if (sdf3.parse(recenica, pp) != null)
            {
                recenica = recenica.substring(pp.getIndex()).trim();
                reci = recenica.split(" ");
            }
            // Datum format 4
            else if (sdf4.parse(recenica, pp) != null)
            {
                recenica = recenica.substring(pp.getIndex()).trim();
                reci = recenica.split(" ");
            }


            // Izvor vesti
            else if (reci[0].equalsIgnoreCase("izvor:"))
            {
                recenica = removeFirstXWords (reci, 2);
                reci = recenica.split(" ");
            }
            // Izvor vesti
            else if (reci[0].equalsIgnoreCase("izvor") && reci[1].equals(":"))
            {
                recenica = removeFirstXWords (reci, 3);
                reci = recenica.split(" ");
            }      

            // Imena mesta
            else if ((reci[1].equals("-") || reci[1].equals("--") || reci[1].equals("–"))
               || ((reci[1].endsWith(",") || reci[1].endsWith("-") || reci[1].endsWith("–")) && isInCapitalLetters(reci[0]) && isInCapitalLetters(reci[1].substring(0, reci[1].length()-1))))
            {
                recenica = removeFirstXWords (reci, 2);
                reci = recenica.split(" ");
            }

            // Imena mesta
            else if ((reci[0].endsWith(",") || reci[0].endsWith("-") || reci[0].endsWith("–")) && isInCapitalLetters(reci[0].substring(0, reci[0].length()-1)))
            {
                recenica = removeFirstXWords (reci, 1);
                reci = recenica.split(" ");
            }

            // Lose formatirana imena mesta npr. Sarajevo -Predsednik...
            else if (reci[1].startsWith("-") || reci[1].startsWith("–"))
            {
                recenica = removeFirstXWords (reci, 1);
                recenica = recenica.substring(1);
                reci = recenica.split(" ");
            }

            // Imena mesta - 2 reci
            else if (reci[2].equals("-") || reci[2].equals("--") || reci[2].equals("–"))
            {
                recenica = removeFirstXWords (reci, 3);
                reci = recenica.split(" ");
            }
                    
            // Imena mesta - 3 reci
            else if (reci[3].equals("-") || reci[3].equals("--") || reci[3].equals("–"))
            {
                recenica = removeFirstXWords (reci, 4);
                reci = recenica.split(" ");
            }

            else
                locationsLeft = false;
        }

        // Ako postoji duzi headline, pa IMEMESTA - tekst_vesti
        // ili headline (izvor vesti) imeMesta --
        for (int j=1; j<reci.length-1; j++)
        {
            if ((reci[j+1].equals("-") || reci[j+1].equals("–") || reci[j+1].equals("--"))
                    && ((isInCapitalLetters(reci[j])) || (reci[j-1].endsWith(")"))))
            {
                recenica = removeFirstXWords (reci, j+2);
                reci = recenica.split(" ");
                break; // nece se ovakva situacija javiti vise od jednom u svakoj vesti
            }
        }
        
        return recenica;
    }
    
    public static boolean isInCapitalLetters (String str)
    {
        boolean isInCapital = true;
        for (int i=0; i<str.length(); i++)
        {
            if (Character.isLowerCase(str.codePointAt(i)))
            {
                isInCapital = false;
                break;
            } 
        }
        return isInCapital;
    }
    
    public static String removeFirstXWords (String [] reci, int x)
    {
        StringBuilder recenica = new StringBuilder ();
        for (int j=0; j<reci.length-x; j++)
            recenica.append(reci[j+x]).append(" ");
        
        return recenica.toString().trim();
    }
}
