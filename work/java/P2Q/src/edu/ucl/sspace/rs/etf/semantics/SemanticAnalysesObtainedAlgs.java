/*
 *  Wrapper for S-Package word-space algorithms
 */



package edu.ucl.sspace.rs.etf.semantics;

import edu.ucla.sspace.coals.Coals;
import edu.ucla.sspace.common.SemanticSpace;
import edu.ucla.sspace.isa.IncrementalSemanticAnalysis;
import edu.ucla.sspace.lsa.LatentSemanticAnalysis;
import edu.ucla.sspace.ri.RandomIndexing;
import edu.ucla.sspace.vector.Vector;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Properties;
import java.util.Set;


/**
 *
 * @author Davor Jovanovic, ES
 */
public class SemanticAnalysesObtainedAlgs {

    public enum SAAlgType{
        LSA,
        RANDOMINDEXING,
        ISA,
        COALS,
        RI

    }

    // String reprezentacija odabranog algoritma
    private String strChosenAlg="LatentSemanticAnalyses";
    // enum reprezentacija odabranog algoritma
    private SAAlgType eSaAlgType=SAAlgType.LSA;

     Properties prop=System.getProperties();

    // Odabrani algoritam
    private SemanticSpace ssAlg=null;


    public SemanticAnalysesObtainedAlgs(){
    }

    /* Postavljanje odgovarajuceg algoritma za sp       */
    public SemanticAnalysesObtainedAlgs(SAAlgType algType){
        eSaAlgType=algType;
        setChosenAlg(algType);
    }


    /* Kreiranje odgovarajuceg algoritma za ss           */
    private void setChosenAlg(SAAlgType algType){
       try{
        switch(algType){
        /* TEMP izbaceno    
        case LSA: strChosenAlg="LatentSemanticAnalyses";
                      prop.setProperty(LatentSemanticAnalysis.LSA_DIMENSIONS_PROPERTY, "180");
                      ssAlg=new LatentSemanticAnalysis(prop);
                      break;
                      */
            case RANDOMINDEXING:
                      strChosenAlg="RandomIndexing";
                      ssAlg=new RandomIndexing();
                      break;
            case ISA: strChosenAlg="IncrementalSemenaticAnalyses";
                      ssAlg=new IncrementalSemanticAnalysis();
                      break;
            case COALS:
                      strChosenAlg="Coals";
                      ssAlg=new Coals();
                      break;
            case RI:
                     strChosenAlg="Random Indexing";
                     ssAlg=new RandomIndexing();
                     break;
        }
      }catch(Exception ex){

      }

    }


    /* Koji tip algoritma je setovan           */
    public SAAlgType getSaAlgType() {
        return eSaAlgType;
    }

    /* Menjanje algoritma za ps               */
    public void setSaAlgType(SAAlgType saAlgType) {
        this.eSaAlgType = saAlgType;
        setChosenAlg(saAlgType);
        // informacija da je promenjen algoritam
        System.out.println("Promenjen je algoritam za procisranje: "+strChosenAlg);
    }

    /* Stampanje informacije: za sada samo o izabranom */
    /* algoritmu                                       */

    public String toString(){
        return strChosenAlg;
    }

    /* Obicni wrapper-i za proizvoljno izabrani alg.  */

    public String getSpaceName(){
        return ssAlg.getSpaceName();
    }

    public Vector getVector(String word){
        return ssAlg.getVector(word);
    }

    public int getVectorLength(){
        return ssAlg.getVectorLength();
    }

    public Set<String> getWords(){
        return ssAlg.getWords();
    }

    public void processDocument(BufferedReader document){
        try{
            ssAlg.processDocument(document);
        }catch(IOException exc){

        }
    }

    public void processSpace(Properties prop){
        ssAlg.processSpace(prop);
    }

    public SemanticSpace getSemanticSpace(){
        return ssAlg;
    }

    public void setSemanticSpace(SemanticSpace sp){
        ssAlg=sp;
    }

}
