/*
 * Fill empty DataBase with semantic space values
 * ESConnection is used for connection with DB. If you want to rename,either
 * to change password or username you need to modify that class
 */

package edu.ucl.sspace.rs.etf.database;

import edu.ucl.sspace.rs.etf.corpus.FileProcessingInfoES;
import edu.ucl.sspace.rs.etf.corpus.FileReaderES;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;

/**
 *
 * @author Davor Jovanovic, ES
 */
public class SemanticSpaceDBCreator {

    private File ssFile=null;
    private int semanticSpaceType=0;

    /**
     *
     * @param ssFile SemanticSpaceFile
     * @param semanticSpaceType 0-COALS, 1-RANDOMINDEXING, 2-LSA
     */
    public SemanticSpaceDBCreator(File ssFile,int semanticSpaceType){
       
            this.ssFile=ssFile;
            this.semanticSpaceType=semanticSpaceType;
   
    }

    /**
     * Fill empty Data base with root word and its vector
     * vector is represented as a sparse string
     * @param the option 0 mean that you don't want to see information
     * about how much has been processed. 1 mean that you want
     */

    public void fillDB(int option) throws FileNotFoundException, IOException{
        // for extra information: in MB, how much has been processed
        FileProcessingInfoES fp=new FileProcessingInfoES(ssFile);
        String line=null;
        // for file reading
        BufferedReader br=new BufferedReader(new FileReader(ssFile));
        // the first line is unimportant(the header of SSPACE fill

        try{
            Connection con=null;
            PreparedStatement ps=null;
            // get the connection
            con=ESConnection.getConnection();
            while((line=br.readLine())!=null){
                String[] termVectorPair = line.split("\\|");
                if(termVectorPair.length>1){
                    if(option==1) fp.printInfo(line);
                    
                    String sqlString=null;
                    if(semanticSpaceType==0) sqlString="INSERT INTO coals(keyword,vector) VALUES(?,?);";
                    else if(semanticSpaceType==1) sqlString="INSERT INTO randomindexing(keyword,vector) VALUES(?,?);";
                    else sqlString="INSERT INTO lsa(keyword,vector) VALUES(?,?);";

                    ps=con.prepareStatement(sqlString);
                    ps.setString(1,termVectorPair[0]);
                    ps.setString(2,termVectorPair[1]);
                    ps.executeUpdate();
                    ps.close();


                }
            }

        }catch(SQLException sqEx){
            sqEx.printStackTrace();
        }
    }

    public static void main(String[] args) throws FileNotFoundException, IOException{
        File file=new File("COALS");
        SemanticSpaceDBCreator spdb=new SemanticSpaceDBCreator(file,0);
        spdb.fillDB(0);

    }
}
