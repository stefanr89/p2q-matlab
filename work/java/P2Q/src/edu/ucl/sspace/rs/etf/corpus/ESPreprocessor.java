/*
 * exclude stop-words and non-english words(ex abcd156, <num>, ...)
 * 
 */

package edu.ucl.sspace.rs.etf.corpus;

import edu.ucl.sspace.rs.etf.database.ESConnection;
import edu.ucl.sspace.rs.etf.stemmer.SerbianStemmer;
import edu.ucla.sspace.text.DocumentPreprocessor;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.tartarus.snowball.ext.porterStemmer;


/**
 *
 * @author Davor Jovanovic, Bojan Furlan
 */
public class ESPreprocessor {

	public static enum Singleton {
		INSTANCE;

		private static final ESPreprocessor singleton = new ESPreprocessor();

		public ESPreprocessor getSingleton() {
			return singleton;
		}
	}
	
	public static double probTCoNorm(double a, double b){
		return a+b - a*b;
	}
	
	// porterStemmer initialize
	porterStemmer ps=new porterStemmer();
	public ESPreprocessor(){
		initializeListOfStopWords("resource/eng/english-stemmed-stop-words.txt");
	}
	
	
	public ArrayList<String> listOfStopWords=new ArrayList<String>();
	public boolean isInitStopList=false;
	public Language lang=Language.ENG;
	public DocumentPreprocessor dp=new DocumentPreprocessor();

	
	/**
	 * filter un English words and numbers and words which length is less than 2
	 * @param strValues array of words that will be cleaned
	 * @param bIncludeNumbers if it is true than don't clean numbers
	 * @param excludeStopWords if it is true than exclude all English stop words
	 * @return String[] which is cleaned
	 */
	public String filter(String strLine,boolean bIncludeNumbers,boolean excludeStopWords){


		String[] strValues=strLine.trim().split("\\s+");

		ArrayList<String> validWords = buildValidWordsList(strValues, bIncludeNumbers, excludeStopWords);

		StringBuilder result = new StringBuilder ();
		for (int i=0; i<validWords.size(); i++) result.append(validWords.get(i)).append(" ");

		return  result.toString().trim();

	}


	ArrayList<String> buildValidWordsList(String[] strValues, boolean bIncludeNumbers, boolean excludeStopWords) {
		ArrayList<String> validWords = new ArrayList<String>();
		// porterStemmer initialize


		for(int i=0;i<strValues.length;i++){

			strValues[i]= dp.process(strValues[i]);

			if (lang== Language.ENG) {

				porterStemmer ps=new porterStemmer();
				ps.setCurrent(strValues[i]);
				ps.stem();
				strValues[i] = ps.getCurrent();

			}
			// try to filter
			boolean isOkCleanWords=strValues[i].matches("[a-zA-Z]+");
			boolean isOkCleanNumber=strValues[i].matches("[1-9]+");
			if(!bIncludeNumbers) isOkCleanNumber=false;             // exclude numbers
			boolean isStopWord=false;
			if(excludeStopWords){
				for(int j=0;j<listOfStopWords.size();j++)
					if(strValues[i].equals(listOfStopWords.get(j))){
						isStopWord=true;
						break;
					}
			}
			if( (isOkCleanWords || isOkCleanNumber)&& !isStopWord ) 
				validWords.add(strValues[i]);
		}
		return validWords;
	}


	/**
	 * filter un English words and numbers and words which length is less than 2
	 * @param strValues array of words that will be cleaned
	 * @param bIncludeNumbers if it is true than don't clean numbers
	 * @param excludeStopWords if it is true than exclude all English stop words
	 * @return String[] which is cleaned
	 */
	public HashMap<String, Double> filterWeightedWords(HashMap<String, Double> strWithValues,boolean bIncludeNumbers,boolean excludeStopWords){
		HashMap<String, Double> validMap = new HashMap<String, Double>();

		

		for (Map.Entry<String, Double> entry : strWithValues.entrySet())
		{

			String word = entry.getKey().toLowerCase();
			String[] arr= word.split("\\s+");
			
			for (int i = 0; i < arr.length; i++) {
				String s= arr[i];
				// first do stemming(porter)
				ps.setCurrent(s);
				ps.stem();
				s = ps.getCurrent();
				// try to filter
				boolean isOkCleanWords = s.matches("[a-z]+");
				boolean isOkCleanNumber = ! s.matches(".*\\d.*");//s.matches("[1-9]+"); // exclude numbers
				if (bIncludeNumbers) {
					isOkCleanNumber = true;
				}
				boolean isStopWord = false;
				if (excludeStopWords) {
					for (int j = 0; j < listOfStopWords.size(); j++)
						if (s.equals(listOfStopWords.get(j))) {
							isStopWord = true;
							break;
						}
				}
				if ((isOkCleanWords || isOkCleanNumber) && !isStopWord){
					if (validMap.containsKey(s)) {
						
						double val = probTCoNorm(entry.getValue(), validMap.get(s).doubleValue());
						validMap.put(s, val);
					}else {
						validMap.put(s, entry.getValue());
					}
				}
					
					
			}
		}

		return  validMap;
	}
	public String stemSerbian(String strLine)
	{
		SerbianStemmer serbStem = new SerbianStemmer ();
		return serbStem.stem(strLine);


	}

	public String stemEng(String s)
	{
		porterStemmer ps=new porterStemmer();
		ps.setCurrent(s);
		ps.stem();
		return ps.getCurrent();


	}

	public String documentPreprocess (String str)
	{
		DocumentPreprocessor dp=new DocumentPreprocessor();
		String cleaned = dp.process(str);
		return cleaned;
	}

	/**
	 * initialize list of English stop words from file added through filename  
	 * @param filename
	 */

	public void initializeListOfStopWords(String filename){
		File file=new File(filename);
		try{
			FileReaderES fr=new FileReaderES(file);
			String line=null;
			while((line=fr.readLine())!=null){
				if(lang == Language.ENG)// STARO ZA ENGLESKI
				{
					String words[]=line.trim().split("\\s+");
					for(int i=0;i<words.length;i++) listOfStopWords.add(words[i]);
				}
				else {

					listOfStopWords.add(line.trim());
				}

			}

		}catch(FileNotFoundException fnfex){
			fnfex.printStackTrace();
		}

	}

	/**
	 *  Only for test purpose. Print all English stop words after init.
	 */
	public void printStopWords(){
		for(int i=0;i<listOfStopWords.size();i++){
			System.out.println(listOfStopWords.get(i));
		}
	}
	
	public void CleanTFIDF() {
		try{
			Connection con=null;
			PreparedStatement ps=null;
			ResultSet rs=null;

			con=ESConnection.getConnection();
			String sqlString="SELECT word, freq FROM Freq WHERE informationSourceId='TFIDF';";
			ps=con.prepareStatement(sqlString);
			//ps.setString(1, word);
			rs=ps.executeQuery();

			while(rs.next()){
				String word=rs.getString("word");
				word = filter(word, false, true);

				if (!word.equals("")) {


					String val = rs.getString("freq");
					double freq = Double.parseDouble(val);


					sqlString = "SELECT word, freq FROM FreqCleaned WHERE informationSourceId='TFIDF' and word=?;";
					PreparedStatement preparedStatementSel =con.prepareStatement(sqlString);
					preparedStatementSel.setString(1, word);
					ResultSet rs1=preparedStatementSel.executeQuery();
					if (rs1.next()) {
						double old = rs1.getDouble(2);
						String updateTableSQL = "UPDATE FreqCleaned SET freq = ? WHERE word = ?";
						PreparedStatement preparedStatementUpd = con.prepareStatement(updateTableSQL);
						preparedStatementUpd.setDouble(1, freq+old/2);
						preparedStatementUpd.setString(2, word);
						// execute insert SQL stetement
						preparedStatementUpd.executeUpdate();

					} else {
						String insertTableSQL = "INSERT into FreqCleaned" + "(word, freq, informationSourceId) VALUES" + "(?,?,?)"; 
						PreparedStatement preparedStatementIns =con.prepareStatement(insertTableSQL);
						preparedStatementIns.setString(1, word);
						preparedStatementIns.setDouble(2, freq);
						preparedStatementIns.setString(3, "TFIDF");
						preparedStatementIns.executeUpdate();

					}
				}

			}
			if(ps!=null) ps.close();
			if(rs!=null) rs.close();

		}catch(SQLException sqlEx){
			sqlEx.printStackTrace();
		}
	}

	public static void main(String[] args){
		//String strValues[]={"davor","is","one","of","the<","the","best","and","probably","st12","programmer"};
		//for(int i=0;i<strValues.length;i++) System.out.print(strValues[i]+" ");
		//		ESPreprocessor.initializeListOfStopWords("resource/eng/english-stop-words.txt");
		//		ESPreprocessor.printStopWords();
		
		ESPreprocessor.Singleton.INSTANCE.getSingleton().filterWeightedWords(
				new HashMap<String, Double>(){
					{
						put("computer",0.7);  //"computer science is looking for hight tech improvment";
						put("tech",0.4);
						put("internet",0.4); 

					}
				}
				, false, true);     	

	}

}
