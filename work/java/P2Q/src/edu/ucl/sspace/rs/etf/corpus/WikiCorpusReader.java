/*
 * Read corpus which is in .xml format, parse it and make clean .txt file
 *
 */
package edu.ucl.sspace.rs.etf.corpus;

import edu.ucla.sspace.text.DocumentPreprocessor;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Davor Jovanovic, ES
 */
public class WikiCorpusReader {

    /**
     * getting text of interest from .xml file
     * @param XhtmlLine
     * @param xhtmlMarkUpHead
     * @param xhtmlMarkUpEnd
     * @return
     */

    private static String getTextOfInterest(String XhtmlLine,String xhtmlMarkUpHead, String xhtmlMarkUpEnd){
        String retString=null;

        if(XhtmlLine==null) return null;
        /* Try to find <abstract> and </abstract>        */
        
        int fromIndex=XhtmlLine.indexOf(xhtmlMarkUpHead);
        int toIndex=-1;
        if(fromIndex!=-1){
            fromIndex=XhtmlLine.indexOf(">", fromIndex);
            fromIndex++;
            toIndex=XhtmlLine.indexOf(xhtmlMarkUpEnd);
        }
        /* copy substring of interesting with <abstract>  */
        if(fromIndex!=-1 && toIndex!=-1) retString= XhtmlLine.substring(fromIndex, toIndex);
        
        return retString;
    }

    
    /**
     * parse wiki corpora. Note that this method is specialized for wiki abstract
     * corpus.
     * @param corporaPathName path name for source corpora which is in .xml format
     * @param outputName path name for destination file which will be consisted of cleaned text
     * @throws FileNotFoundException
     */

    public static void parseWikiCorpora(String corporaPathName,String outputName) throws FileNotFoundException, IOException{
        File file = new File(corporaPathName);
        FileReaderES fr=new FileReaderES(file);
        File fileOutput = new File(outputName);
        if (fileOutput.exists())
        {
            fileOutput.delete();
            fileOutput.createNewFile();
        }
        FileWriterES output = new FileWriterES (fileOutput);
        FileProcessingInfoES fpi=new FileProcessingInfoES(file);

        try {
            // two strings
            String finalText=null;
            String testText =null;

            // String wich represent seperated file
            String strForSeparatedFile="";


            while ((finalText=fr.readLine())!=null) {
             
                testText+=finalText;

                testText=getTextOfInterest(testText,"<abstract>","</abstract>");
                if(testText!=null){
                    // Dodato - izbacuje "__bezkn__", "__NOTC__" i "==Dogadyaji=="
                    if (testText.equals("__BEZKN__") || testText.equals("__NOTC__") || 
                            (testText.startsWith("==") && testText.endsWith("==") && testText.contains("Dogadyaji")))
                        continue;
                    
                    //  DocumentPreprocessor(separate punuc. from words)
                    String cleaned=ESPreprocessor.Singleton.INSTANCE.getSingleton().documentPreprocess(testText);
                    testText=null;

                    output.writeLine(cleaned);
                    
                    // and print info(how much is processing)
                    fpi.printInfo(cleaned);
                    // if you want to put new line
                    //NOTE:
                    //In Google airhead documentation notes that the text in the new line
                    //represents a separate file and it can be crucial to the final result
                    
                }


            }
             fr.close();
             output.close();
            // dispose all the resources after using them.
             
             System.out.println("\nAbstract extraction successful!\n");
            
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
    

    /**
     * just append text to file that is specified with file_path name
     * @param file_path
     * @param text
     */

    // Izmena - Kod ove metode je prebacen u glavnu metodu klase da se ne bi za svaku liniju
    // kreirao novi BufferedWriter
    /*
    private void writeToSpecifiedCorpora(String file_path,String text){
        Writer output = null;
        File file = new File(file_path);
        try {
          

            output = new BufferedWriter(new FileWriter(file,true));  // append to file(true)
            output.write(text);
            // if you want to put new line
            //NOTE:
            //In Google airhead documentation notes that the text in the new line
            //represents a separate file and it can be crucial to the final result

            output.write("\r\n");
            output.close();
        } catch (IOException ex) {
            Logger.getLogger(EnwikiCorpusReader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /* main for testing internal methods                   */
    public static void main(String []args) throws FileNotFoundException, IOException{
        WikiCorpusReader ecp=new WikiCorpusReader();
        ecp.parseWikiCorpora("serbianwiki.xml","serbiancorpus.txt");
        
    }

}
