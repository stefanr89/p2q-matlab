/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.ucl.sspace.rs.etf.corpus;

/**
 *
 * @author Miodrag Dinic RTI 2005/0473
 */
public class StopWordGeneratorException extends Exception{
    public StopWordGeneratorException(String msg){
        super(msg);
    }
}
