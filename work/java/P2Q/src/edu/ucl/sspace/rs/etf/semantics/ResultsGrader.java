/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ucl.sspace.rs.etf.semantics;

import edu.ucl.sspace.rs.etf.corpus.FileReaderES;
import edu.ucl.sspace.rs.etf.corpus.FileWriterES;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Vuk Batanovic
 */
public class ResultsGrader {

    private static int [] trainLength = {386, 449};
    private static int [] testLength = {167, 192};
    private String name;
    private boolean testOnly;
    
    private double [] equalTrainResults = new double [386];
    private double [] nonEqualTrainResults = new double [449];
    private double [] equalTestResults = new double [167];
    private double [] nonEqualTestResults = new double [192];
    
    public ResultsGrader (String s, boolean testOnly) throws FileNotFoundException, IOException
    {
        File equalFileTrain, nonEqualFileTrain, equalFileTest, nonEqualFileTest;
        name = s;
        this.testOnly = testOnly;
        if (!testOnly)
        {
            equalFileTrain = new File (name + "Eq.txt");
            nonEqualFileTrain = new File (name + "NonEq.txt");
            String testStr = name.replace("Train", "Test");
            equalFileTest = new File (testStr + "Eq.txt");
            nonEqualFileTest = new File (testStr + "NonEq.txt");
            
             FileReaderES fr = new FileReaderES (equalFileTrain);
            String line = null;
            int index = 0;
            while ((line = fr.readLine()) != null)
                equalTrainResults[index++] = Double.parseDouble(line);
            fr.close();

            fr = new FileReaderES (nonEqualFileTrain);
            line = null;
            index = 0;
            while ((line = fr.readLine()) != null)
                nonEqualTrainResults[index++] = Double.parseDouble(line);
            fr.close();
        }
        else
        {
            equalFileTest = new File (name + "Eq.txt");
            nonEqualFileTest = new File (name + "NonEq.txt");
        }
        
        FileReaderES fr = new FileReaderES (equalFileTest);
        String line = null;
        int index = 0;
        while ((line = fr.readLine()) != null)
            equalTestResults[index++] = Double.parseDouble(line);
        fr.close();
        
        fr = new FileReaderES (nonEqualFileTest);
        line = null;
        index = 0;
        while ((line = fr.readLine()) != null)
            nonEqualTestResults[index++] = Double.parseDouble(line);
        fr.close();
    }
    
    public void produceBestThreshold () throws IOException
    {
        String s;
        if (!testOnly)
            s = name.replace("ResultNumbersTrainSet", "");
        else
            s = name.replace("ResultNumbersTestSet", "");
        s = s.trim();
        File output = new File ("Threshold" + s + ".txt");
        FileWriterES fw = new FileWriterES(output);
        
        
        ArrayList<Double> bestThresholdsTrain = new ArrayList<Double> ();
        bestThresholdsTrain.add(0.1);
        double bestAccuracyTrain = 0;
        if (!testOnly)
        {

            for (double currentThreshold = 0.1; currentThreshold<=0.9; currentThreshold+=0.001)
            {
                int trainCount = 0;
                for (double d: equalTrainResults)
                    if (d >= currentThreshold) trainCount ++;
                for (double d: nonEqualTrainResults)
                    if (d < currentThreshold) trainCount ++;
                double currentAccuracy = trainCount / 835.0;
                if (currentAccuracy > bestAccuracyTrain)
                {
                    bestAccuracyTrain = currentAccuracy;
                    bestThresholdsTrain.clear();
                    bestThresholdsTrain.add(currentThreshold);
                }
                else if (currentAccuracy == bestAccuracyTrain)
                    bestThresholdsTrain.add(currentThreshold);
            }
        }
        
        double bestThresholdTest = 0.1, bestAccuracyTest = 0;
        double finalAccuracy = 0, selectedThreshold = 0.1;
        for (double currentThreshold = 0.1; currentThreshold<=0.9; currentThreshold+=0.001)
        {
            int testCount = 0;
            for (double d: equalTestResults)
                if (d >= currentThreshold) testCount ++;
            for (double d: nonEqualTestResults)
                if (d < currentThreshold) testCount ++;
            double currentAccuracy = testCount / 359.0;
            if (currentAccuracy > bestAccuracyTest)
            {
                bestAccuracyTest = currentAccuracy;
                bestThresholdTest = currentThreshold;
            }
            if (!testOnly && bestThresholdsTrain.contains(currentThreshold))
            {
                if (currentAccuracy > finalAccuracy)
                {
                    finalAccuracy = currentAccuracy;
                    selectedThreshold = currentThreshold;
                }
            }

        }
        
        if (!testOnly)
        {
            fw.writeLine("///  Train file  ///");
            fw.writeLine("Threshold: " + selectedThreshold);
            fw.writeLine("Accuracy: " + bestAccuracyTrain);
            fw.writeLine("");
        }
        fw.writeLine("///  Test file  ///");
        fw.writeLine("Threshold: " + bestThresholdTest);
        fw.writeLine("Accuracy: " + bestAccuracyTest);
        if (!testOnly)
        {
            fw.writeLine("");
            fw.writeLine("Final Accuracy: " + finalAccuracy);
            
            double precision, recall, fMeasure;
            int truePositives = 0, falsePositives = 0, falseNegatives = 0;
            
            for (double d: equalTestResults)
                if (d >= selectedThreshold) truePositives++;
                else falseNegatives++;
            
            for (double d: nonEqualTestResults)
                if (d >= selectedThreshold) falsePositives++;
            
            precision = truePositives / (double)(truePositives + falsePositives);
            recall = truePositives / (double)(truePositives + falseNegatives);
            fMeasure = 2*precision*recall / (precision + recall);
            
            fw.writeLine("Precision: " + precision);
            fw.writeLine("Recall: " + recall);
            fw.writeLine("F-Measure: " + fMeasure);
        }
        fw.close();
    }
}
