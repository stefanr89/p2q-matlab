/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ucl.sspace.rs.etf.paraphrase;

import java.util.ArrayList;

/**
 *
 * @author vuk
 */
public class Article {
    
    private ArrayList<Paraphrase> list;
    private String name;
    
    public Article (String name)
    {
        this.name = name;
        list = new ArrayList<Paraphrase> ();
    }
    
    public void addParaphrase (String str1, String str2, int sameWordCount, int shortSentWordCnt, int longSentWordCnt)
    {
        if (!paraphraseExists(str1, str2)) 
            list.add(new Paraphrase (str1, str2, new ParaphraseStatistics (sameWordCount, shortSentWordCnt, longSentWordCnt)));
    }
    
    public void addParaphrase (Paraphrase p)
    {
        if (!paraphraseExists(p)) list.add(p);
    }
    
    public ArrayList<Paraphrase> getParaphrases ()
    {
        return list;
    }
    
    public Paraphrase getFirstParaphrase ()
    {
        if (list.size()>0) return list.get(0);
        else return null;
    }
    
    public Paraphrase getBestParaphrase ()
    {
        if (list.isEmpty()) return null;
        if (list.size() == 1) return list.get(0);
        else
        {
            double bestGrade = list.get(0).gradeParaphrase();
            int bestIndex = 0;
            for (int i=1; i<list.size(); i++)
            {
                double newGrade = list.get(i).gradeParaphrase();
                if (newGrade > bestGrade)
                {
                    bestGrade = newGrade;
                    bestIndex = i;
                }
            }
            return list.get(bestIndex);
        }
    }
    
    public int paraphraseNo () { return list.size();}
    
    public String toString ()
    {
        StringBuilder sb = new StringBuilder ("");
        for (int i=0; i<list.size(); i++)
            if (i != list.size() - 1)
                sb.append(list.get(i)).append("\r\n");
            else
                sb.append(list.get(i));
        return sb.toString();
    }
    
    private boolean paraphraseExists (String str1, String str2)
    {
        boolean exists = false;
        for (Paraphrase p: list)
            if ((p.getSentence1().equals(str1) && p.getSentence2().equals(str2))
                    || (p.getSentence1().equals(str2) && p.getSentence2().equals(str1)))
            {
                exists = true;
                break;
            }
        return exists;
    }
    
    private boolean paraphraseExists (Paraphrase par) { return paraphraseExists(par.getSentence1(), par.getSentence2());}
}
