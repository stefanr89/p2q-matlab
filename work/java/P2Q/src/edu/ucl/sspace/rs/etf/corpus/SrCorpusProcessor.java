/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ucl.sspace.rs.etf.corpus;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author Vuk Batanovic
 */
public class SrCorpusProcessor {
    
    // Metoda za transformaciju teksta korpusa koji moze biti na cirilici ili latinici u formu koju prihvata stemmer
    
    public static void processSrCorpus (String corpusPath, String outputPath) throws FileNotFoundException, IOException
    {
        File fileIn = new File(corpusPath);
        BufferedReader br = new BufferedReader (new FileReader (fileIn));
        File fileOut = new File (outputPath);
        if (fileOut.exists())
        {
            fileOut.delete();
            fileOut.createNewFile();
        }
        PrintWriter pw = new PrintWriter (fileOut);
        FileProcessingInfoES fpi = new FileProcessingInfoES (fileIn);
        
        try {            
            int intCharacter;
            char znak;
            char stariZnak = ' ';
            while ((intCharacter = br.read()) != -1)
            {
                znak = (char) intCharacter;
                fpi.printInfo(znak);
                String izlaz = processCharacter (intCharacter, stariZnak);
                pw.print(izlaz);
                stariZnak = znak;
            }
            
            
            br.close();
            pw.flush();
            pw.close();
            // dispose all the resources after using them.
            
            System.out.println ("\nConversion successful!\n");
            
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    public static String processSrString (String word)
    {
        String newWord="";
        if (word == null || word.equals("")) return newWord;
        char znak;
        char stariZnak = ' ';
        int intCharacter;
        for (int i=0; i<word.length(); i++)
        {
            intCharacter = word.codePointAt(i);
            znak = (char) intCharacter;
            String izlaz = processCharacter (intCharacter, stariZnak);
            newWord += izlaz;
            stariZnak = znak;
        }
        return newWord;
    }
    
    private static String processCharacter (int intCharacter, char stariZnak)
    {
        char znak = (char) intCharacter;
        
        // ako karakter nije slovo -> prepisuje se
        if (!Character.isLetter(intCharacter))
            return Character.toString(znak);

        // ako se radi o karakteru koje spada u engleski alfabet i nije 'j'/'J', prepisuje se
        else if (Character.toString(znak).matches("\\w") && znak != 'j' && znak != 'J') 
            return Character.toString(znak);

        // ako se radi o karakteru 'j'/'J', onda je mozda u pitanju slovo Lj/Nj/Dj
        else if (znak == 'j' || znak == 'J') 
        {
            if (stariZnak != 'l' && stariZnak != 'L' && stariZnak != 'n' && stariZnak != 'N' && stariZnak != 'd' && stariZnak != 'D')
                return Character.toString(znak);
            else return "y";
        }

        // ovde se radi obrada dijakritika i cirilicnih slova        
        else switch (znak)
        {

            // latinicni dijakritici

            case 'ć': return "cy";
            case 'č': return "cx";
            case 'š': return "sx";
            case 'ž':                           // provera da li se radi o dz
                if (stariZnak == 'd')
                    return "dx";
                else if (stariZnak == 'D')
                    return "Dx";
                else
                    return "zx";
            case 'đ': return "dy";

            case 'Š': return "Sx";
            case 'Ž': return "Zx";
            case 'Đ' : return "Dy";
            case 'Ć': return "Cy";
            case 'Č': return "Cx";


            // cirilica    

            case 'а': return "a";
            case 'А': return "A";
            case 'б': return "b";
            case 'Б': return "B";
            case 'в': return "v";
            case 'В': return "V";
            case 'г': return "g";
            case 'Г': return "G";
            case 'д': return "d";
            case 'Д': return "D";
            case 'ђ': return "dy";
            case 'Ђ': return "Dy";
            case 'е': return "e"; 
            case 'Е': return "E";
            case 'ж': return "zx";
            case 'Ж': return "Zx";   
            case 'з': return "z";
            case 'З': return "Z";
            case 'и': return "i";
            case 'И': return "I";    
            case 'ј': return "j";    
            case 'Ј': return "J";    
            case 'к': return "k";    
            case 'К': return "K";    
            case 'л': return "l";   
            case 'Л': return "L";    
            case 'љ': return "ly";
            case 'Љ': return "Ly";    
            case 'м': return "m";    
            case 'М': return "M";    
            case 'н': return "n";    
            case 'Н': return "N";    
            case 'њ': return "ny";    
            case 'Њ': return "Ny";
            case 'о': return "o";    
            case 'О': return "O";    
            case 'п': return "p";    
            case 'П': return "P";    
            case 'р': return "r";
            case 'Р': return "R";    
            case 'с': return "s";    
            case 'С': return "S";    
            case 'т': return "t";    
            case 'Т': return "T";    
            case 'ћ': return "cy";
            case 'Ћ': return "Cy";    
            case 'у': return "u";    
            case 'У': return "U";    
            case 'ф': return "f";    
            case 'Ф': return "F";    
            case 'х': return "h";
            case 'Х': return "H"; 
            case 'ц': return "c";
            case 'Ц': return "C";     
            case 'ч': return "cx";                
            case 'Ч': return "Cx";                         
            case 'џ': return "dx";                         
            case 'Џ': return "Dx";                         
            case 'ш': return "sx";                         
            case 'Ш': return "Sy";   
            
        }
     return "";   
    }
}
