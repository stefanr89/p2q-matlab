/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ucl.sspace.rs.etf.paraphrase;

import edu.ucl.sspace.rs.etf.corpus.FileWriterES;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author Vuk Batanovic
 */
public class ParaphraseCorpusBuilder {
    
    private class StringWID implements Comparable<StringWID>{
        private int id;
        private String text;
        
        public StringWID (String s, int i)
        {
            id = i;
            text = s;
        }
        
        public boolean equals (StringWID o)
        {
            if (this.id == o.id) return true;
            else return false;
        }

        public int compareTo(StringWID o) {
            if (this.id < o.id) return -1;
            else if (this.id == o.id) return 0;
            else return 1;
        }
        
        public String getText () { return text;}
        public String toString () { return text;}
    }

    
    public void buildTrainAndTestCorpus (String inputFileString) throws IOException
    {   
        File inputFile = new File (inputFileString);
        ArrayList<StringWID> strings = new ArrayList<StringWID> (1000);
        
        BufferedReader br = new BufferedReader (new InputStreamReader (new FileInputStream(inputFile)));
        String line = null;
        int cnt = 0;
        while ((line = br.readLine()) != null)
        {
            StringWID swid = new StringWID (line, (int) (Math.random() * 10000));
            strings.add(swid);
            cnt++;
        }
        br.close();

        Object [] stringsArray = strings.toArray();
        Arrays.sort(stringsArray);
        

        File train = new File ("ParaphraseTrainSet.txt");
        File test = new File ("ParaphraseTestSet.txt");
        if (train.exists())
        {
            train.delete();
            train.createNewFile();
        }
        if (test.exists())
        {
            test.delete();
            test.createNewFile();
        }
        FileWriterES trainFile = new FileWriterES (train);
        FileWriterES testFile = new FileWriterES (test);

        for (int i=0; i<= (int) (cnt * 0.3); i++)
            testFile.writeLine(stringsArray[i].toString());
        for (int i=(int) (cnt *0.3) + 1; i<cnt; i++)
            trainFile.writeLine(stringsArray[i].toString());
        
        testFile.close();
        trainFile.close();
    }
}
