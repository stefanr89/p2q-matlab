/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ucl.sspace.rs.etf.paraphrase;

/**
 *
 * @author Vuk Batanovic
 */
public class ParaphraseStatistics {
    
    private int sameWordCount;
    private int shorterSentenceWordCount;
    private int longerSentenceWordCount;
    
    public ParaphraseStatistics (int sameWordCount, int shorterSentenceWordCount, int longerSentenceWordCount)
    {
        this.sameWordCount = sameWordCount;
        this.shorterSentenceWordCount = shorterSentenceWordCount;
        this.longerSentenceWordCount = longerSentenceWordCount;
    }
    
    public int getSameWordCount() {
        return sameWordCount;
    }

    public void setSameWordCount(int sameWordCount) {
        this.sameWordCount = sameWordCount;
    }

    public int getShorterSentenceWordCount() {
        return shorterSentenceWordCount;
    }

    public void setShorterSentenceWordCount(int shorterSentenceWordCount) {
        this.shorterSentenceWordCount = shorterSentenceWordCount;
    }
    
    public int getLongerSentenceWordCount() {
        return longerSentenceWordCount;
    }

    public void setLongerSentenceWordCount(int longerSentenceWordCount) {
        this.longerSentenceWordCount = longerSentenceWordCount;
    }
}