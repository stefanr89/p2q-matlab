/****** Script for SelectTopNRows command from SSMS  ******/
--SELECT * 
--FROM [interestminingl6typeall].[dbo].Users as users
--inner join [interestminingl6typeall].[dbo].Posts as posts
--on users.id = (select top 1 ownerUserId from [interestminingl6typeall].[dbo].Posts as p1 where posts.acceptedAnswerId = p1.id) 
--WHERE users.type='3' and posts.postTypeId = 1


--SELECT id FROM [interestminingl6typeall].[dbo].Posts WHERE postTypeId = 1 AND cat2='Environment';
-- za svakog korisnika iz TypeDB dohvati po jedno pitanje na koje je odgovorio kao bestAnswer

-- dohvati pitanja koje je korisnik najbolje odgovorio
select question.*  
from [interestminingl6typeall].[dbo].Posts as question
inner join [interestminingl6typeall].[dbo].Posts as bestAnswer
on question.acceptedAnswerId = bestAnswer.id
WHERE question.acceptedAnswerId <> '' and bestAnswer.ownerUserId = 'u1000694' 

-- dohvati pitanja za datog korisnika koja je on najbolje odgovorio
select bestAnswer.id, bestAnswer.ownerUserId, bestAnswer.body, question.*   
from [interestminingl6typeall].[dbo].Posts as question
inner join (
-- za odredjenog korisnika
select posts.* from [interestminingl6typeall].[dbo].Posts as posts 
inner join [interestminingl6typeall].[dbo].Users as users
on users.id = posts.ownerUserId
where users.id = 'u1005846' 
) as bestAnswer
on question.acceptedAnswerId = bestAnswer.id
WHERE question.acceptedAnswerId <> ''
--group by  bestAnswer.ownerUserId

SELECT * 
FROM [interestminingl6typeall].[dbo].Users as users
WHERE users.type='3' 

--dohvati za pitanje korisnika koji je pitao
SELECT u.* ,p.* 
FROM [interestminingl6typeall].[dbo].Users as u, [interestminingl6typeall].[dbo].Posts p 
WHERE p.id = '1125569' AND u.id = p.ownerUserId AND p.postTypeId = 1

-- dohvati korisnika koji je dao acceptedAnswer za dato pitanje
select bestAnswer.id as bestAnswerid, bestAnswer.ownerUserId as bestAnswerer, bestAnswer.body as bestAnswerbody, question.*   
from [interestminingl6typeall].[dbo].Posts as question
inner join  [interestminingl6typeall].[dbo].Posts as bestAnswer
on question.acceptedAnswerId = bestAnswer.id
WHERE question.id = '3501383'

-- raspon vrednosti za svaki informationSourceId
SELECT informationSourceId,max(weight), min(weight)
FROM [interestminingl6typeall].[dbo].[Evidence]
group by informationSourceId

use interestminingl6typeall
select bestAnswer.id as bestAnswerid from Posts as question inner join Posts as bestAnswer 
on question.acceptedAnswerId = bestAnswer.id
WHERE question.id = '3501383'


UPDATE Evidence SET weight = (weight-0.00559557)/(10.6288-0.00559557) WHERE informationSourceId = 'TFIDF';

SELECT count(*)
FROM [interestminingl6typeall].[dbo].[Evidence]
WHERE userId <> '' --informationSourceId = 'TFIDF' and weight = 0
