/*
TYPE3DB (we return only userIDs): 

topAskersAndAnswerers (knowledge&interest) - the one who ASKED and BEST ANSWERED 10(5*questions+5*answers)+1(asnwer). 
Each of these questions must have at least 5 to max 20 answers. Again we need About 100 users within 1000 questions.

*/
SELECT DISTINCT(q1.userId), q1.questionCount, q1.questionList, q2.answerCount, q2.answerList FROM
	(SELECT u.id AS userId, COUNT(p.id) AS questionCount, group_concat(p.id) AS questionList
	FROM 
		Posts p,
		Users u
	WHERE 
		p.postTypeId = 1 AND
		u.id = p.ownerUserId AND
		5 <= p.answerCount AND
		20 >= p.answerCount AND
		u.id != ''
	GROUP BY
		u.id
	HAVING
		COUNT(p.id) = 5) AS q1,
	(SELECT u.id AS userId, COUNT(p.id) AS answerCount, group_concat(p.id) AS answerList
	FROM 
		Posts p,
		Users u
	WHERE 
		p.postTypeId = 2 AND
		u.id = p.ownerUserId AND
		5 <= (SELECT p1.answerCount FROM Posts p1 WHERE p1.id = p.parentId) AND
		20 >= (SELECT p1.answerCount FROM Posts p1 WHERE p1.id = p.parentId) AND
		u.id != ''
	GROUP BY
		u.id
	HAVING
		COUNT(p.id) = 6) AS q2
WHERE
	q1.userId = q2.userId
LIMIT 1000;