/****** Script for SelectTopNRows command from SSMS  ******/
SELECT [userId], count(keyword) 
   FROM [yahooL6].[dbo].[Evidence]
  WHERE  informationSourceId='TFIDF' -- userId='u1517486' --informationSourceId='tfidf'
  group by userId
  having count(keyword)=0

SELECT COUNT(id), ownerUserId 
FROM 
 [yahooL6].[dbo].Posts
WHERE
 postTypeId = 2
GROUP BY ownerUserId
HAVING COUNT(ownerUserId) > 4
ORDER BY COUNT(id) DESC;
--[17:19:31] Slavko Žitnik: no, 13 such users
--[17:19:47] Slavko Žitnik: and 246 users have answered more than 1 question
--[17:20:13] Slavko Žitnik: 51 users answered more than 2 questions
--[17:20:24] Slavko Žitnik: 22 users more than 3
--[17:20:37] Slavko Žitnik: 13 users more than 4
--[17:20:48] Slavko Žitnik: 8 users more than 5

SELECT informationSourceId 
   FROM [yahooL6].[dbo].[Evidence]
  group by informationSourceId

  select *
  from [yahooL6].[dbo].Posts
  where id='1002731'

  SELECT * FROM [yahooL6].[dbo].Posts WHERE id = '1002731';
SELECT * FROM [yahooL6].[dbo].Posts e, [yahooL6].[dbo].EvidencePost ep WHERE e.id=ep.idEvidence AND idPost = '1002731';


use [yahooL6]
UPDATE Evidence SET weight = (weight-0.006116388365626335)/(10.851897239685059-0.006116388365626335) WHERE informationSourceId = 'TFIDF';