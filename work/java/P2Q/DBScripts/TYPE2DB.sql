/*
TYPE2DB (we return only userIDs): 

topAnswerers (knowledge) - the one who BEST ANSWERED 10(answers)+1(answer) questions. 
Each of these questions must have at least 5 to max 20 answers. 
Again we need about 100 best answerers within 1000 questions.

*/
SELECT u.id AS userId, COUNT(p.id) AS answerCount, group_concat(p.id) AS answerList
	FROM 
		Posts p,
		Users u
	WHERE 
		p.postTypeId = 2 AND
		u.id = p.ownerUserId AND
		5 <= (SELECT p1.answerCount FROM Posts p1 WHERE p1.id = p.parentId) AND
		20 >= (SELECT p1.answerCount FROM Posts p1 WHERE p1.id = p.parentId) AND
		u.id != ''
	GROUP BY
		u.id
	HAVING
		COUNT(p.id) = 11
LIMIT 1000;