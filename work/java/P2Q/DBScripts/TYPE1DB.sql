/*
TYPE1DB (we return only userIDs): 

topQuestioners (interest) - the one who ASKED 10(questions)+1(answer) questions. 
Each of these questions must have at least 5 to max 20 answers. 
For that 100 users, find also all answers they have answered (the whole threads).
*/
SELECT u.id AS userId, COUNT(p.id) AS questionCount, group_concat(p.id) AS questionList
	FROM 
		Posts p,
		Users u
	WHERE 
		p.postTypeId = 1 AND
		p.answerCount >= 5 AND
		p.answerCount <= 20 AND
		u.id = p.ownerUserId AND
		u.id != '' AND 
		u.id IN (
				SELECT p1.ownerUserId
				FROM 
					Posts p1
				WHERE 
					p1.postTypeId = 2 AND
					u.id = p1.ownerUserId)
	GROUP BY
		u.id
	HAVING
		COUNT(p.id) = 10
LIMIT 1000;