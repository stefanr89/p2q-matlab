classdef ESPreprocessor < handle
    %ESPREPROCESSOR Priprema tekst za dalju obradu
    %   Cisti tekst od suvisnih simbola i radi stemming
    
    properties(Access = 'private')
        listOfStopWords; %lista stop reci, pokupljena iz fajla
    end
    
    methods
        function obj = ESPreprocessor(filename)
            % sve sto konstruktor radi je inicijalizacija liste stop reci i
            % porter-stemmera
            
            obj@handle(); %konstruktor superklase
            
            if nargin == 0 %moze da se pozove i bez argumenata, tada ide default stop lista
                filename = 'work/m/P2Q/resource/eng/english-stemmed-stop-words.txt';
            end
            
            obj.initializeListOfStopWords(filename);
        end
        
        function initializeListOfStopWords(obj, filename)
            %otvara fajl sa stop recima, cita ga u listu
            %ovo je u posebnoj funkciji za slucaj da hocemo u toku rada da
            %promenimo fajl sa stop recima
            stop_words_file = fopen(filename, 'r');
            obj.listOfStopWords = {}; %obrisemo prethodni sadrzaj
            if stop_words_file == -1 
                disp('GRESKA: neuspesno otvaranje fajla');
                return;
            end
            obj.listOfStopWords = textscan(stop_words_file, '%s', 'Delimiter', '\n'); % cita u cell array
            %disp(obj.listOfStopWords{:});
            fclose(stop_words_file);
        end
        
        function out = filterWeightedWords(obj, strWithValues, bIncludeNumbers, excludeStopWords)
            %ova funkcija filtrira reci i radi stemming
            
            validMap = containers.Map(); % ovo se vraca kao rezultat
            words = lower(strWithValues.keys()); %prvo sve prebacimo u lower
            for I=1:numel(words) %za svaku rec
                split_words = strsplit(words{I});
                for J=1:numel(split_words)
                    word = split_words{J};
                    stemmed_word = char(porterStemmer(word)); %dohvata stemmovanu rec, moramo da pretvorimo word iz cell u string
                    isOkCleanWords = all(isstrprop(stemmed_word, 'alpha')); %proverimo da li je rec cista
                    isOkCleanNumber = bIncludeNumbers && all(isstrprop(stemmed_word, 'digit')); %proveravamo da li je broj cist, treba nam ako je setovan bIncludeNumbers
                    if excludeStopWords %izbacujemo stop reci
                        isStopWord = find(strcmp(obj.listOfStopWords{:}, stemmed_word));
                        if isStopWord
                            continue; %ako jeste stop rec samo preskacemo iteraciju
                        end
                    end
                    if isOkCleanWords || isOkCleanNumber %ako je podatak ispravan
                        if validMap.isKey(stemmed_word) %ako vec imamo rec u tabeli
                            val_a = validMap(stemmed_word); 
                            val_b = strWithValues(words{I}); %ako je u pitanju vise reci, TF za svaku od njih je isti kao TF za konstrukciju
                            validMap(stemmed_word) = val_a + val_b - val_a*val_b; %prema ovoj formuli racunamo T-conorm
                        else %ako nemamo datu rec u tabeli
                            validMap(stemmed_word) = strWithValues(words{I}); %samo dodajemo TF koji smo dobili
                        end
                    end
                end
            end
            out = validMap; %vracamo hash map ociscenih i stemmovanih reci
        end
    end %end public methods
    
    methods(Static)
        function main(args)
            tic
            keyset1 = {'computer', 'tech', 'internet'};
            valset1 = [0.7, 0.4, 0.4];
            esp = ESPreprocessor();
            res = esp.filterWeightedWords(containers.Map(keyset1, valset1), false, true);
            disp(res.keys());
            disp(res.values());
            toc
        end %end main method
    end
    
end

