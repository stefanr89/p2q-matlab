classdef MatrixES
    %MatrixES - Operacije nad matricama
    %   Neki delovi ove klase i nisu neophodni (mozda cak i cela klasa),
    %   ali neka ostanu da bi se odrzala slicnost sa Java kodom.
    
    properties
    end
    
    methods(Static = true)
        function out = scalarMultiplication(matrix, factor)
            %mnozi matricu faktorom
            out = matrix * factor; 
        end
        
        function out = addition(matrix1, matrix2)
            %sabira 2 matrice
            out = matrix1 + matrix2; 
        end
        
        function [out_mat, ro] = findMaxElemAndRemove(matrix, ro)
            %nalazi najveci elemenet u matrici matrix, stavlja ga u listu
            %ro i brise red i kolonu tog elementa iz matrice
            if(~all(size(matrix)))
                out_mat = 0;
                return
            end
            out_mat = matrix;
            [elem, pos] = max(matrix(:));
            [I,J] = ind2sub(size(matrix), pos);
            out_mat(I, :) = []; %brise red
            if size(matrix, 2) > 0
                out_mat(:, J) = []; %brise kolonu samo ako ima sta da brise
            end
            ro(end+1) = elem; %ro je array
        end
        
        function out = findMaxElemRowsSum(matrix)
            %sumira najveci element iz svakog reda matrice
            if(~all(size(matrix)))
                out = 0;
                return
            end
            out = sum(max(matrix, [], 2));
        end
        
        function out = findMaxElemRowsSumAndMultiplyWithWeight(matrix, matrixW)
            %isto kao prethodna funkcija samo mnozi sa weightom
            res = 0;
            for I=1:size(matrix, 1)
                [elem, pos] = max(matrix(I, :));
                res = res + elem * matrixW(I, pos);
            end
            out = res;
        end
        
        function out = findMaxElemColumnsSum(matrix)
            %sumira najveci element iz svake kolone matrice
            if(~all(size(matrix)))
                out = 0;
                return
            end
            out = sum(max(matrix));
        end
        
        function main(args)
            %ovo je prekopirana main funkcija iz Java klase
            matrix = [0.505, 0.010, 0.195, 0.162, 0.297, 0.449;
                      0.018, 0.248, 0.204, 0.083, 0.017, 0.011;
                      0.242, 0    , 0.039, 0.071, 0.032, 0.044;
                      0.416, 0.041, 0.134, 0.133, 0.225, 0.124;];
            N = 4;
            M = 6;
            matrixW = zeros(N, M);
            for I=1:N
                for J=1:M
                    matrixW(I,J) = 2^(((I-1)*(J-1))/(M*N)-1);
                end
            end
            
            %stampa matricu, ako je potrebna veca preciznost obrisati % sa
            %sledece linije
            %format long
            fprintf('--------------------------------\n');
            disp(matrix);
            fprintf('--------------------------------\n');
            disp(matrixW);
            fprintf('RowMAX&MultiplyWithWeight: %.15f\n', MatrixES.findMaxElemRowsSumAndMultiplyWithWeight(matrix, matrixW));
        end
    end
    
end

