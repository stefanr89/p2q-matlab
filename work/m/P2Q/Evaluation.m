classdef Evaluation
    %EVALUATION MRR i P@N evaluacija za CE, SemNet i TFIDF
    %   Odradice evaluaciju za 1 pitanje 
    
    properties
    end
    
    methods(Static)
        function main(args)
            %test, samo za Type 1 bazu (pitanja) sa konceptima u aEs
            
            %ocekivani rezultat treba da se poklapa sa ovim dobijenim iz
            %jave:
            %Sep 18, 2014 6:53:21 PM edu.ucl.sspace.rs.etf.IQRSEvaluation.Evaluation parallelExecutionBagOfTasks
            %INFO: Question Processing Execution time:5643622
            %Sep 18, 2014 6:53:21 PM edu.ucl.sspace.rs.etf.IQRSEvaluation.Evaluation parallelExecutionBagOfTasks
            %INFO: Result calculateMRRRank: 0.05263157894736842
            %Sep 18, 2014 6:53:21 PM edu.ucl.sspace.rs.etf.IQRSEvaluation.Evaluation parallelExecutionBagOfTasks
            %INFO: Rank is: 19.0 out of :100
            %Sep 18, 2014 6:53:21 PM edu.ucl.sspace.rs.etf.IQRSEvaluation.Evaluation parallelExecutionBagOfTasks
            %INFO: Question Count: 1

            aEs = {'ConceptExtractor', 'SemNet', 'TFIDF'};
            aCs = {'Cars & Transportation', 'Computers & Internet', 'Food & Drink', 'Society & Culture', 'Travel'};
            
            fprintf('--------------------------------------\n');
            fprintf('DB Type: 1 **** EvidenceType QUESTION\n');
            fprintf('+++++++++++++++++++++++++++++++++++++++++++++++++++\n');
            fprintf('ConceptExtractor, SemNet, TFIDF\n');
            
            questionCount = 0;
            evalMRRValues = {};
            evalPrecisionValues = {};
            
            questions = {};
            usersList = EvidenceToHashMapMapper.getAllUserIdsForType(1, aCs); %prvo pokupimo relevantne userIDs
            for I=1:numel(usersList) %pa onda pitanja na koje su davali best answer
                retrieved = EvidenceToHashMapMapper.getAllQuestionIdsForUserForEvaluation(usersList{I});
                questions{end+1} = retrieved{1}; %samo se prvo pitanje uzima
            end
            
            for I=1:numel(questions) %prodjemo kroz pitanja
                tic
                fprintf('Start QUESTION Processing!!!\n');
                
                %questionId = questions{I};
                questionId = '2347417'; %test pitanje je 2347417
                
                questionEvidences = containers.Map(); %ovo je joint hashmap za sve tipove dokaza
                mapperList = {};
                for J=1:numel(aEs)
                    mapper = EvidenceToHashMapMapper(aEs{J});
                    mapperList{end+1} = mapper;
                    pmap = mapper.generatePostHashMap(questionId);
                    questionEvidences = EvidenceToHashMapMapper.joinEvidences(questionEvidences, pmap);
                end
                
                esp = ESPreprocessor(); %preprocesiranje
                questionEvidences = esp.filterWeightedWords(questionEvidences, false, true); %bez brojeva, izbacuje stop reci
                
                printMap('preproc q: ', questionEvidences);
                
                sThread = QuestionToProfileSimilarityBagOfTasks(questionId, questionEvidences,...
                    usersList, mapperList, 1, true, true); %Max2 Weighted vatijanta za questions
                sThread.run();
                
                results = sThread.getResults(); %dohvatamo rezultate
                
                questionCount = questionCount + 1; %pitanje obradjeno, inkrementiramo ukupan broj
                
                bestAnswererIdForQuestion = EvidenceToHashMapMapper.getBestAnswererIdForQuestion(questionId);
                rank = Evaluator.calculateRank(bestAnswererIdForQuestion, results);
                resultMRRRank = 1/rank; %nema potrebe da ovo bude posebna funkcija
                evalMRRValues{end+1} = resultMRRRank;
                evalPrecisionValues{end+1} = rank;
                
                toc
                fprintf('Result calculateMRRRank: %f\n', resultMRRRank);
                fprintf('Rank is %d out of %d\n', rank, numel(results));
                fprintf('Question Count: %d\n', questionCount);
                break; %test
            end
            
            fprintf('The final MRR result is: %f\n', Evaluator.calculateMRR(evalMRRValues));
            fprintf('The final P@5 result is: %f\n', Evaluator.calculatePrecisionAtN(evalPrecisionValues, 5));
            fprintf('The final P@10 result is: %f\n', Evaluator.calculatePrecisionAtN(evalPrecisionValues, 10));
            fprintf('The final P@15 result is: %f\n', Evaluator.calculatePrecisionAtN(evalPrecisionValues, 15));
            fprintf('The final P@20 result is: %f\n', Evaluator.calculatePrecisionAtN(evalPrecisionValues, 20));
            fprintf('The final P@30 result is: %f\n', Evaluator.calculatePrecisionAtN(evalPrecisionValues, 30));
            fprintf('Number of questions: %d\n' + questionCount);
        end %end main
    end %end static methods
    
end

