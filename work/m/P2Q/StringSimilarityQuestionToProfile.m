classdef StringSimilarityQuestionToProfile < StringSimilarity
    %STRINGSIMILARITYQUESTIONTOPROFILE utvrdjuje string slicnost ali za P2Q algoritam
    %   Kao StringSimilarity samo je P uvek question a R uvek user
    
    methods
        function obj = StringSimilarityQuestionToProfile(question, user)
            %Kod P2Q potrebno je da P i R budu tacno po odredjenom redosledu, tj.
            %da ih StringSimilarity konstruktor ne ispremensta. Treba
            %proslediti iskljucivo 2 containers.Map objekta
            obj@StringSimilarity();
            
            obj.refP = question;
            obj.refR = user;
            obj.P = question.keys();
            obj.R = user.keys();
            obj.m = numel(obj.P);
            obj.n = numel(obj.R);
        end
        
        function out = getStringSimilarityMatrix(obj)
            %pravi string similarity matrix na osnovu recenica koje smo
            %dobili pri konstrukciji
            simMatrix = zeros(obj.m, obj.n);
            for I=1:numel(obj.P)
                for J=1:numel(obj.R)
                    simMatrix(I,J) = obj.getStringSimilarity(obj.R{J}, obj.P{I});
                end
            end
            out = simMatrix;
        end %end getStringSimilarityMatrix
    end
    
    methods(Static)
        function main(args)
            %jednostavan test, ipak ovo i StringSimilarity rade prakticno istu stvar
            tic
            keyset1 = {'computer', 'is', 'looking', 'for', 'hight', 'tech', 'improvement'};
            valset1 = [0.1, 0.1, 0.5, 0.1, 0.5, 0.1, 0.5];
            keyset2 = {'history', 'try', 'to', 'prove', 'what', 'happen', 'a', 'long', 'time', 'ago'};
            valset2 = [0.1, 0.1, 0.5, 0.1, 0.5, 0.1, 0.5, 0.1, 0.5, 0.1];
            
            question = containers.Map(keyset1, valset1);
            user = containers.Map(keyset2, valset2);
            ss = StringSimilarity(question, user);
            ssp2q = StringSimilarityQuestionToProfile(question, user);
            calc1 = ss.getStringSimilarityMatrix();
            calc2 = ssp2q.getStringSimilarityMatrix();
            
            fprintf('--------------------------------\n');
            disp(calc1);
            fprintf('--------------------------------\n');
            disp(calc2);
            fprintf('--------------------------------\n');
            fprintf('Matrice su iste: %d\n', all(all(calc1 == calc2)));
            toc
        end
    end
    
end

