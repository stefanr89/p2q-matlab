function printMap(msg, map)
%PRINTMAP izbacuje containers.Map objekat na ekran
%   malo formatira output jer nema dobrog nacina da se elegantno prikaze
%   sadrzaj containers.Map objekta, msg je poruka koja ide pre mape

keys = map.keys();

fprintf(msg);
fprintf('{');
for I=1:numel(keys)
    fprintf('%s=%.15f ',keys{I}, map(keys{I}));
end
fprintf('}\n');

end

