% Ovde postavljamo pathove i to prvo za .m matlab fajlove a zatim i za java fajlove
function setpaths()
    %postavlja paths i importuje MSSQL drajver
    message1 = 'Added path %s to Java class path.\n';

    javaaddpath('.\work\java\P2Q\');
    fprintf(message1, '.\work\java\P2Q\');
    javaaddpath('.\work\java\P2Q\lib\'); % ovde su .jar fajlovi
    fprintf(message1, '.\work\java\P2Q\lib\');
    javaaddpath('.\work\java\P2Q\bin\'); % ovde su kompajlirane klase i paketi
    fprintf(message1, '.\work\java\P2Q\bin\');
    javaaddpath('.\work\java\P2Q\lib\sqljdbc4.jar'); % MSSQL konektor
    fprintf(message1, '.\work\java\P2Q\lib\sqljdbc4.jar');
end
