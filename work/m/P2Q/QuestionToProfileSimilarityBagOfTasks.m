classdef QuestionToProfileSimilarityBagOfTasks < QuestionToProfileSimilarity
    %QUESTIONTOPROFILESIMILARITYBAGOFTASKS P2Q za vise pitanja/korisnika
    %   Sadrzi funkciju run koja radi semanticku analizu za vise pitanja i
    %   korisnika. Polje results sadrzi rezulatate analize koji se mogu
    %   dalje koristiti pri evaluaciji.
    
    properties
        usersList; %lista korisnika
        question; %pitanje i weight
        results; %rezultat analize, reci i weight
        mapperList; %lista EvidenceToHashMapMappera
        questionId; %id pitanja
        evidenceType; %1 - question, 2 - answer, 3 - thread
    end
    
    methods
        function obj = QuestionToProfileSimilarityBagOfTasks(questionId, questionEvidences, usersList,...
                mapperList, evidenceType, isOriginal, isMax)
            obj@QuestionToProfileSimilarity(isOriginal, isMax);
            
            obj.questionId = questionId;
            obj.question = questionEvidences;
            obj.usersList = usersList;
            %obj.results = results; %results ne koristimo je je MATLAB pass-by-value tipa
            obj.results = containers.Map(); %samo mu postavimo tip
            obj.mapperList = mapperList;
            obj.evidenceType = evidenceType;
        end
        
        function out = getResults(obj)
            out = obj.results;
        end
        
        function run(obj)
            for I=1:numel(obj.usersList)
                userId = obj.usersList{I}; %uzimamo korisnika
                userEvidences = containers.Map(); %ovde idu dokazi (reci) za tog korisnika
                
                for J=1:numel(obj.mapperList)
                    mapper = obj.mapperList{J}; %uzimamo mapper objekat
                    userEvidences = EvidenceToHashMapMapper.joinEvidences(userEvidences, mapper.generateUserHashMapForPost(userId, obj.evidenceType , obj.questionId)); %prosirujemo listu reci
                end
                
                obj.results(userId) = obj.getWeightedSimilarity(userEvidences, obj.question); %racuna similarity i dodaje u user_values na svoje mesto
                
                fprintf('%3d. User: %s\t%.15f\t', I, userId, obj.results(userId));
                printMap('', obj.question);
            end
        end %end run
    end
    
end

