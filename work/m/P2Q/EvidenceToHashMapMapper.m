classdef EvidenceToHashMapMapper
    %EVIDENCETOHASHMAPMAPPER Summary of this class goes here
    %   sadrzi razne funkcije za kopanje po interestmining bazi
    
    properties(Access = 'private', Constant);
        connection = edu.ucl.sspace.rs.etf.database.ESConnection.getConnection('interestminingl6typeall');; %ESConnection objekat
    end
    
    properties(Access = 'private')
        informationSourceId;
    end
    
    methods
        function obj = EvidenceToHashMapMapper(informationSourceId)
            obj.informationSourceId = informationSourceId;
        end
        
        function out = generatePostHashMap(obj, postId)
            out = obj.getEvidences(postId);
        end
        
        function out = generateUserHashMapForPost(obj, userId, evidenceType, questionIdToRemove)
            userMap = containers.Map(); %ovo je prazno na pocetku
            
            if evidenceType == 1 %evidence je question
                qids = obj.getAllQuestionIdsForUser(userId); %sva pitanja koje je korisnik postavio
                for I=1:numel(qids)
                    questionId = qids{I};
                    postMap = obj.generatePostHashMap(questionId);
                    pmapKeys = postMap.keys(); %pokupimo sve kljuceve (reci)
                    for J=1:numel(pmapKeys)
                        keyword = pmapKeys{J};
                        if userMap.isKey(keyword)
                            userMap(keyword) = postMap(keyword) + userMap(keyword) - postMap(keyword) * userMap(keyword); %probTCoNorm
                        else
                            userMap(keyword) = postMap(keyword);
                        end
                    end
                end
            elseif evidenceType == 2 %evidence je answer
                aids = obj.getAllAnswerIdsForUser(userId); %svi odgovori korisnika
                for I=1:numel(aids)
                    questionId = aids{I};
                    postMap = obj.generatePostHashMap(questionId);
                    pmapKeys = postMap.keys(); %pokupimo sve kljuceve (reci)
                    for J=1:numel(pmapKeys)
                        keyword = pmapKeys{J};
                        if userMap.isKey(keyword)
                            userMap(keyword) = postMap(keyword) + userMap(keyword) - postMap(keyword) * userMap(keyword); %probTCoNorm
                        else
                            userMap(keyword) = postMap(keyword);
                        end
                    end
                end
            end
            
            out = userMap;
        end
    end %end public methods
    
    methods(Access = 'private')
        function out = getEvidences(obj, postId)
            % dohvata dokaze (reci) i weight za pitanje/odgovor postId
            import com.microsoft.sqlserver.jdbc.SQLServerDriver;
            query = 'SELECT e.keyword AS keyword, e.weight AS weight ';
            query = strcat(query, ' FROM ');
            query = strcat(query, '   Evidence e, ');
            query = strcat(query, '   EvidencePost ep, ');
            query = strcat(query, '   Posts p ');
            query = strcat(query, ' WHERE ');
            query = strcat(query, '   p.id = ? AND ');
            query = strcat(query, '   e.informationSourceId = ? AND ');
            query = strcat(query, '   p.id = ep.idPost AND ');
            query = strcat(query, '   ep.idEvidence = e.id ');
            query = strcat(query, ' ORDER BY e.keyword ASC; ');
            %disp(query);
            stmt = EvidenceToHashMapMapper.connection.prepareStatement(query);

            stmt.setString(1, postId);
            stmt.setString(2, obj.informationSourceId);
            rs = stmt.executeQuery();

            postMap = containers.Map();
            while rs.next()
                keyword = char(rs.getString('keyword'));
                weight = double(rs.getDouble('weight'));
                postMap(keyword) = weight;
                %fprintf('%-15s = %.15f\n', keyword, weight);
            end
            
            out = postMap;
        end %end getEvidences
        
        function out = getAllQuestionIdsForUser(obj, userId)
            %dohvata question ids za korisnika userId
            retVal = {};
            import com.microsoft.sqlserver.jdbc.SQLServerDriver;
            query = 'SELECT p.id AS id ';
            query = strcat(query, ' FROM ');
            query = strcat(query, '   Users u, ');
            query = strcat(query, '   Posts p ');
            query = strcat(query, ' WHERE ');
            query = strcat(query, '   u.id = ? AND ');
            query = strcat(query, '   u.id = p.ownerUserId AND ');
            query = strcat(query, '   p.postTypeId = 1;');
            stmt = EvidenceToHashMapMapper.connection.prepareStatement(query);
            stmt.setString(1, userId);
            rs = stmt.executeQuery();
            while rs.next()
                retVal{end+1} = char(rs.getString('id'));
            end
            rs.close();
            stmt.close();
            %disp(retVal);
            out = retVal;
        end %end getAllQuestionIdsForUser
        
        function out = getAllAnswerIdsForUser(obj, userId)
            %dohvata answer ids za korisnika userId
            retVal = {};
            import com.microsoft.sqlserver.jdbc.SQLServerDriver;
            query = 'SELECT p.id AS id ';
            query = strcat(query, ' FROM ');
            query = strcat(query, '  Users u, ');
            query = strcat(query, '  Posts p ');
            query = strcat(query, ' WHERE ');
            query = strcat(query, '   u.id = ? AND ');
            query = strcat(query, '   u.id = p.ownerUserId AND ');
            query = strcat(query, '   p.postTypeId = 2; ');
            stmt = EvidenceToHashMapMapper.connection.prepareStatement(query);
            stmt.setString(1, userId);
            rs = stmt.executeQuery();
            while rs.next()
                retVal{end+1} = char(rs.getString('id'));
            end
            rs.close();
            stmt.close();
            %disp(retVal);
            out = retVal;
        end %end getAllAnswerIdsForUser
        
    end %end private methods
    
    methods(Static)
        function out = getAllUserIdsForType(type, userCategories)
            %dohvata sve UserId za trazeni tip baze
            %   type - tip baze
            %   userCategories - kategorije interesovanja korisnika
            import com.microsoft.sqlserver.jdbc.SQLServerDriver;
            retVal = {};
            for I=1:numel(userCategories)
                query = strcat('SELECT DISTINCT u.id FROM Users u, Posts p WHERE p.ownerUserId = u.id AND u.type = ', int2str(type), ' AND p.cat2 LIKE ''%', userCategories{I}, '%'';'); 
                %disp(query);
                stmt = EvidenceToHashMapMapper.connection.prepareStatement(query);
                rs = stmt.executeQuery();
                while rs.next()
                    retVal{end+1} = char(rs.getString('id'));
                end
                rs.close();
                stmt.close();
            end
            out = retVal;
        end %end getAllUserIdsForType
        
        function out = joinEvidences(e1, e2)
            %spaja 2 containers.Map
            joined = e1;
            e2keys = e2.keys();
            for I=1:numel(e2keys)
                keyword = e2keys{I};
                if e1.isKey(keyword) %ako postoji ista rec u e1 i e2 racunamo preko TConorm formule
                    joined(keyword) = e1(keyword) + e2(keyword) - e1(keyword) * e2(keyword);
                else %u suprotnom dodajemo element iz e2
                    joined(keyword) = e2(keyword);
                end 
            end
        	out = joined;
        end %end joinEvidences
        
        function out = getAllQuestionIdsForUserForEvaluation(userId)
            %vraca pitanja na koja je korisnik userId dao najbolji odgovor
            import com.microsoft.sqlserver.jdbc.SQLServerDriver;
            retVal = {};
            query = 'select question.id from Posts as question inner join Posts as bestAnswer on question.acceptedAnswerId = bestAnswer.id WHERE question.acceptedAnswerId <> '''' and bestAnswer.ownerUserId = ? ;';
            %disp(query);
            stmt = EvidenceToHashMapMapper.connection.prepareStatement(query);
            stmt.setString(1, userId);
            rs = stmt.executeQuery();
            while rs.next() 
                retVal{end+1} = char(rs.getString('id'));
            end
            rs.close();
            stmt.close();
            
            %disp(retVal);
            out = retVal;
        end
        
        function out =  getBestAnswererIdForQuestion(questionId)
            %vraca id najboljeg odgovora za pitanje questionId
            import com.microsoft.sqlserver.jdbc.SQLServerDriver;
            query = 'select bestAnswer.ownerUserId from Posts as question inner join Posts as bestAnswer on question.acceptedAnswerId = bestAnswer.id WHERE question.id = ?;';
            
            stmt = EvidenceToHashMapMapper.connection.prepareStatement(query);
            stmt.setString(1, questionId);
            rs = stmt.executeQuery();
            retVal = '';
            if rs.next()
                retVal =  char(rs.getString('ownerUserId'));
            else 
               fprintf('NO bestAnswerOwnerUserId"\n');
            end
            rs.close();
            stmt.close();

            out = retVal;
        end
    end
    
end

