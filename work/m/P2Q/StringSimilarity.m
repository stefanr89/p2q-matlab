classdef StringSimilarity < handle
    %STRINGSIMILARITY klasa koja uvrdjuje string slicnost
    %   iz nekog razloga sva polja su public u javi, ovde private
    %   ova klasa sadrzi jos i implementacije LCS, MCLCS1 i MCLCSN
    
    properties(Access = 'protected')
        sigmaM = 0; %kraci
        sigmaN = 0; %duzi
        m = 0; %kraci
        n = 0; %duzi
        P = cell(0);
        R = cell(0);
        refP = containers.Map(); %duza recenica
        refR = containers.Map(); %kraca recenica
        sameWords = cell(0);
    end %end properties
    
    methods
        function obj = StringSimilarity(sent1, sent2)
            %ovaj konstruktor obuhvata sva 3 konstruktora iz istoimene java
            %klase, prima 2 objekta container.Map, 2 cell arraya ili bez
            %argumenata
            
            obj@handle(); %konstruktor superklase
            
            if(nargin == 0) % ako nema argumenata ne konstruisemo nista
                return;
            end
            
            if(isa(sent1, 'containers.Map') && isa(sent2, 'containers.Map')) %ako su oba argumenta hash mape
                strSentence1 = sent1.keys();
                strSentence2 = sent2.keys();
                if(length(strSentence1) >= length(strSentence2))
                    obj.refP = sent1;
                    obj.refR = sent2;
                else
                    obj.refP = sent2;
                    obj.refR = sent1;
                end
            else %na kraju oba argumenta mogu biti samo 2 cell arraya
                strSentence1 = sent1;
                strSentence2 = sent2;
            end
            
            [strShorterSent, strLongerSent] = obj.determineLength(strSentence1, strSentence2); %odredimo koja je duza
            
            %ovo je za tekst R, to je kraci tekst
            for currentStr = strShorterSent
                pos = strcmp(currentStr, strLongerSent);
                if any(pos) %ako smo uspesno matchovali dodajemo u sameWords
                    obj.sameWords(end + 1) = currentStr;
                    obj.sigmaM = obj.sigmaM + 1;
                else % ako nismo matchovali, ide u R
                    obj.R(end+1) = currentStr;
                end
            end
            
            %isto uradimo i za drugi tekst
            for currentStr = strLongerSent
                pos = strcmp(currentStr, strShorterSent);
                if any(pos) %ako smo uspesno matchovali dodajemo u sameWords
                    obj.sigmaN = obj.sigmaN + 1;
                else % ako nismo matchovali, ide u P
                    obj.P(end+1) = currentStr;
                end
            end
            
            obj.m = length(strShorterSent);
            obj.n = length(strLongerSent);
        end %end konstruktor
        
        
        % GETTERI
        function value = getM(obj)
            value = obj.m;
        end
        
        function value = getN(obj)
            value = obj.n;
        end
        
        function value = getSigmaM(obj)
            value = obj.sigmaM;
        end
        
        function value = getSigmaN(obj)
            value = obj.sigmaN;
        end
        
        function value = getRefP(obj, hash)
            value = obj.refP(hash);
        end
        
        function value = getRefR(obj, hash)
            value = obj.refR(hash);
        end
        
        function value = getR(obj, num)
            value = obj.R{num};
        end
        
        function value = getP(obj, num)
            value = obj.P{num};
        end
        
        function value = getSameWords(obj)
            value = obj.sameWords;
        end
        
        function out = getCommonWordOrderSimilarity(obj, str1, str2)
            %ovo nije implementirano u javi, pa nije ni ovde, neka ostane
            %samo da odrzi API
            out = 1.0;
        end
        
        function out = getStringSimilarity(obj, word1, word2)
            %poredi 2 reci na sva tri nacina, svaki nacin ima tezinu 1/3
            %tj. ukupna string slicnost je 1/3 rezultata lcs, 1/3 mclcs1 i
            %1/3 mclcsN
            strLCS = obj.lcs(word1, word2);
            strMCLCS1 = obj.mclcs1(word1, word2);
            strMCLCSN = obj.mclcsN(word1, word2);
            
            %normalizacija
            w_total = length(word1)*length(word2);
            v1 = (length(strLCS)^2)/w_total;
            v2 = (length(strMCLCS1)^2)/w_total;
            v3 = (length(strMCLCSN)^2)/w_total;
            
            %ne bi trebalo da bude problema ako ne mnozim svaki ponaosob
            out = 0.33*(v1+v2+v3); %ipak mislim da ovde treba * 1/3
        end %end getStringSimilarity
        
        function out = getStringSimilarityMatrix(obj)
            %pravi string similarity matrix na osnovu recenica koje smo
            %dobili pri konstrukciji
            simMatrix = zeros(obj.m - obj.sigmaM, obj.n - obj.sigmaN);
            for I=1:numel(obj.R)
                for J=1:numel(obj.P)
                    simMatrix(I,J) = obj.getStringSimilarity(obj.R{I}, obj.P{J});
                end
            end
            out = simMatrix;
        end %end getStringSimilarityMatrix
    
        function out = allWordsAreSame(obj)
            %odredjuje da li bar jedan tekst sadrzi sve reci drugog teksta
            if (numel(obj.P) == 0 || numel(obj.R) == 0)
                out = true;
            else
                out = false;
            end
        end
        
    end %end public methods
    
    methods(Access = 'private')
        function [shorter, longer] = determineLength(obj, strSentence1, strSentence2)
            %odredjuje koja je recenica duza
            if(length(strSentence1) >= length(strSentence2))
                shorter = strSentence2;
                longer = strSentence1;
            else
                shorter = strSentence1;
                longer = strSentence2;
            end
        end
        
        function out = lcs(obj, a, b)
            %racuna Longest Common Subsequence
            lcsmat = zeros(length(a)+1, length(b)+1);
            %ovo ne moze da se uradi na pametniji nacin jer je neophodan
            %char-by-char comparison, pogledati opis algoritma na wiki
            for I=1:length(a)
                for J=1:length(b)
                    if a(I) == b(J)
                        lcsmat(I+1, J+1) = lcsmat(I, J) + 1;
                    else
                        lcsmat(I+1, J+1) = max(lcsmat(I+1, J), lcsmat(I, J+1));
                    end
                end
            end
            %kada smo sastavili lcsmat treba pronaci najduzi substring
            result = '';
            I = length(a) + 1;
            J = length(b) + 1;
            while(I>1 && J>1)
                if (lcsmat(I,J) == lcsmat(I-1,J))
                    I = I-1;
                elseif (lcsmat(I,J) == lcsmat(I,J-1))
                    J = J-1;
                else
                    assert(a(I-1) == b(J-1));
                    result = [a(I-1), result];
                    I = I-1;
                    J = J-1;
                end
            end
            out = result;
        end % end lcs
        
        function out = mclcs1(obj, str1, str2)
            %racuna MCLCS1
            padder = char({str1, str2}); %ako su razlicite duzine, kraci string bice dopunjen blank poljima
            res = find(padder(1,:) ~= padder(2,:), 1); %ovo je prva razlicita lokacija
            if isempty(res) %stringovi su identicni
                out = str1;
            else %stringovi se samo delimicno preklapaju, vracamo substring
                out = padder(1, 1:res-1);
            end
        end
        
        function out = mclcsN(obj, str1, str2)
            %racuna MCLCSN
            [shorter, longer] = obj.determineLength(str1, str2);
            max_substr = ''; %pretpostavimo da nista nece naci
            for I=1:length(shorter)
                for J=I:length(shorter)
                    substr = shorter(I:J); %isecka kraci string
                    res = strfind(longer, substr); %potrazi ga u duzem
                    if ~isempty(res) && (length(substr) > length(max_substr)) %ako smo nasli nesto sto je duze od maksimuma
                        max_substr = substr;
                    end
                end
            end
            out = max_substr;
        end
    end %end private methods
    
    methods(Static)
        function main(args)
            %u javi ovde nista nije implementirano, neka ostane da odrzi API i
            %napravicu test poput onog u DSSManuscript koji meri i vreme
            tic
            sent1 = {'invaliditet', 'svi', 'ima', 'potesxkocy', 'kretany', 'mo', 'kor', 'podzemn', 'beograd'};
            sent2 = {'posebn', 'potre', 'pusx', 'rad'};
            test = StringSimilarity(sent1, sent2);
            disp(test.getStringSimilarityMatrix());
            toc
        end
    end
    
end

