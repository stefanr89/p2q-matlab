classdef Evaluator
    %EVALUATOR evaluira po MRR i P@N
    %   MRR � Metoda za evaluaciju prikupljenih informacija (information retrieval task). 
    %       Lista dohvacenih odgovora na pitanje(query) je sortirana po verovatnoci da je odgovor tacan. 
    %       Iz grupe pitanja Q za svako pitanje q odredimo slicnost S(q,u) 
    %       za sve korisnicke profile u iz grupe profila U i rangiramo korisnike. 
    %       Za svako pitanje sracunamo rank korisnika koji je dao najbolji odgovor. 
    %       MRR = 1/Q * sum(1/ranki), i = 1...Q
    %   P@N - Preciznost sa cut-off vrednoscu N (obicno multiple od 5).
    %       Preciznost je deo dohvacenih dokumenata (korisnika) koji su relevantni za query (pitanje).
    %       U ovom domenu istrazivanja, to je mera koja nam govori kolike su sanse da pitanje upuceno 
    %       izabranim N korisnicima dobije tacan odgovor. Korisnici se rangiraju prema S(q,u).
    
    methods(Static)
        function out = evaluate(results)
            %radi evaluaciju za sve elemente u strukturi
            retval = 0;
            for I = 1:numel(results)
                pos = find(strcmp(results(I).rankedIds, results(I).bestId)); %nadjemo rec koja odgovara
                retval = retval + 1/pos; %ovo je formula, nije pos+1 zato sto ovde indeksiramo od 1
            end
            out = retval / numel(results); %delimo sa brojem unosa u strukturi
        end %end evaluate
        
        function out = calculateRank(bestAnswererUserId, result)
            %prebroji koliko ima elemenata sa vecim TF od
            %bestAnswererUserId, pa vrati 1/count
            
            target = result(bestAnswererUserId);
            values = result.values();
            
            count = sum(target < cell2mat(values)) + 1; %broj svih elemenata koji su veci od target TF, krece od 1
            
            out = count;
        end %end calculateRank
        
        function out = calculateMRRRank(bestAnswererUserId, result)
            out = 1/Evaluator.calculateRank(bestAnswererUserId, result);
        end
        
        function out = calculateMRR(ranks)
            %racuna MRR preko liste rankova
            out = sum(ranks)/numel(ranks);
        end
        
        function out = calculatePrecisionAtN(ranks, N)
            %racuna P@N
            out = sum(ranks <= N)/numel(ranks);
        end
        
        function main(args)
            results = struct('rankedIds', {}, 'bestId', {});
            results(1).rankedIds = {'catten', 'cati', 'cats'}; results(1).bestId = 'cats';
            results(2).rankedIds = {'torii', 'tori', 'toruses'}; results(2).bestId = 'tori';
            results(3).rankedIds = {'viruses', 'virii', 'viri'}; results(3).bestId = 'viruses';

            fprintf('This value: %f should equal about 0.61\n', Evaluator.evaluate(results)); %test za evaluator
            
            map = containers.Map(results(1).rankedIds, {0.4, 0.7, 0.9});
            fprintf('This value: %f should equal about 0.33\n', Evaluator.calculateMRRRank('catten', map)); %test za MRR rank
            
            ranks = [3, 7, 1, 6];
            fprintf('calculatePrecisionAtN : %f = 0.5\n', Evaluator.calculatePrecisionAtN(ranks, 5)); %test za P@5
        end %end main
    end %end static methods
    
end
