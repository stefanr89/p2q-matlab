classdef QuestionToProfileSimilarity < handle
    %QUESTIONTOPROFILESIMILARITY Racuna semanticku slicnost kod P2Q i LInSTSS
    %   Detailed explanation goes here
    
    properties(Access = 'private')
        question = containers.Map(); %drzi String, Double
        users = containers.Map(); %drzi String, Map<String, Double>
        resMap = containers.Map(); %drzi String, Double
        isOriginal = false;
        isMax = true;
        ssDBr; %za komunikaciju sa bazom
    end
    
    methods
        function obj = QuestionToProfileSimilarity(questions, users, results, isOriginal, isMax)
            %konstruktor, moramo koristiti SemanticSpaceDBReader iz jave
            %ukoliko se pozove sa svih 5 parametara, svaki parametar ima
            %svoj odgovarajuci, ako se konstruise sa 2, questions ->
            %isOriginal, users -> isMax
            import edu.ucl.sspace.rs.etf.database.SemanticSpaceDBReader; %neophodno da bi smo pravili DB reader objekat
            import com.microsoft.sqlserver.jdbc.SQLServerDriver; %neophodno da bi konektor video drajver
            
            obj@handle(); %konstruktor handle superklase
            
            if nargin == 2
                obj.isOriginal = questions;
                obj.isMax = users;
                return;
            end
            
            obj.ssDBr = SemanticSpaceDBReader(14000,0); %matlab ne moze da dohvati enum u klasi, pa cemo ovo resiti ovako
            obj.question = questions;
            obj.users = users;
            obj.resMap = results;
            obj.isOriginal = isOriginal;
            obj.isMax = isMax;
        end %end konstruktor
        
        function out = getWeightedSimilarity(obj, question, user)
            %pronalazi slicnost izmedju pitanja i korisnika, max ili
            %weighted
            if (numel(question) == 0 || numel(user) == 0)
                out = 0;
                return;
            end
            
            if obj.isMax
                out = obj.maxSemSim(question, user);
            else
                out = obj.weightedSemSim(question, user);
            end
        end %end getWeightedSimilarity
        
        
        function out = weightedSemSim(obj, sent1, sent2)
            %vraca weighted similarity score (za LInSTSS)
            
            % STEP 1 - izbacujemo stop reci, vec odradjeno pre pozivanja
            % ove funkcije
            
            % STEP 2 - izbacimo identicne reci, formiramo P i R
            sSim = StringSimilarity(sent1, sent2);
            
            if sSim.allWordsAreSame()
                out = 1;
                return;
            end
            
            % STEP 3 - formiramo Str Sim matricu
            stringSimilarityMatrix = sSim.getStringSimilarityMatrix();
            
            % STEP 4 - formiramo Sem Sim i TF matricu
            % ovde su m i n ukupan broj reci, pa moramo oduzeti sigme (br.
            % ponovljenjih reci)
            m = sSim.getM()-sSim.getSigmaM();
            n = sSim.getN()-sSim.getSigmaN();
            semanticSimilarityMatrix = zeros(m, n);
            termFrequencyMatrix = ones(m, n);
            
            import edu.ucl.sspace.rs.etf.database.SemanticSpaceDBReader;
            import com.microsoft.sqlserver.jdbc.SQLServerDriver;
            obj.ssDBr = SemanticSpaceDBReader(14000,0); %otvaramo konekciju, ne radi bez ovih importa
            
            %trebace nam i ovi algoritmi
            import edu.ucla.sspace.common.Similarity;
            vectorList = cell(n, 1); %ovde idu vektori iz baze za kraci tekst, da bismo izbegli ponovni querying baze
            for I=1:m
                v2 = obj.ssDBr.getVector(sSim.getR(I)); %dohvatimo vektor za rec iz pitanja
                if ~obj.isOriginal
                    termFrequencyMatrix(I, :) = sSim.getRefR(sSim.getR(I)); %dohvatimo TF vrednost i upisemo u ceo red
                end
                
                for J=1:n
                    if I == 1 %prvi put sve sto dohvatimo iz baze za kraci tekst ide u vectorList
                        v1 = obj.ssDBr.getVector(sSim.getP(J));
                        vectorList{J} = v1;
                    else %svaki sledeci pristup vektorima kracih reci ide preko vectorList
                        v1 = vectorList{J};
                    end
                    
                    if ~isempty(v1) && ~isempty(v2)
                        semanticSimilarityMatrix(I,J) = Similarity.cosineSimilarity(v1, v2); % kao getSimiliarity za SimType.COSINE
                    end
                    
                    if ~obj.isOriginal
                        termFrequencyMatrix(I,J) = termFrequencyMatrix(I,J) * sSim.getRefP(sSim.getP(J));
                    end
                end
            end
            
             % STEP 5 - pravimo unificiranu matricu
            psi = 0.45; % sa ovolikim udelom ucestvuje string similarity u unificiranoj matrici
            fiCorpusBased = 0.55; % sa ovolikim udelom ucestvuje semantic similarity u unificiranoj matrici
            psiM1 = stringSimilarityMatrix .* psi; % Str SM pomnozen sa faktorom 0.45
            fiCorpusBasedM2 = semanticSimilarityMatrix .* fiCorpusBased; % Sem SM pomonozen sa faktorom 0.55
            
            jointMatrix = psiM1 + fiCorpusBasedM2; % unificirana matrica
            termFrequencyMatrix = 2 .^ (termFrequencyMatrix - 1); % prema formuli za TF
            jointMatrix = jointMatrix .* termFrequencyMatrix;
            
            % STEP 6 - pravimo ro listu sa maksimalnim elementima prema
            % algoritmu za LInSTSS. Od ovog koraka se razlikuju LInSTSS i
            % P2Q
            
            ro = [];
            while ~isempty(jointMatrix) %dok ne ispraznimo joint matricu vadimo najveci element i brisemo prvo redove pa kolone
                [jointMatrix, ro] = MatrixES.findMaxElemAndRemove(jointMatrix, ro);
            end
            
            roSum = sum(ro); % sumiramo sve iz ro
            words = sSim.getSameWords(); %dohvatimo reci
            tfnorms = ones(1, numel(words)); % treba naci i TF za iste reci
            for I=1:numel(words)
                tfnorm1 = sSim.refR.get(words{I}); 
				tfnorm2 = sSim.refP.get(words{I}); 
                tfnorms(I) = tfnorm1 * tfnorm2;
            end
            
            tfnorms = 2.^(tfnorms-1);
            delta = sum(tfnorms);
            
            numerator = (roSum+delta)*(sSim.getM()+sSim.getN());
            denominator = 2*sSim.getM()*sSim.getN();
            
            out = numerator/denominator;
        end %end weightedSemSim
        
        
        function out = maxSemSim(obj, question, user)
            %vraca max similarity score (za P2Q)
            
            %STEP 1 - izbacimo stop reci. smatramo da je ovo vec uradjeno
            %preko ESPreprocesora
            
            %STEP 2 - izbacimo identicne reci
            sSim = StringSimilarityQuestionToProfile(question, user);
            
            if sSim.allWordsAreSame()
                out = 1.0;
                return;
            end
            
            %STEP 3 - napravimo String Similarity matricu
            stringSimilarityMatrix = sSim.getStringSimilarityMatrix();
            
            %STEP 4 - napravimo Semantic Similarity matricu u pomocnu TF matricu
            semanticSimilarityMatrix = zeros(sSim.getM(), sSim.getN()); %pravi se samo od preostalih reci, tj koje nisu istovetne u user i question
            termFrequencyMatrix = ones(sSim.getM(), sSim.getN()); %potrebna je i TF matrica
            
            import edu.ucl.sspace.rs.etf.database.SemanticSpaceDBReader;
            import com.microsoft.sqlserver.jdbc.SQLServerDriver;
            obj.ssDBr = SemanticSpaceDBReader(14000,0); %otvaramo konekciju, ne radi bez ovih importa
            
            %trebace nam i ovi algoritmi
            import edu.ucla.sspace.common.Similarity;
            vectorList = cell(sSim.getN(), 1); %ovde idu vektori iz baze za kraci tekst, da bismo izbegli ponovni querying baze
            for I=1:sSim.getM()
                v2 = obj.ssDBr.getVector(sSim.getP(I)); %dohvatimo vektor za rec iz pitanja
                if ~obj.isOriginal
                    termFrequencyMatrix(I, :) = sSim.getRefP(sSim.getP(I)); %dohvatimo TF vrednost i upisemo u ceo red
                end
                
                for J=1:sSim.getN()
                    if I == 1 %prvi put sve sto dohvatimo iz baze za kraci tekst ide u vectorList
                        v1 = obj.ssDBr.getVector(sSim.getR(J));
                        vectorList{J} = v1;
                    else %svaki sledeci pristup vektorima kracih reci ide preko vectorList
                        v1 = vectorList{J};
                    end
                    
                    if ~isempty(v1) && ~isempty(v2)
                        semanticSimilarityMatrix(I,J) = Similarity.cosineSimilarity(v1, v2); % kao getSimiliarity za SimType.COSINE
                    end
                    
                    if ~obj.isOriginal
                        termFrequencyMatrix(I,J) = termFrequencyMatrix(I,J) * sSim.getRefR(sSim.getR(J));
                    end
                end
            end
            
            % STEP 5 - pravimo unificiranu matricu
            psi = 0.45; % sa ovolikim udelom ucestvuje string similarity u unificiranoj matrici
            fiCorpusBased = 0.55; % sa ovolikim udelom ucestvuje semantic similarity u unificiranoj matrici
            psiM1 = stringSimilarityMatrix .* psi; % Str SM pomnozen sa faktorom 0.45
            fiCorpusBasedM2 = semanticSimilarityMatrix .* fiCorpusBased; % Sem SM pomonozen sa faktorom 0.55
            
            jointMatrix = psiM1 + fiCorpusBasedM2; % unificirana matrica
            termFrequencyMatrix = 2 .^ (termFrequencyMatrix - 1); % prema formuli za TF
            
            % STEP 6 - pravimo roSum maksimalnih elemenata
            roSum = MatrixES.findMaxElemRowsSumAndMultiplyWithWeight(jointMatrix, termFrequencyMatrix);
            
            out = roSum/sSim.getM(); % normalizacija roSum nad brojem koncepata (znacajnih reci) u pitanju
        end %end maxSemSim
    end %end public methods
    
    
    methods(Static)
        function main(args)
            tic
            keyset1 = {'computer', 'tech', 'internet'};
            valset1 = [0.7, 0.4, 0.4];
            keyset2 = {'history', 'science', 'try', 'informatics', 'prove', 'what', 'happen', 'a', 'long', 'time', 'ago'};
            valset2 = [0.1, 0.3, 0.2, 0.5, 0.4, 0.5, 0.7, 0.8, 0.9, 0.11, 0.12];
            keyset3 = {'informatics'};
            valset3 = [0.5];
            
            question1 = containers.Map(keyset1, valset1);
            user1 = containers.Map(keyset2, valset2);
            user2 = containers.Map(keyset3, valset3);
            
            disp('Some random weights to words in sentence');
            disp('Question: '); disp(question1.keys());
            disp('User1: '); disp(user1.keys());
            disp('User2: '); disp(user2.keys());
            
            ESP = ESPreprocessor();
            question1 = ESP.filterWeightedWords(question1, false, true);
            user1 = ESP.filterWeightedWords(user1, false, true); 
            user2 = ESP.filterWeightedWords(user2, false, true);
            
            disp('After Processing:');
            disp('Question: '); disp(question1.keys()); disp(question1.values());
            disp('User1: '); disp(user1.keys()); disp(user1.values());
            disp('User2: '); disp(user2.keys()); disp(user2.values());
            
            simWeighted = QuestionToProfileSimilarity(false, false);
            simMax = QuestionToProfileSimilarity(false, true);
            
            %fprintf('Weighted Similarity for user1  is: %.15f\n', simWeighted.getWeightedSimilarity(question1, user1));
            
            fprintf('Max2 Similarity for user1  is: %.15f\n', simMax.getWeightedSimilarity(question1, user1));
            fprintf('Max2 Similarity for user2  is: %.15f\n', simMax.getWeightedSimilarity(question1, user2));
            toc
        end
    end
end

